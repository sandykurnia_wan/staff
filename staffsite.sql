-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Des 2017 pada 09.50
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `staffsite`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `judul_buku` varchar(200) NOT NULL,
  `tahun_terbit` year(4) NOT NULL,
  `jml_hlmn` int(11) NOT NULL,
  `penerbit` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `username`, `judul_buku`, `tahun_terbit`, `jml_hlmn`, `penerbit`) VALUES
(3, 'retno@live.undip.ac.id', 'Matematika Diskrit', 2009, 89, 'Erlangga'),
(4, 'retno@live.undip.ac.id', 'Matematika 2', 2001, 280, 'Erlangga'),
(5, 'retno@live.undip.ac.id', 'Matematika 3', 2002, 289, 'Erlangga'),
(7, 'sandykurniawan@outlook.com', 'AI untuk Masa Depan', 2013, 89, 'Tembalang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen`
--

CREATE TABLE `dokumen` (
  `idDokumen` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jenisAct` enum('Org','Buku','Review','Narasumber','Leader') NOT NULL,
  `idAct` int(20) NOT NULL,
  `filename` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `dokumen`
--

INSERT INTO `dokumen` (`idDokumen`, `username`, `jenisAct`, `idAct`, `filename`) VALUES
(18, 'sandykurniawan@outlook.com', 'Review', 2, 'Sandy_-_Rangkuman_-_TSP_dengan_GA1.pdf'),
(21, 'sandykurniawan@outlook.com', 'Narasumber', 1, 'Sandy_-_Rangkuman_-_TSP_dengan_GA3.pdf'),
(22, 'sandykurniawan@outlook.com', 'Org', 29, 'Sandy_-_Rangkuman.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen_achievement`
--

CREATE TABLE `dokumen_achievement` (
  `idDokumen` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jenisAch` enum('HKI','Jurnal','Prosiding','Penelitian','Pengabdian','Penghargaan') NOT NULL,
  `idAch` int(20) NOT NULL,
  `filename` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokumen_achievement`
--

INSERT INTO `dokumen_achievement` (`idDokumen`, `username`, `jenisAch`, `idAch`, `filename`) VALUES
(1, 'sandykurniawan@outlook.com', 'HKI', 3, 'uasku.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumen_pendidikan`
--

CREATE TABLE `dokumen_pendidikan` (
  `idDokumen` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jenisPend` enum('Formal','Non Formal') NOT NULL,
  `idPend` int(20) NOT NULL,
  `filename` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokumen_pendidikan`
--

INSERT INTO `dokumen_pendidikan` (`idDokumen`, `username`, `jenisPend`, `idPend`, `filename`) VALUES
(2, 'sandykurniawan@outlook.com', 'Non Formal', 2, 'boaordpass-azis1.pdf'),
(3, 'sandykurniawan@outlook.com', 'Non Formal', 4, 'boaordpass-azis-3.pdf'),
(4, 'sandykurniawan@outlook.com', 'Non Formal', 8, 'uasku.pdf'),
(5, 'sandykurniawan@outlook.com', 'Formal', 5, 'uasfafa.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `formal`
--

CREATE TABLE `formal` (
  `id_formal` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `jenjang` varchar(8) NOT NULL,
  `nama_univ` varchar(30) NOT NULL,
  `bidang` varchar(50) NOT NULL,
  `thn_masuk` year(4) NOT NULL,
  `thn_lulus` year(4) NOT NULL,
  `judul_ta` varchar(300) NOT NULL,
  `dosbing1` varchar(50) NOT NULL,
  `dosbing2` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `formal`
--

INSERT INTO `formal` (`id_formal`, `username`, `jenjang`, `nama_univ`, `bidang`, `thn_masuk`, `thn_lulus`, `judul_ta`, `dosbing1`, `dosbing2`) VALUES
(1, 'zulfikaraka@gmail.com', 'S1', 'Universitas Diponegoro', 'Ilmu Komputer/ Informatika', 2014, 2018, 'Pengaruh Bermain Dota Terhadap Perilaku Remaja', 'ncdkcnknv', 'ndknfdkjvnkjfnv'),
(3, 'sandykurniawan@outlook.com', 'S1', 'Universitas Gajah Mada', 'Manajemen Sistem Informasi', 2015, 2016, 'JKbjbgjGJ UHjhgHJ  gh k KgKHKJH kjgkHk', 'JGbncdnbcdbBb HJK hkjchds js cj', 'jhskjc  jshjkdshc J Hsjc d'),
(5, 'sandykurniawan@outlook.com', 'S2', 'Universitas Diponegoro', 'Pemerintahan', 2014, 2018, 'scbdshcbhdsbc', 'bdhbcdsbcjhbcdhjc', 'bjhbbdbcjdfb'),
(6, 'sandykurniawan@outlook.com', 'S3', 'sxsxsxsd', 'fdfdsfsfsdsd scdcsdcsddsc', 2014, 2018, 'sggsgudhkdhuh dadada', 'indah', 'indahhhhhhhhhh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fungsional`
--

CREATE TABLE `fungsional` (
  `id_fung` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `jab_fung` enum('Staff Pengajar','Asisten Ahli','Lektor','Lektor Kepala','Guru Besar') NOT NULL,
  `awal_fung` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fungsional`
--

INSERT INTO `fungsional` (`id_fung`, `username`, `jab_fung`, `awal_fung`) VALUES
(1, 'sandykurniawan@outlook.com', 'Staff Pengajar', '2017-05-09'),
(2, 'sandykurniawan@outlook.com', 'Lektor', '2017-06-16'),
(3, 'sandykurniawan@outlook.com', 'Lektor Kepala', '2017-05-02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grup`
--

CREATE TABLE `grup` (
  `id_grup` int(1) NOT NULL,
  `nama_grup` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `grup`
--

INSERT INTO `grup` (`id_grup`, `nama_grup`) VALUES
(1, 'Admin'),
(2, 'Dosen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hki`
--

CREATE TABLE `hki` (
  `id_hki` int(20) NOT NULL,
  `judul_hki` varchar(50) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jenis_hki` varchar(20) NOT NULL,
  `no_hki` varchar(20) NOT NULL,
  `username` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hki`
--

INSERT INTO `hki` (`id_hki`, `judul_hki`, `tahun`, `jenis_hki`, `no_hki`, `username`) VALUES
(3, 'Pengering Sepatu', 2005, 'Hak Cipta', '0192092019', 'sandykurniawan@outlook.com'),
(4, 'wudghewdh', 2000, 'Paten Sederhana', '1278912', 'zulfikaraka@gmail.com'),
(5, 'asagsagsugquhsjas', 2001, 'Paten Sederhana', '12345684280392393923', 'sandykurniawan@outlook.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurnal`
--

CREATE TABLE `jurnal` (
  `id_jurnal` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul_jurnal` varchar(50) NOT NULL,
  `tahun` year(4) NOT NULL,
  `volume` varchar(20) NOT NULL,
  `no_jurnal` varchar(20) NOT NULL,
  `tgl_terbit` date NOT NULL,
  `indeks` varchar(20) NOT NULL,
  `tingkat` varchar(20) NOT NULL,
  `penulis_ke` varchar(10) NOT NULL,
  `url_jurnal` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurnal`
--

INSERT INTO `jurnal` (`id_jurnal`, `username`, `judul_jurnal`, `tahun`, `volume`, `no_jurnal`, `tgl_terbit`, `indeks`, `tingkat`, `penulis_ke`, `url_jurnal`) VALUES
(6, 'zulfikaraka@gmail.com', 'HJVvsjcvdjvcdv', 2010, 'III', '8971 1278', '2017-10-11', 'Tidak', 'Internasional', '2', 'dhchjdbchjghjchdvcdc'),
(8, 'sandykurniawan@outlook.com', 'Struktur Diskrit', 2004, 'III', '231231232132132', '2017-12-07', 'Ya', 'Nasional', '2', 'http://www.djfjkdnjfdndfn.com/diskrit'),
(9, 'sandykurniawan@outlook.com', 'aaaaaaaaaaaaaasssda 1234', 2017, 'IV', '1231312asasasa', '2017-12-05', 'Tidak', 'Internasional', '1', 'https://www.google.co.id/search?q=arti+pseudocode&');

-- --------------------------------------------------------

--
-- Struktur dari tabel `leader`
--

CREATE TABLE `leader` (
  `id_leader` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `tingkat` varchar(50) NOT NULL,
  `periode` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `leader`
--

INSERT INTO `leader` (`id_leader`, `username`, `jabatan`, `tingkat`, `periode`) VALUES
(1, 'sandykurniawan@outlook.com', 'Ketua Pelatdas', 'Departemen', '2014');

-- --------------------------------------------------------

--
-- Struktur dari tabel `narasumber`
--

CREATE TABLE `narasumber` (
  `id_narasumber` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul` varchar(200) NOT NULL,
  `penyelenggara` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `tingkat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `narasumber`
--

INSERT INTO `narasumber` (`id_narasumber`, `username`, `judul`, `penyelenggara`, `tanggal`, `tingkat`) VALUES
(1, 'sandykurniawan@outlook.com', 'Seminar Kesehatan Nasional', 'FKM Undip', '2017-05-01', 'Nasional'),
(3, 'sandykurniawan@outlook.com', 'werwe', 'tsdgsd', '2017-09-05', 'sdgsdg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `non_formal`
--

CREATE TABLE `non_formal` (
  `id_non_formal` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_kegiatan` varchar(50) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `penyelenggara` varchar(30) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `pola` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `non_formal`
--

INSERT INTO `non_formal` (`id_non_formal`, `username`, `nama_kegiatan`, `tgl_kegiatan`, `penyelenggara`, `tempat`, `pola`) VALUES
(1, 'zulfikaraka@gmail.com', 'Seminar Data Mining', '2017-01-20', 'Semarang Data Mining Community', 'Auditorium Imam Barjo', '2 Hari'),
(2, 'sandykurniawan@outlook.com', 'Seminar Big Data', '2013-06-03', 'BPPT', 'Dekanat FSM Undip', '2 Hari'),
(4, 'sandykurniawan@outlook.com', 'Seminar Nasional SmartCity', '2017-05-24', 'Kementrian Kominfo', 'Gedung Teater Keong Mas', '4 Hari'),
(8, 'sandykurniawan@outlook.com', 'scscdscsdcsd', '2017-12-12', 'dfvdfvdfvdfvdv', 'fvdfvdfvdfvdf', 'fdvdfvdfvf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `org`
--

CREATE TABLE `org` (
  `id_org` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `no_anggota` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `org`
--

INSERT INTO `org` (`id_org`, `username`, `judul`, `tahun`, `jabatan`, `no_anggota`) VALUES
(1, 'retno@live.undip.ac.id', 'Himpunan Mahasiswa Informatika', '0000', 'Anggota Divisi Diklat', '4320842039853205'),
(2, 'retno@live.undip.ac.id', 'kjanskas', '0000', 'akdjasd', 'askjdasd'),
(3, 'retno@live.undip.ac.id', 'khk', '0000', 'oasdaskj', 'kjnsadjknasdkj'),
(4, 'retno@live.undip.ac.id', 'sdkfjnsdfjsdnK', '0000', 'kajsndaksnd', 'asdkja'),
(5, 'retno@live.undip.ac.id', 'hgjghgj', '6875', 'dsxsfsx', 'hfhfh'),
(29, 'sandykurniawan@outlook.com', 'mhs', '2389', 'mhs', '909'),
(31, 'sandykurniawan@outlook.com', 'sdfsdf', '4235', 'werwe', 'werwer'),
(35, 'sandykurniawan@outlook.com', 'rrwer', '1222', 'qwe', 'qwew');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pangkat`
--

CREATE TABLE `pangkat` (
  `id_pangkat` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `gol_ruang` varchar(50) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `tmt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pangkat`
--

INSERT INTO `pangkat` (`id_pangkat`, `username`, `gol_ruang`, `jenis`, `tmt`) VALUES
(1, 'sandykurniawan@outlook.com', 'II/a', 'Pengatur Muda', '2017-05-01'),
(2, 'sandykurniawan@outlook.com', 'IV/a', 'Pembina', '2017-05-02'),
(3, 'sandykurniawan@outlook.com', 'II/b', 'Pengatur Muda Tingkat I', '2017-05-10'),
(4, 'sandykurniawan@outlook.com', 'III/d', 'Penata Tingkat I', '2017-05-03'),
(5, 'sandykurniawan@outlook.com', 'I/d', 'Juru Tingkat I', '2017-09-12'),
(6, 'sandykurniawan@outlook.com', 'I/c', 'Juru', '2017-09-12'),
(7, 'sandykurniawan@outlook.com', 'IV/d', 'Pembina Utama Madya', '2017-09-06'),
(8, 'sandykurniawan@outlook.com', 'IV/e', 'Pembina Utama', '2017-09-14'),
(9, 'sandykurniawan@outlook.com', 'IV/c', 'Pembina Utama Muda', '2017-09-15'),
(10, 'sandykurniawan@outlook.com', 'IV/c', 'Pembina Utama Muda', '2017-09-04'),
(11, 'sandykurniawan@outlook.com', 'I/a', 'Juru Muda', '2017-09-08'),
(12, 'sandykurniawan@outlook.com', 'III/b', 'Penata Muda Tingkat I', '2017-09-14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penelitian`
--

CREATE TABLE `penelitian` (
  `id_penelitian` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul_penelitian` varchar(50) NOT NULL,
  `tahun` year(4) NOT NULL,
  `sumber_dana` varchar(30) NOT NULL,
  `jumlah_dana` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `url_penelitian` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penelitian`
--

INSERT INTO `penelitian` (`id_penelitian`, `username`, `judul_penelitian`, `tahun`, `sumber_dana`, `jumlah_dana`, `status`, `url_penelitian`) VALUES
(4, 'sandykurniawan@outlook.com', 'Coba coba', 2020, 'LPDP', 'Rp 800.000', 'Baru', 'http://www.hdbchdbchdb.com'),
(5, 'sandykurniawan@outlook.com', 'popopopipipipipuxvchhbhjb', 2017, 'Internal Fakultas', 'Rp 478.686.979', 'terferivikasi', 'https://mail.google.com/mail/u/0/#inbox');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengabdian`
--

CREATE TABLE `pengabdian` (
  `id_pengabdian` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul_pengabdian` varchar(50) NOT NULL,
  `tahun` year(4) NOT NULL,
  `sumber_dana` varchar(30) NOT NULL,
  `jumlah_dana` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `url_pengabdian` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengabdian`
--

INSERT INTO `pengabdian` (`id_pengabdian`, `username`, `judul_pengabdian`, `tahun`, `sumber_dana`, `jumlah_dana`, `status`, `url_pengabdian`) VALUES
(2, 'sandykurniawan@outlook.com', 'Bsbcdb jkNKJN jkKJN ', 2004, 'Internal Fakultas', 'Rp 6.500.000', 'Bddhhdfvh', 'http://www.dcdfjbv.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penghargaan`
--

CREATE TABLE `penghargaan` (
  `id_penghargaan` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_penghargaan` varchar(50) NOT NULL,
  `tahun` year(4) NOT NULL,
  `pemberi` varchar(30) NOT NULL,
  `tingkat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penghargaan`
--

INSERT INTO `penghargaan` (`id_penghargaan`, `username`, `nama_penghargaan`, `tahun`, `pemberi`, `tingkat`) VALUES
(1, 'zulfikaraka@gmail.com', 'hsdhjbcdb', 2001, 'xskjnxkjdcjkdc', 'Nasional'),
(2, 'sandykurniawan@outlook.com', 'Akademisi Teladan', 2011, 'Kemenristek Dikti', 'Nasional'),
(3, 'sandykurniawan@outlook.com', 'hahaha', 2019, 'akuaja', 'Nasional');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pribadi`
--

CREATE TABLE `pribadi` (
  `id_dosen` int(5) NOT NULL,
  `nm_dosen` varchar(60) NOT NULL,
  `glr_dpn` varchar(10) NOT NULL,
  `glr_blkg` varchar(10) NOT NULL,
  `tmpt_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `almt_rmh` varchar(250) NOT NULL,
  `hp1` varchar(20) NOT NULL,
  `hp2` varchar(20) NOT NULL,
  `almt_ktr` varchar(250) NOT NULL,
  `no_tlp` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `no_krt_peg` varchar(20) NOT NULL,
  `no_ktp` varchar(20) NOT NULL,
  `nidn` varchar(30) NOT NULL,
  `scopus_id` varchar(30) NOT NULL,
  `rgate` varchar(30) NOT NULL,
  `sinta` varchar(250) NOT NULL,
  `lab` varchar(20) NOT NULL,
  `interest` varchar(60) NOT NULL,
  `scholar` varchar(50) NOT NULL,
  `deskripsi` varchar(900) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pribadi`
--

INSERT INTO `pribadi` (`id_dosen`, `nm_dosen`, `glr_dpn`, `glr_blkg`, `tmpt_lahir`, `tgl_lahir`, `almt_rmh`, `hp1`, `hp2`, `almt_ktr`, `no_tlp`, `fax`, `username`, `email`, `nip`, `npwp`, `no_krt_peg`, `no_ktp`, `nidn`, `scopus_id`, `rgate`, `sinta`, `lab`, `interest`, `scholar`, `deskripsi`) VALUES
(13, 'Sandy Kurniawan', 'Dr.', 'M. Cs.', 'serang', '2016-12-21', 'hayoooooooo', '08103810830182012801', '12121111111111111111', '  jcjkdcjkns,cnjdn       ', '089178787', '72380989', 'sandykurniawan@outlook.com', 'sandykurniawati@gmail.com', '72736726381762376', '76273672637', '878237892737', '33780283980912', '82320938092838', 'sandyk', 'sandyk', 'sandyk', 'bcbd', 'Artificial Intellegence', 'sandyk12', ''),
(30, 'zulfikar', '', '', '', '0000-00-00', '', '', '', '', '', '', 'zulfikaraka@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(32, 'sandy', '', '', '', '0000-00-00', '', '', '', '', '', '', 'sieghart.sandy@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(33, 'Anggun Cahya', '', '', '', '0000-00-00', '', '', '', '', '', '', 'anggun.cahya6@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(35, 'Retno', '', '', '', '0000-00-00', '', '', '', '', '', '', 'retno@live.undip.ac.id', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(36, 'Kurniawan', '', '', '', '0000-00-00', '', '', '', '', '', '', 'Kurniawan@asdasda.co.id', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(37, 'muti', '', '', '', '0000-00-00', '', '', '', '', '', '', 'rmutiaras@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(38, 'Alfania Sarah Handayani', '', '', '', '0000-00-00', '', '', '', '', '', '', 'alfaniash7@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profpic`
--

CREATE TABLE `profpic` (
  `idImage` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `filename` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profpic`
--

INSERT INTO `profpic` (`idImage`, `username`, `filename`) VALUES
(1, 'sandykurniawan@outlook.com', 'sandykurniawan@outlook_com.png'),
(2, 'sieghart.sandy@gmail.com', 'sieghart_sandy@gmail_com.png'),
(3, 'zulfikaraka@gmail.com', 'zulfikaraka@gmail_com.jpg'),
(4, 'alfaniash7@gmail.com', 'alfaniash7@gmail_com.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prosiding`
--

CREATE TABLE `prosiding` (
  `id_prosiding` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul_artikel` varchar(50) NOT NULL,
  `nama_prosiding` varchar(50) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `tanggal1` date NOT NULL,
  `tanggal2` date NOT NULL,
  `penulis_ke` int(5) NOT NULL,
  `url_prosiding` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prosiding`
--

INSERT INTO `prosiding` (`id_prosiding`, `username`, `judul_artikel`, `nama_prosiding`, `tempat`, `tanggal1`, `tanggal2`, `penulis_ke`, `url_prosiding`) VALUES
(3, 'sandykurniawan@outlook.com', 'jbbhb', 'contoh prosiding', 'kjnkjnjn', '2013-04-03', '2013-04-04', 2, 'http://www.sjkcndsjkcnds.com'),
(4, 'sandykurniawan@outlook.com', 'hehehe', 'hahaha', 'Semarang', '2017-12-05', '2017-12-12', 3, 'https://www.google.co.id/search');

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id_review` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `penyelenggara` varchar(50) NOT NULL,
  `tingkat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `review`
--

INSERT INTO `review` (`id_review`, `username`, `judul`, `tanggal`, `penyelenggara`, `tingkat`) VALUES
(1, 'sandykurniawan@outlook.com', 'Bedah Buku', '2017-05-04', 'BEM FSM Undip', 'Regional'),
(2, 'sandykurniawan@outlook.com', 'PKL an', '2017-09-22', 'GG', 'lokal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktural`
--

CREATE TABLE `struktural` (
  `id_struk` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `jab_struk` varchar(60) NOT NULL,
  `awal_struk` date NOT NULL,
  `akhir_struk` date NOT NULL,
  `keterangan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `struktural`
--

INSERT INTO `struktural` (`id_struk`, `username`, `jab_struk`, `awal_struk`, `akhir_struk`, `keterangan`) VALUES
(1, 'sandykurniawan@outlook.com', 'Ketua Laboratorium', '2017-05-01', '2017-05-04', ''),
(2, 'sandykurniawan@outlook.com', 'Sekretaris Departemen', '2017-05-01', '2017-05-02', ''),
(3, 'sandykurniawan@outlook.com', 'Ketua Departemen', '2017-04-18', '2017-05-01', ''),
(4, 'sandykurniawan@outlook.com', 'Ketua Departemen', '2017-09-10', '2017-09-20', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `grup` int(11) NOT NULL,
  `logged` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`, `name`, `grup`, `logged`) VALUES
(1, 'admin@staff.undip.ac.id', '96d02fb19d339c27eafbf4db5f8586b4', 'Admin', 1, 0),
(37, 'sandykurniawan@outlook.com', '25f9e794323b453885f5181f1b624d0b', 'Sandy Kurniawan', 2, 1),
(51, 'zulfikaraka@gmail.com', '6eea9b7ef19179a06954edd0f6c05ceb', 'Zulfikar Awan Kurniawan', 2, 1),
(53, 'sieghart.sandy@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'sandy', 2, 0),
(54, 'anggun.cahya6@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'Anggun Cahya', 2, 0),
(56, 'retno@live.undip.ac.id', '6eea9b7ef19179a06954edd0f6c05ceb', 'Retno', 2, 1),
(58, 'rmutiaras@gmail.com', '25d55ad283aa400af464c76d713c07ad', 'muti', 2, 0),
(62, 'alfaniash7@gmail.com', 'b7f64ded7e843d114b234a438ce70504', 'Alfania Sarah Handayani', 2, 1);

--
-- Trigger `user`
--
DELIMITER $$
CREATE TRIGGER `new user` AFTER INSERT ON `user` FOR EACH ROW INSERT INTO pribadi SET nm_dosen = new.name, username = new.username
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `dokumen`
--
ALTER TABLE `dokumen`
  ADD PRIMARY KEY (`idDokumen`);

--
-- Indexes for table `dokumen_achievement`
--
ALTER TABLE `dokumen_achievement`
  ADD PRIMARY KEY (`idDokumen`);

--
-- Indexes for table `dokumen_pendidikan`
--
ALTER TABLE `dokumen_pendidikan`
  ADD PRIMARY KEY (`idDokumen`);

--
-- Indexes for table `formal`
--
ALTER TABLE `formal`
  ADD PRIMARY KEY (`id_formal`);

--
-- Indexes for table `fungsional`
--
ALTER TABLE `fungsional`
  ADD PRIMARY KEY (`id_fung`);

--
-- Indexes for table `grup`
--
ALTER TABLE `grup`
  ADD PRIMARY KEY (`id_grup`);

--
-- Indexes for table `hki`
--
ALTER TABLE `hki`
  ADD PRIMARY KEY (`id_hki`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id_jurnal`);

--
-- Indexes for table `leader`
--
ALTER TABLE `leader`
  ADD PRIMARY KEY (`id_leader`);

--
-- Indexes for table `narasumber`
--
ALTER TABLE `narasumber`
  ADD PRIMARY KEY (`id_narasumber`);

--
-- Indexes for table `non_formal`
--
ALTER TABLE `non_formal`
  ADD PRIMARY KEY (`id_non_formal`);

--
-- Indexes for table `org`
--
ALTER TABLE `org`
  ADD PRIMARY KEY (`id_org`);

--
-- Indexes for table `pangkat`
--
ALTER TABLE `pangkat`
  ADD PRIMARY KEY (`id_pangkat`);

--
-- Indexes for table `penelitian`
--
ALTER TABLE `penelitian`
  ADD PRIMARY KEY (`id_penelitian`);

--
-- Indexes for table `pengabdian`
--
ALTER TABLE `pengabdian`
  ADD PRIMARY KEY (`id_pengabdian`);

--
-- Indexes for table `penghargaan`
--
ALTER TABLE `penghargaan`
  ADD PRIMARY KEY (`id_penghargaan`);

--
-- Indexes for table `pribadi`
--
ALTER TABLE `pribadi`
  ADD PRIMARY KEY (`id_dosen`),
  ADD UNIQUE KEY `email1` (`username`);

--
-- Indexes for table `profpic`
--
ALTER TABLE `profpic`
  ADD PRIMARY KEY (`idImage`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `prosiding`
--
ALTER TABLE `prosiding`
  ADD PRIMARY KEY (`id_prosiding`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`);

--
-- Indexes for table `struktural`
--
ALTER TABLE `struktural`
  ADD PRIMARY KEY (`id_struk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dokumen`
--
ALTER TABLE `dokumen`
  MODIFY `idDokumen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `dokumen_achievement`
--
ALTER TABLE `dokumen_achievement`
  MODIFY `idDokumen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dokumen_pendidikan`
--
ALTER TABLE `dokumen_pendidikan`
  MODIFY `idDokumen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `formal`
--
ALTER TABLE `formal`
  MODIFY `id_formal` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fungsional`
--
ALTER TABLE `fungsional`
  MODIFY `id_fung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `hki`
--
ALTER TABLE `hki`
  MODIFY `id_hki` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id_jurnal` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `leader`
--
ALTER TABLE `leader`
  MODIFY `id_leader` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `narasumber`
--
ALTER TABLE `narasumber`
  MODIFY `id_narasumber` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `non_formal`
--
ALTER TABLE `non_formal`
  MODIFY `id_non_formal` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `org`
--
ALTER TABLE `org`
  MODIFY `id_org` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `pangkat`
--
ALTER TABLE `pangkat`
  MODIFY `id_pangkat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `penelitian`
--
ALTER TABLE `penelitian`
  MODIFY `id_penelitian` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pengabdian`
--
ALTER TABLE `pengabdian`
  MODIFY `id_pengabdian` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `penghargaan`
--
ALTER TABLE `penghargaan`
  MODIFY `id_penghargaan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pribadi`
--
ALTER TABLE `pribadi`
  MODIFY `id_dosen` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `profpic`
--
ALTER TABLE `profpic`
  MODIFY `idImage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `prosiding`
--
ALTER TABLE `prosiding`
  MODIFY `id_prosiding` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id_review` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `struktural`
--
ALTER TABLE `struktural`
  MODIFY `id_struk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
