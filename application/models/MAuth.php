<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class MAuth extends CI_Model {

		public function cek_user($data) {
			$query = $this->db->get_where('user', $data);
			return $query;
		}

		public function ifExist($username){
			$this->db->where('username',$username);
			$this->db->from('user');
			$count = $this->db->count_all_results(); 
			if($count==1){
				return true;
			} else {
				return false;
			}
		}

		public function resetPass($data, $id){
			$this->db->where('username', $id);
			$res = $this->db->update('user', $data);
			return $res;
		}

	}

?>