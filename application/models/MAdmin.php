<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAdmin extends CI_Model {
	
	public function insertDosen($data){
		$res = $this->db->insert('user', $data);
		return $res;
	}

	public function getDosen(){
		$this->db->where('grup', 2);
		$this->db->order_by('username', 'ASC');
		$res = $this->db->get('user');
		return $res->result_array();
	}

	public function getUser($params){
		$query = 'SELECT * FROM user WHERE username ="'.$params.'"';
        $data = $this->db->query($query);
		// $data = $this->db->query('select * from '.$table.' '.$where);
		return $data->result_array();
	}
	
	public function updateDataDosen($data, $id){
		// $res = $this->db->update($tabel, $data, $where);
		$this->db->where('username', $id);
		$res = $this->db->update('user', $data);
		// $res = $this->db->affected_rows();
		return $res;
	}

	public function updatePribadi($data, $id){
		// $res = $this->db->update($tabel, $data, $where);
		$this->db->where('username', $id);
		$res = $this->db->update('pribadi', $data);
		// $res = $this->db->affected_rows();
		return $res;
	}

	public function deleteDosen($tabel, $where){
		$res = $this->db->delete($tabel, $where);
		return $res;
	}

	public function countUser(){
		$query = 'SELECT * FROM user WHERE grup = 2 ';
        $data = $this->db->query($query);
		return $data->num_rows();
	}

	public function ifExist($username){
		$this->db->where('username',$username);
		$this->db->from('user');
		$count = $this->db->count_all_results(); 
		if($count==1){
			return true;
		} else {
			return false;
		}
	}

	public function resetPassDosen($data, $id){
		$this->db->where('username', $id);
		$res = $this->db->update('user', $data);
		return $res;
	}

	//count penulis
	public function countData($table){
		return $this->db->count_all($table);
	}

}