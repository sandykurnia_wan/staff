<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDosen extends CI_Model {
	public function getUser($params){
		$query = 'SELECT * FROM user WHERE username ="'.$params.'"';
        $res = $this->db->query($query);
		// $data = $this->db->query('select * from '.$table.' '.$where);
		return $res->result_array();
	}

	public function updateUser($data, $id){
		$this->db->where('username', $id);
		$res = $this->db->update('user', $data);
		return $res;
	}

	public function updateData($data, $id){
		$this->db->where('username', $id);
		$res = $this->db->update('pribadi', $data);
		return $res;
	}

	public function getData($table, $id){
		$this->db->where('username', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function addPend($table, $data){
		$res = $this->db->insert($table,$data);
	}

	public function getPend($table, $id){
		$this->db->where('username', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function updatePend($data, $idPend, $idKolom, $table){
		$this->db->where($idKolom, $idPend);
		$this->db->update($table);
		$res = $this->db->affected_rows();
		return $res;
	}

	public function getFormal($id){
		$this->db->where('username', $id);
		$this->db->order_by('jenjang', 'ASC');
		$res = $this->db->get('formal');
		return $res->result_array();
	}

	public function updateFormal($data, $id, $idPend){
		$this->db->where('username', $id);
		$this->db->where('id_formal', $idPend);
		$res = $this->db->update('formal', $data);
		return $res;
	}

	public function ifExistJenjang($id, $jenjang){
		$this->db->where('username', $id);
		$this->db->where('jenjang', $jenjang);
		$this->db->from('formal');
		$count = $this->db->count_all_results();
		// return $this->db->count_all_results();
		if($count > 0){
			return true;
		} else {
			return false;
		}
	}

	public function cekTahunAkhir($thn_masuk, $thn_lulus){
		if($thn_lulus <= $thn_masuk){
			return false;
		} else {
			return true;
		}
	}	

	public function getNonFormal($id){
		$this->db->where('username', $id);
		$this->db->order_by('nama_kegiatan','ASC');
		$res = $this->db->get('non_formal');
		return $res->result_array();
	}

	public function updateNonFormal($data, $id, $idPend){
		$this->db->where('username', $id);
		$this->db->where('id_non_formal', $idPend);
		//print_r($_POST);die();
		$res = $this->db->update('non_formal', $data);
		return $res;
	}

	public function delPend($idPend, $idKolom, $table){
		$this->db->where($idKolom, $idPend);
		$this->db->delete($table);
		$res = $this->db->affected_rows();
		return $res;
	}

	public function addAchievement($table, $data){
		$res = $this->db->insert($table,$data);
	}

	public function getAchievement($table, $id){
		$this->db->where('username', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function getHKI($id, $judul){
		$this->db->select('id_hki');
		$this->db->where('username', $id);
		$this->db->where('judul_hki', $judul);
		$res = $this->db->get('hki');
		return $res->result_array();
	}

	public function getJurnal($id, $judul){
		$this->db->select('id_jurnal');
		$this->db->where('username', $id);
		$this->db->where('judul_jurnal', $judul);
		$res = $this->db->get('jurnal');
		return $res->result_array();
	}

	public function getPenelitian($id, $judul){
		$this->db->select('id_penelitian');
		$this->db->where('username', $id);
		$this->db->where('judul_penelitian', $judul);
		$res = $this->db->get('penelitian');
		$this->db->order_by('tahun', 'ASC');
		return $res->result_array();
	}

	public function getPengabdian($id, $judul){
		$this->db->select('id_pengabdian');
		$this->db->where('username', $id);
		$this->db->where('judul_pengabdian', $judul);
		$res = $this->db->get('pengabdian');
		return $res->result_array();
	}

	public function getPenghargaan($id, $judul){
		$this->db->select('id_penghargaan');
		$this->db->where('username', $id);
		$this->db->where('nama_penghargaan', $judul);
		$res = $this->db->get('penghargaan');
		return $res->result_array();
	}

	public function getProsiding($id, $judul){
		$this->db->select('id_prosiding');
		$this->db->where('username', $id);
		$this->db->where('judul_artikel', $judul);
		$res = $this->db->get('prosiding');
		return $res->result_array();
	}

	public function delAch($idAch, $idKolom, $table){
		$this->db->where($idKolom, $idAch);
		$this->db->delete($table);
		$res = $this->db->affected_rows();
		return $res;
	}

	public function updateAch($data, $idAch, $idKolom, $table){
		$this->db->where($idKolom, $idAch);
		$this->db->update($table);
		$res = $this->db->affected_rows();
		return $res;
	}

	public function updateHKI($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_hki', $idAch);
		$res = $this->db->update('hki', $data);
		return $res;
	}

	public function updateJurnal($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_jurnal', $idAch);
		$res = $this->db->update('jurnal', $data);
		return $res;
	}

	public function updateProsiding($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_prosiding', $idAch);
		$res = $this->db->update('prosiding', $data);
		return $res;
	}

	public function updatePenelitian($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_penelitian', $idAch);
		$res = $this->db->update('penelitian', $data);
		return $res;
	}

	public function updatePengabdian($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_pengabdian', $idAch);
		$res = $this->db->update('pengabdian', $data);
		return $res;
	}

	public function updatePenghargaan($data, $id, $idAch){
		$this->db->where('username', $id);
		$this->db->where('id_penghargaan', $idAch);
		$res = $this->db->update('penghargaan', $data);
		return $res;
	}

	public function insertDokumenAch($data){
		$res = $this->db->insert('dokumen_achievement',$data);
	}

	public function getDokumenAch($id, $jenisAch, $idAch){
		$this->db->where('username', $id);
		$this->db->where('jenisAch', $jenisAch);
		$this->db->where('idAch', $idAch);
		$res = $this->db->get('dokumen_achievement');
		return $res->result_array();
	}

	public function getDokumenbyIdAch($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$res = $this->db->get('dokumen_achievement');
		return $res->result_array();
	}

	public function delDokumenAch($idAch, $jenisAch){
		$this->db->where('idAch', $idAch);
		$this->db->where('jenisAch', $jenisAch);
		$this->db->delete('dokumen_achievement');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function delDokumenbyIdAch($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$this->db->delete('dokumen_achievement');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function getFileAch($jenisAch, $idAch){
		$this->db->where('jenisAch', $jenisAch);
		$this->db->where('idAch', $idAch);
		$res = $this->db->get('dokumen_achievement');
		return $res->result_array();
	}

	public function insertDokumenPend($data){
		$res = $this->db->insert('dokumen_pendidikan',$data);
	}

	public function getDokumenPend($id, $jenisPend, $idPend){
		$this->db->where('username', $id);
		$this->db->where('jenisPend', $jenisPend);
		$this->db->where('idPend', $idPend);
		$res = $this->db->get('dokumen_pendidikan');
		return $res->result_array();
	}

	public function getDokumenbyIdPend($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$res = $this->db->get('dokumen_pendidikan');
		return $res->result_array();
	}

	public function delDokumenPend($idPend, $jenisPend){
		$this->db->where('idPend', $idPend);
		$this->db->where('jenisPend', $jenispend);
		$this->db->delete('dokumen_pendidikan');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function delDokumenbyIdPend($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$this->db->delete('dokumen_pendidikan');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function getFilePend($jenisPend, $idPend){
		$this->db->where('jenisPend', $jenisPend);
		$this->db->where('idPend', $idPend);
		$res = $this->db->get('dokumen_pendidikan');
		return $res->result_array();
	}

	public function countAch($namatabel, $id){
		$this->db->from($namatabel);
		$total = $this->db->count_all_results();
		
		$this->db->where('username', $id);
		$this->db->from($namatabel);
		$totalId = $this->db->count_all_results();
		
		if($total==0 || $totalId==0){
			$persen = 0;
		} else {
			$persen = ($totalId/$total)*100;
		}
		$ret = [$totalId, $total, $persen];
		return $ret;
	}

//==============================================================================================================================================================================================================================================================================================================================================================================================================================================================================================================

	
	public function updateDeskripsi($id, $data){
		$this->db->where('username', $id);
		$res = $this->db->update('pribadi', $data);
		return $res;
	}
	
	public function getDeskripsi($id){
		$this->db->select('deskripsi');
		$this->db->where('username', $id);
		$res = $this->db->get('pribadi');
		return $res->result_array();
	}

	public function addActivity($table, $data){
		$res = $this->db->insert($table,$data);
	}

	public function getActivity($table, $id){
		$this->db->where('username', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function getIdOrg($id, $judul){
		$this->db->select('id_org');
		$this->db->where('username', $id);
		$this->db->where('judul', $judul);
		$res = $this->db->get('org');
		return $res->result_array();
	}
	// buat get lainnya

	//buat get file
	public function getFile($jenisAct, $idAct){
		$this->db->where('jenisAct', $jenisAct);
		$this->db->where('idAct', $idAct);
		$res = $this->db->get('dokumen');
		return $res->result_array();
	}

	public function updateOrg($data, $id, $idAct){
		$this->db->where('username', $id);
		$this->db->where('id_org', $idAct);
		$res = $this->db->update('org', $data);
		return $res;
	}
	public function updateBuku($data, $id, $idAct){
		$this->db->where('username', $id);
		$this->db->where('id_buku', $idAct);
		$res = $this->db->update('buku', $data);
		return $res;
	}
	public function updateRev($data, $id, $idAct){
		$this->db->where('username', $id);
		$this->db->where('id_review', $idAct);
		$res = $this->db->update('review', $data);
		return $res;
	}
	public function updateNara($data, $id, $idAct){
		$this->db->where('username', $id);
		$this->db->where('id_narasumber', $idAct);
		$res = $this->db->update('narasumber', $data);
		return $res;
	}
	public function updateLeader($data, $id, $idAct){
		$this->db->where('username', $id);
		$this->db->where('id_leader', $idAct);
		$res = $this->db->update('leader', $data);
		return $res;
	}

	//buat Dokumen
	public function insertDokumen($data){
		$res = $this->db->insert('dokumen',$data);
	}

	public function getDokumen($id, $jenisAct, $idAct){
		$this->db->where('username', $id);
		$this->db->where('jenisAct', $jenisAct);
		$this->db->where('idAct', $idAct);
		$res = $this->db->get('dokumen');
		return $res->result_array();
	}

	public function getDokumenbyId($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$res = $this->db->get('dokumen');
		return $res->result_array();
	}

	public function delDokumen($idAct, $jenisAct){
		$this->db->where('idAct', $idAct);
		$this->db->where('jenisAct', $jenisAct);
		$this->db->delete('dokumen');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function delDokumenbyId($idDoc){
		$this->db->where('idDokumen', $idDoc);
		$this->db->delete('dokumen');
		$res = $this->db->affected_rows();
		return $res;
	}

	public function delAct($idAct, $idKolom,$table){
		$this->db->where($idKolom, $idAct);
		$this->db->delete($table);
		$res = $this->db->affected_rows();
		return $res;
	}

	public function addJabatan($table, $data){
		$res = $this->db->insert($table,$data);
	}

	public function getJabatan($table, $id){
		$this->db->where('username', $id);
		$res = $this->db->get($table);
		return $res->result_array();
	}

	public function editJabatan($table, $kolom, $idjab,$data){
		$this->db->where($kolom, $idjab);
		$this->db->update($table, $data);
		return $this->db->affected_rows();
	}

	public function getPangkat($id){
		$this->db->where('username', $id);
		$this->db->order_by('gol_ruang', 'ASC');
		$res = $this->db->get('pangkat');
		return $res->result_array();
	}

	public function ifExistPangkat($id, $jenis){
		$this->db->where('username', $id);
		$this->db->where('jenis', $jenis);
		$this->db->from('pangkat');
		$count = $this->db->count_all_results();
		// return $this->db->count_all_results();
		if($count > 0){
			return true;
		} else {
			return false;
		}
	}

	public function getFung($id){
		$this->db->where('username', $id);
		$this->db->order_by('awal_fung', 'DESC');
		$res = $this->db->get('fungsional');
		return $res->result_array();
	}

	public function getFungFront($id){
		$this->db->where('username', $id);
		$this->db->order_by('awal_fung', 'DESC');
		$this->db->limit(1);
		$res = $this->db->get('fungsional');
		return $res->result_array();
	}

	public function ifExistFung($id, $jabatan){
		$this->db->where('username', $id);
		$this->db->where('jab_fung', $jabatan);
		$this->db->from('fungsional');
		$count = $this->db->count_all_results();
		// return $this->db->count_all_results();
		if($count > 0){
			return true;
		} else {
			return false;
		}
	}

	public function getLog($id){
		$this->db->select('logged');
		$this->db->where('username', $id);
		$res = $this->db->get('user');
		return $res->result_array();
	}

	public function addProfPic($data){
		$res = $this->db->insert('profpic',$data);
	}

	public function updateProfPic($data, $id){
		$this->db->where('username', $id);
		$res = $this->db->update('profpic', $data);
		return $res;
	}
	public function ifExistProfPic($id){
		$this->db->where('username', $id);
		$this->db->from('profpic');
		return $this->db->count_all_results();
	}

	public function getProfPic($id){
		$this->db->where('username', $id);
		$res = $this->db->get('profpic');
		return $res->result_array();
	}

	public function getIdProfil($id){
		$this->db->select('id_dosen');
		$this->db->where('username', $id);
		$res = $this->db->get('pribadi');
		return $res->result_array()[0]['id_dosen'];
	}

}