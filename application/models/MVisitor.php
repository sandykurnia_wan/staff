<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MVisitor extends CI_Model {

	public function getDataDosen(){
		// $this->db->where('grup', 2);
		$this->db->order_by('nip', 'ASC');
		$res = $this->db->get('pribadi');
		return $res->result_array();
	}

	public function getId($idDosen){
		$this->db->where('id_dosen', $idDosen);
		$this->db->select('username');
		$res = $this->db->get('pribadi');
		return $res->result_array();
	}
}