<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitor extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mvisitor');
		$this->load->model('mdosen');
	}

	public function index()
	{
		$data['title']='Home Page | Visitor';
		$data['dosen'] = $this->mvisitor->getDataDosen();
		$this->template->load('template','visitor/home',$data);
	}

	public function profile($idDosen){
		$res = $this->mvisitor->getId($idDosen);
		if(empty($res)){
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Dosen belum terdaftar</strong>	</div>' );
			redirect('visitor');
		}
		$id = $res[0]['username'];
		$ppdata = $this->mdosen->getProfpic($id);
		if(empty($ppdata)){
			$data['profpic'] = 'dosen.png';
		}else{
			$data['profpic'] = $ppdata[0]['filename'];
		}
        $data['pribadi'] = $this->mdosen->getData('pribadi', $id);
        $data['fungakhir'] = $this->mdosen->getFungFront($id);
		$data['formal'] = $this->mdosen->getFormal($id);
		$data['non_formal'] = $this->mdosen->getNonFormal($id);
		$data['fungsional'] = $this->mdosen->getJabatan('fungsional', $id);
		$data['struktural'] = $this->mdosen->getJabatan('struktural', $id);
		$data['pangkat'] = $this->mdosen->getJabatan('pangkat', $id);
		$data['org'] = $this->mdosen->getActivity('org', $id);
		$data['buku'] = $this->mdosen->getActivity('buku', $id);
		$data['review'] = $this->mdosen->getActivity('review', $id);
		$data['narasumber'] = $this->mdosen->getActivity('narasumber', $id);
		$data['leader'] = $this->mdosen->getActivity('leader', $id);
		$data['hki'] = $this->mdosen->getAchievement('hki', $id);
		$data['jurnal'] = $this->mdosen->getAchievement('jurnal', $id);
		$data['prosiding'] = $this->mdosen->getAchievement('prosiding', $id);
		$data['penelitian'] = $this->mdosen->getAchievement('penelitian', $id);
		$data['pengabdian'] = $this->mdosen->getAchievement('pengabdian', $id);
		$data['penghargaan'] = $this->mdosen->getAchievement('penghargaan', $id);
		$data['persen_hki'] = $this->mdosen->countAch('hki', $id);
		$data['persen_jurnal'] = $this->mdosen->countAch('jurnal', $id);
		$data['persen_prosiding'] = $this->mdosen->countAch('prosiding', $id);
		$data['persen_penelitian'] = $this->mdosen->countAch('penelitian', $id);
		$data['persen_pengabdian'] = $this->mdosen->countAch('pengabdian', $id);
		$data['persen_penghargaan'] = $this->mdosen->countAch('penghargaan', $id);
		// print_r($data);die;
		$this->load->view('tema',$data);
	}
}
