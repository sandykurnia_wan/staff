<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mauth');
		$this->load->helper('text');
		$this->load->helper('string');
		$this->load->library('email');
	}

	public function index() {
		if(!empty($this->session->userdata())){
			if ($this->session->userdata('grup') == '1') {
				redirect('admin');
			} else if ($this->session->userdata('grup') == '2') {
				redirect('dosen');
			}
		}
		$this->load->view('login');
	}

	public function cek_login() {
		$data = array(
			'username' => $this->input->post('username', TRUE),
			'password' => md5($this->input->post('password', TRUE))
		);
		$this->load->model('mauth'); // load model
		$hasil = $this->mauth->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = $sess->logged;
				$sess_data['iduser'] = $sess->iduser;
				$sess_data['username'] = $sess->username;
				$sess_data['name'] = $sess->name;
				$sess_data['grup'] = $sess->grup;
				$this->session->set_userdata($sess_data);
			}
			if ($this->session->userdata('grup') == '1') {
				redirect('admin');
			}
			else if ($this->session->userdata('grup') == '2') {
				if ($this->session->userdata('logged_in') == '0'){
					redirect('dosen/firstlogin');
				} else {
					redirect('dosen');
				}				
			}
		
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

	public function lupaPassword(){
		$id=$this->input->post('username');
		$cek = $this->mauth->ifexist($id);
		if($cek){
			$pass = random_string('alnum',8);
			$data_insert = array(
					'password' 	=> md5($pass),	'logged' 	=> 0
				);
			$res = $this->mauth->resetPass($data_insert,$id);
			$this->resetEmail($id, $pass);
			echo "<script>alert('Password telah di reset, silahkan cek email anda');history.go(-1);</script>";
		}else{
			echo "<script>alert('Username Anda belum terdaftar');history.go(-1);</script>";
		}
	}

	public function resetEmail($to, $pass){
		$login = base_url('auth');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer(true);
        try{
        	$mail->IsSMTP(); // we are going to use SMTP
	        $mail->SMTPAuth   = true; // enabled SMTP authentication
	        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
	        $mail->SMTPOptions = array(
								    'ssl' => array(
								        'verify_peer' => false,
								        'verify_peer_name' => false,
								        'allow_self_signed' => true
								    )
								);
	        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
	        $mail->Port       = 587;                   // SMTP port to connect to GMail
	        $mail->Username   = "staff.ifundip@gmail.com";  // user email address
	        $mail->Password   = "staffifundip";            // password in GMail
	        $mail->SetFrom('staff.ifundip@gmail.com', 'Admin Staff Site Informatika');  //Who is sending 
	        $mail->isHTML(true);
	        $mail->Subject    = "Reset Password Akun Staff Site";
	        $mail->Body      = "
	            <html>
	            <head>
	                <title>Password Reset</title>
	            </head>
	            <body>
	            <h3>Staff Site Informatika Universitas Diponegoro</h3>
	            <br>
	            <p>Password akun Anda telah direset, berikut informasi baru mengenai akun Anda:</p>
	            <table>
	            	<tr>
	            		<td>Username</td>
	            		<td>:</td>
	            		<td>$to</td>
	            	</tr>
	            	<tr>
	            		<td>Password Baru</td>
	            		<td>:</td>
	            		<td>$pass</td>
	            	</tr>
	            </table>
	            <p>Anda dapat melakukan login dengan mengakses link berikut: <a href='$login'>Staff Site</a></p>
	            <p>Mohon diperhatikan bahwa saat Anda login dengan password yang baru, Anda akan diminta untuk mengganti password dan mengupload foto profil kembali.</p>
	            <p>Terima Kasih</p>
	            </body>
	            </html>
	        ";
	        // Who is addressed the email to
	        $mail->AddAddress($to, $to);
	        $mail->Send();
	        
        } catch(phpmailerException $e){
        	echo $e->errorMessage();
        } catch(Exception $e){
        	echo $e->getMessage();
        }
    }
	
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}

?>