<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Uploadfile extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    //index function
    function index()
    {
        //load file upload form
        $this->load->view('upload_file_view');
    }

    //file upload function
    function upload(){
        //set preferences
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'txt|pdf|png|jpg|gif';
        $config['max_size']    = '2048';

        //load upload class library
        $this->load->library('upload', $config);
        $count = count($_FILES['filename']['name']);
        // print_r($count);die;
        for($i=0;$i<$count;$i++){
            $_FILES['upload']['name'] = $_FILES['filename']['name'][$i];
            $_FILES['upload']['type'] = $_FILES['filename']['type'][$i];
            $_FILES['upload']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
            $_FILES['upload']['error'] = $_FILES['filename']['error'][$i];
            $_FILES['upload']['size'] = $_FILES['filename']['size'][$i];
            if($this->upload->do_upload('upload')){
                $fileData = $this->upload->data();
                $uploadData[$i]['filename'] = $fileData['file_name'];
            }
        }

        print_r($uploadData);die;

        // if (!$this->upload->do_upload('upload'))
        // {
        //     // case - failure
        //     $upload_error = array('error' => $this->upload->display_errors());
        //     $this->load->view('upload_file_view', $upload_error);
        // }
        // else
        // {
        //     // case - success
        //     $upload_data = $this->upload->data(); print_r($upload_data);die;
        //     // $data['success_msg'] = '<div class="alert alert-success text-center">Your file <strong>' . $upload_data['file_name'] . '</strong> was successfully uploaded!</div>';
        //     // $this->load->view('upload_file_view', $data);
        //     $this->session->set_flashdata('pesan', 
        //                 '<div class="alert alert-success" role="alert">
        //                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        //                 <strong>Foto berhasil diganti</strong>
        //                 </div>');
        //     redirect('dosen');
        // }
    }
}
?>