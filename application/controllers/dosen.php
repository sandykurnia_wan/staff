<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	protected $sukses = '<div class="alert alert-success" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data berhasil ditambahkan</strong>	</div>';
	protected $gagal = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data gagal ditambahkan </strong> </div>';
	protected $sukseshapus = '<div class="alert alert-success" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data berhasil dihapus</strong>	</div>';
	protected $gagalhapus = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data gagal dihapus </strong> </div>';
	protected $suksesubah = '<div class="alert alert-success" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data berhasil diubah</strong>	</div>';
	protected $gagalubah = '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Data gagal diubah </strong> </div>';

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
		} else if ($this->session->userdata('grup') == '1') {
			redirect('admin');
		} 
		$this->load->helper('text');
		$this->load->model('mdosen');
		$this->load->helper(array('form', 'url'));
	}

	public function index() {
		$id = $this->session->userdata('username');
		$data['name'] = $this->session->userdata('name');
		$data['title']='Home page | Dosen';
		$data['deskripsi'] = $this->mdosen->getDeskripsi($id);
		// print_r($data['deskripsi']);die;
		$this->template->load('TDosen','dosen/dashboard',$data);
	}

	public function firstLogin() {
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Dosen';
		$this->template->load('TBlank','dosen/first',$data);
	}

	public function firstUpdate(){
		$id = $this->session->userdata('username');
		$ppdata = $this->mdosen->getProfpic($id);
		// print_r($ppdata);
        if(empty($ppdata)){
            $path = $_FILES['filename']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			//set preferences
	        $config['upload_path'] = './uploads/profilePhoto/';
	        $config['allowed_types'] = 'png|jpg|gif';
	        $config['max_size'] = '2024';
	        $config['file_name'] = $id.'.'.$ext;
	        $config['overwrite'] = true;
	        // print_r($config['file_name']);die;
	        // echo $this->mdosen->ifExistProfPic($id);die;
	        
	        //load upload class library
	        $this->load->library('upload', $config);
	        if (!$this->upload->do_upload('filename')){
	            // case - failure
	            $upload_error = array('error' => $this->upload->display_errors());
	            // print_r($upload_error);die;
	            $this->session->set_flashdata('pesan', 
	                        '<div class="alert alert-danger" role="alert">
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                        '.$upload_error['error'].'
	                        </div>');
	            // print_r('gagal');die;
	            redirect('dosen/firstLogin');
	            // $this->load->view('upload_file_view', $upload_error);
	        } else {
	            // case - success
	            $upload_data = $this->upload->data(); //print_r($upload_data);die;
	            $id = $this->session->userdata('username');
	            $filename = $upload_data['file_name'];
	           	
				if ($this->mdosen->ifExistProfPic($id) == 0) {
					$data = array('username' 	=> $id, 	'filename'		=> $filename);
					$newpass = $this->input->post('password');
					$oldpass = '12345678';
					$id = $this->session->userdata('username');
					$data_update = array(
						'password' => md5($newpass),
						'logged'=> 1
			 		);
					$res1 = $this->mdosen->updateUser($data_update, $id);
					$res2 = $this->mdosen->addProfPic($data);
					if (!$res2){
						$this->session->set_flashdata('pesan', $this->sukses );
						redirect('dosen');
					} else {
						$this->session->set_flashdata('pesan', $this->gagal );
						redirect('dosen/firstLogin');
					}
					// echo 'haha';
				} else {
					$data = array('filename'	=> $filename);
					$newpass = $this->input->post('password');
					$id = $this->session->userdata('username');
					$data_update = array(
						'password' => md5($newpass),
						'logged'=> 1
			 		);
					$res1 = $this->mdosen->updateUser($data_update, $id);
					$res2 = $this->mdosen->updateProfpic($data, $id);
					if ($res2){
						$this->session->set_flashdata('pesan', $this->sukses );
						redirect('dosen');
					} else {
						$this->session->set_flashdata('pesan', $this->gagal );
						redirect('dosen/firstLogin');
					}
				} 
	        }
        }else{
			$newpass = $this->input->post('password');
			$id = $this->session->userdata('username');
			$data_update = array(
				'password' => md5($newpass),
				'logged'=> 1
			 );
			$res1 = $this->mdosen->updateUser($data_update, $id);
			if ($res1){
				$this->session->set_flashdata('pesan', $this->sukses );
				redirect('dosen');
			} else {
				$this->session->set_flashdata('pesan', $this->gagal );
				redirect('dosen/firstLogin');
			}
        }
		
	}

	public function changePassword(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Dosen';
		$this->template->load('TDosen','dosen/password',$data);
	}

	public function updatePassword(){
		$newpass = $this->input->post('password');
		$oldpass = '12345678';
		$id = $this->session->userdata('username');
		$data_update = array(
			'password' => md5($newpass),
			'logged'=> 1
 		);

		if ($oldpass !== $newpass){
			$res = $this->mdosen->updateUser($data_update, $id);

			if ($res){
				$this->session->set_flashdata('pesan', 
						'<div class="alert alert-success" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>Password berhasil diubah</strong>
						</div>');
				redirect('dosen');
			} else {
				$this->session->set_flashdata('pesan', 
						'<div class="alert alert-danger" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>Password gagal diubah</strong>
						</div>');
				redirect('dosen/changePassword');
			}
		} else {
			$this->session->set_flashdata('pesan', 
						'<div class="alert alert-danger" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>Password sama</strong>
						</div>');
			redirect('dosen/changePassword');
		}
	}

	// public function 
	public function dataPribadi(){
		$id = $this->session->userdata('username');
		$data['dosen']=$this->mdosen->getData('pribadi', $id);
		$data['name'] = $this->session->userdata('name');
		$data['title']='Data Pribadi | Dosen';
		$data['pangkat'] = $this->mdosen->getPangkat($id);
		$data['struktural'] = $this->mdosen->getJabatan('struktural',$id);
		$data['fungsional'] = $this->mdosen->getFung($id);
		$this->template->load('TDosen','dosen/detail_pribadi',$data);
	}

	public function riwayatPend(){
		$id = $this->session->userdata('username');
		$data['formal']=$this->mdosen->getData('formal', $id);
		$data['non_formal']=$this->mdosen->getData('non_formal', $id);
		$data['dt_formal'] = $this->mdosen->getFormal($id);
		$data['dt_non_formal'] = $this->mdosen->getNonFormal($id);
		$data['name'] = $this->session->userdata('name');
		$data['title']='Riwayat Pendidikan | Dosen';
		$this->template->load('TDosen','dosen/detail_pend',$data);
	}

	public function activity(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Activity | Dosen';
		//ngambil data riwayat pendidikan
		$id = $this->session->userdata('username');
		$data['org'] = $this->mdosen->getActivity('org',$id);
		$data['buku'] = $this->mdosen->getActivity('buku',$id);
		$data['rev'] = $this->mdosen->getActivity('review',$id);
		$data['nara'] = $this->mdosen->getActivity('narasumber',$id);
		$data['leader'] = $this->mdosen->getActivity('leader',$id);
		$this->template->load('TDosen','dosen/detail_activity',$data);
	}

	public function achievement(){
		//ngambil data riwayat pendidikan
		$data['name'] = $this->session->userdata('name');
		$data['title']='Achievement | Dosen';
		$id = $this->session->userdata('username');
		$data['hki'] = $this->mdosen->getAchievement('hki',$id);
		$data['jurnal'] = $this->mdosen->getAchievement('jurnal',$id);
		$data['penelitian'] = $this->mdosen->getAchievement('penelitian',$id);
		$data['pengabdian'] = $this->mdosen->getAchievement('pengabdian',$id);
		$data['penghargaan'] = $this->mdosen->getAchievement('penghargaan',$id);
		$data['prosiding'] = $this->mdosen->getAchievement('prosiding',$id);
		$this->template->load('TDosen','dosen/detail_achievement',$data);
	}

	public function detformal(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='User Table | Dosen';
		$this->template->load('TDosen','vdetail_pend_formal',$data);
	}

	public function updatepribadi(){
		$no_ktp = $this->input->post('no_ktp');
		$tmpt_lahir = $this->input->post('tmpt_lahir');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$almt_rmh = $this->input->post('almt_rmh');
		$hp1 = $this->input->post('hp1');
		$hp2 = $this->input->post('hp2');
		$data_update = array(
			'no_ktp' 		=> $no_ktp,		'tmpt_lahir' 	=> $tmpt_lahir, 	
			'tgl_lahir' 	=> $tgl_lahir,	'almt_rmh' 		=> $almt_rmh,
			'hp1' 			=> $hp1,		'hp2' 			=> $hp2
			);
		$id = $this->session->userdata('username');
		$res = $this->mdosen->updateData($data_update, $id);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen/dataPribadi');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/dataPribadi');
		}
	}

	public function updatekantor(){
		$nm_dosen = $this->input->post('nm_dosen');
		$glr_dpn = $this->input->post('glr_dpn');
		$glr_blkg = $this->input->post('glr_blkg');
		$almt_ktr = $this->input->post('almt_ktr');
		$no_tlp = $this->input->post('no_tlp');
		$lab = $this->input->post('lab');
		$fax = $this->input->post('fax');
		$email = $this->input->post('email');
		$npwp = $this->input->post('npwp');
		$no_krt_peg = $this->input->post('no_krt_peg');
		$nip = $this->input->post('nip');
		$nidn = $this->input->post('nidn');
		$data_update = array(
			'nm_dosen' 		=> $nm_dosen, 	'glr_dpn' 		=> $glr_dpn,
			'glr_blkg' 		=> $glr_blkg,	'almt_ktr' 		=> $almt_ktr,	
			'no_tlp' 		=> $no_tlp,		'lab' 			=> $lab,
			'fax' 			=> $fax, 		'nip' 			=> $nip,
			'email' 		=> $email,	 	'npwp' 			=> $npwp,
			'no_krt_peg' 	=> $no_krt_peg,	'nidn' 			=> $nidn,
			);
		$update_user = array('name' => $nm_dosen);
		$id = $this->session->userdata('username');
		$res2 = $this->mdosen->updateUser($update_user, $id);
		$res = $this->mdosen->updateData($data_update, $id);
		if ($res){
			$this->session->set_userdata('name', $nm_dosen);
			$this->session->set_flashdata('pesan', $this->suksesubah);
			redirect('dosen/dataPribadi');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah);
			redirect('dosen/dataPribadi');
		}
	}

	public function updatelain(){
		$scopus_id = $this->input->post('scopus_id');
		$rgate = $this->input->post('rgate');
		// print_r($rgate);die();
		$interest = $this->input->post('interest');
		$scholar = $this->input->post('scholar');
		$sinta = $this->input->post('sinta');
		$data_update = array(
			'scopus_id' 	=> $scopus_id, 	'rgate' 		=> $rgate,
			'interest' 		=> $interest,	'scholar'		=> $scholar,
			'sinta'			=> $sinta
			);
		$id = $this->session->userdata('username');
		$res = $this->mdosen->updateData($data_update, $id);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah);
			redirect('dosen/dataPribadi');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah);
			redirect('dosen/dataPribadi');
		}
	}

	public function addFormal(){
		$id = $this->session->userdata('username');
		$jenjang = $this->input->post('jenjang');
		$nama_univ = $this->input->post('nama_univ');
		$bidang = $this->input->post('bidang');
		$thn_masuk = $this->input->post('thn_masuk');
		$thn_lulus = $this->input->post('thn_lulus');
		$judul_ta = $this->input->post('judul_ta');
		$dosbing1 = $this->input->post('dosbing1');
		$dosbing2 = $this->input->post('dosbing2');
		$data_update = array(
			'username' 		=> $id,			'jenjang' 		=> $jenjang,
			'nama_univ'		=> $nama_univ,	'bidang' 		=> $bidang,
			'thn_masuk'		=> $thn_masuk,	'thn_lulus' 	=> $thn_lulus,
			'judul_ta' 		=> $judul_ta,	'dosbing1' 		=> $dosbing1,
			'dosbing2' 		=> $dosbing2
			);
		$cek = $this->mdosen->ifExistJenjang($id, $jenjang);
		$cek_tahun = $this->mdosen->cekTahunAkhir($thn_masuk, $thn_lulus);
		if($cek_tahun){
			if(!$cek){	
				$res = $this->mdosen->addPend('formal', $data_update);
				if (!$res){
					$this->session->set_flashdata('pesan', $this->sukses);
					redirect('dosen/riwayatPend');
				} else {
					$this->session->set_flashdata('pesan', $this->gagal);
					redirect('dosen/riwayatPend');
				}
			} else {
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Jenjang yang akan ditambahkan sudah ada </strong> </div>' );
				redirect('dosen/riwayatPend');
			}
		} else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Cek ulang tahun lulus! </strong> </div>' );
				redirect('dosen/riwayatPend');
		}
	}

	public function editFormal(){
		$idPend = $this->input->post('id_formal');
		$id = $this->session->userdata('username');
		$jenjang = $this->input->post('jenjang');
		$nama_univ = $this->input->post('nama_univ');
		$bidang = $this->input->post('bidang');
		$thn_masuk = $this->input->post('thn_masuk');
		$thn_lulus = $this->input->post('thn_lulus');
		$judul_ta = $this->input->post('judul_ta');
		$dosbing1 = $this->input->post('dosbing1');
		$dosbing2 = $this->input->post('dosbing2');
		$data_update = array(
			'jenjang' 		=> $jenjang,	'nama_univ'		=> $nama_univ,
			'bidang' 		=> $bidang,		'thn_masuk'		=> $thn_masuk,
			'thn_lulus' 	=> $thn_lulus,	'judul_ta' 		=> $judul_ta,
			'dosbing1' 		=> $dosbing1,	'dosbing2' 		=> $dosbing2
			);
		$cek_tahun = $this->mdosen->cekTahunAkhir($thn_masuk, $thn_lulus);
		if($cek_tahun){
			$res = $this->mdosen->updateFormal($data_update, $id, $idPend);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukses);
				redirect('dosen/riwayatPend');
			} else {
				$this->session->set_flashdata('pesan', $this->gagal);
				redirect('dosen/riwayatPend');
			}
		} else{
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Cek ulang tahun lulus! </strong> </div>' );
				redirect('dosen/riwayatPend');
		}
	}

	public function delFormal($idPend = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_formal';
		$table = 'formal';
		$file = $this->mdosen->getDokumen($id, $table, $idPend);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delPend($idPend, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idPend, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/riwayatPend');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/riwayatPend');
			}
		} else {
			$res = $this->mdosen->delPend($idPend, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/riwayatPend');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/riwayatPend');
			}
		}
	}

	public function addNonFormal(){
		$id = $this->session->userdata('username');
		$nama_kegiatan = $this->input->post('nama_kegiatan');
		$tgl_kegiatan = $this->input->post('tgl_kegiatan');
		$tgl_kegiatan_2 = $this->input->post('tgl_kegiatan_2');
		$penyelenggara = $this->input->post('penyelenggara');
		$tempat = $this->input->post('tempat');
		$pola = $this->input->post('pola');
		$data_update = array(
			'username' 		=> $id,				'nama_kegiatan' => $nama_kegiatan,	
			'tgl_kegiatan'	=> $tgl_kegiatan,	'tgl_kegiatan_2'=> $tgl_kegiatan_2,
			'penyelenggara' => $penyelenggara,	'tempat'		=> $tempat,
			'pola'			=> $pola
			);
		$res = $this->mdosen->addPend('non_formal', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/riwayatPend');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/riwayatPend');
		}
	}

	public function editNonFormal(){
		$idPend = $this->input->post('id_non_formal');
		$id = $this->session->userdata('username');
		$nama_kegiatan = $this->input->post('nama_kegiatan');
		$tgl_kegiatan = $this->input->post('tgl_kegiatan');
		$tgl_kegiatan_2 = $this->input->post('tgl_kegiatan_2');
		$penyelenggara = $this->input->post('penyelenggara');
		$tempat = $this->input->post('tempat');
		$pola = $this->input->post('pola');
		$data_update = array(
			'nama_kegiatan' => $nama_kegiatan,	'tgl_kegiatan'	=> $tgl_kegiatan,
			'tgl_kegiatan_2'=> $tgl_kegiatan_2,	'penyelenggara' => $penyelenggara,
			'tempat'		=> $tempat,			'pola' 			=> $pola
			);
		//print_r($data_update);die;
		$res = $this->mdosen->updateNonFormal($data_update, $id, $idPend);
		//print_r($data_update);die;
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/riwayatPend');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/riwayatPend');
		}
	}

	public function delNonFormal($idPend = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_non_formal';
		$table = 'non_formal';
		$file = $this->mdosen->getDokumen($id, $table, $idPend);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delPend($idPend, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idPend, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/riwayatPend');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/riwayatPend');
			}
		} else {
			$res = $this->mdosen->delPend($idPend, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/riwayatPend');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/riwayatPend');
			}
		}
	}

	public function addDokumenPend(){
		$id = $this->session->userdata('username');
		//set preferences
        $config['upload_path'] = './uploads/dokumen/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2024';
        $config['overwrite'] = false;
        //load upload class library
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('docs')){
        	$fileData = $this->upload->data();
        	$uploadData['username'] = $id;
		    $uploadData['jenisPend'] = $this->input->post('jenisPend');
		    $uploadData['idPend'] = $this->input->post('idPend');
	        $uploadData['filename'] = $fileData['file_name'];
        	$file = $this->mdosen->insertDokumenPend($uploadData);
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/riwayatPend');
        } else {
        	// case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('pesan', 
                        '<div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$upload_error['error'].'
                        </div>');
            redirect('dosen/riwayatPend');
        }
	}

	public function delDokumenPend($idDoc){
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumenbyIdPend($idDoc);
		$filename = $file[0]['filename'];
		$del = $this->mdosen->delDokumenbyIdPend($idDoc);
		if ($del = 1){
			unlink('uploads/dokumen/'.$filename);
			$this->session->set_flashdata('pesan', $this->sukseshapus );
			redirect('dosen/riwayatPend');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalhapus );
			redirect('dosen/riwayatPend');
		}
		
	}

	public function addHKI(){
		$id = $this->session->userdata('username');
		$judul_hki = $this->input->post('judul_hki');
		$tahun = $this->input->post('tahun');
		$jenis_hki = $this->input->post('jenis_hki');
		$no_hki = $this->input->post('no_hki');
		$data_update = array(
			'username' 	=> $id, 		'judul_hki'	=> $judul_hki,
			'tahun' 	=> $tahun,		'jenis_hki'	=> $jenis_hki,
			'no_hki' 	=> $no_hki
			);
		$res = $this->mdosen->addAchievement('hki', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editHKI(){
		$idAch = $this->input->post('id_hki');
		$id = $this->session->userdata('username');
		$judul_hki = $this->input->post('judul_hki');
		$tahun = $this->input->post('tahun');
		$jenis_hki = $this->input->post('jenis_hki');
		$no_hki = $this->input->post('no_hki');
		$data_update = array(
			'judul_hki'		=> $judul_hki,	'tahun' 	=> $tahun, 
			'jenis_hki' 	=> $jenis_hki,	'no_hki'	=> $no_hki
			);
		$res = $this->mdosen->updateHKI($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delHKI($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_hki';
		$table = 'hki';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addJurnal(){
		$id = $this->session->userdata('username');
		$judul_jurnal = $this->input->post('judul_jurnal');
		$judul_artikel = $this->input->post('judul_artikel');
		$tahun = $this->input->post('tahun');
		$volume = $this->input->post('volume');
		$no_jurnal = $this->input->post('no_jurnal');
		$tgl_terbit = $this->input->post('tgl_terbit');
		$indeks = $this->input->post('indeks');
		$tingkat = $this->input->post('tingkat');
		$penulis_ke = $this->input->post('penulis_ke');
		$url_jurnal = $this->input->post('url_jurnal');
		$data_update = array(
			'username' 		=> $id, 			'judul_jurnal'	=> $judul_jurnal,
			'judul_artikel'	=> $judul_artikel,	'tahun' 		=> $tahun,
			'volume'		=> $volume,			'no_jurnal' 	=> $no_jurnal,
			'tgl_terbit'	=> $tgl_terbit,		'indeks' 		=> $indeks,
			'tingkat'		=> $tingkat,		'penulis_ke'	=> $penulis_ke,
			'url_jurnal'	=> $url_jurnal
			);
		$res = $this->mdosen->addAchievement('jurnal', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editJurnal(){
		$idAch = $this->input->post('id_jurnal');
		$id = $this->session->userdata('username');
		$judul_jurnal = $this->input->post('judul_jurnal');
		$judul_artikel = $this->input->post('judul_artikel');
		$tahun = $this->input->post('tahun');
		$volume = $this->input->post('volume');
		$no_jurnal = $this->input->post('no_jurnal');
		$tgl_terbit = $this->input->post('tgl_terbit');
		$indeks = $this->input->post('indeks');
		$tingkat = $this->input->post('tingkat');
		$penulis_ke = $this->input->post('penulis_ke');
		$url_jurnal = $this->input->post('url_jurnal');
		$data_update = array(
			'judul_jurnal'	=> $judul_jurnal,	'judul_artikel'	=> $judul_artikel,
			'tahun' 		=> $tahun,			'volume'		=> $volume,
			'no_jurnal' 	=> $no_jurnal,		'tgl_terbit'	=> $tgl_terbit,
			'indeks' 		=> $indeks,			'tingkat'		=> $tingkat,
			'penulis_ke'	=> $penulis_ke,		'url_jurnal'	=> $url_jurnal
			);
		$res = $this->mdosen->updateJurnal($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delJurnal($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_jurnal';
		$table = 'jurnal';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addPenelitian(){
		$id = $this->session->userdata('username');
		$judul_penelitian = $this->input->post('judul_penelitian');
		$tahun = $this->input->post('tahun');
		$sumber_dana = $this->input->post('sumber_dana');
		$jumlah_dana = $this->input->post('jumlah_dana');
		$status = $this->input->post('status');
		$data_update = array(
			'username' 			=> $id, 			'judul_penelitian'	=> $judul_penelitian,
			'tahun' 			=> $tahun,			'sumber_dana'		=> $sumber_dana,
			'jumlah_dana' 		=> $jumlah_dana,	'status'			=> $status
			);
		$res = $this->mdosen->addAchievement('penelitian', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editPenelitian(){
		$idAch = $this->input->post('id_penelitian');
		$id = $this->session->userdata('username');
		$judul_penelitian = $this->input->post('judul_penelitian');
		$tahun = $this->input->post('tahun');
		$sumber_dana = $this->input->post('sumber_dana');
		$jumlah_dana = $this->input->post('jumlah_dana');
		$status = $this->input->post('status');
		$data_update = array(
			'judul_penelitian'	=> $judul_penelitian,	'tahun' 			=> $tahun,
			'sumber_dana'		=> $sumber_dana,		'jumlah_dana' 		=> $jumlah_dana,
			'status'			=> $status				
		);
		//print_r($data_update);die;
		$res = $this->mdosen->updatePenelitian($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delPenelitian($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_penelitian';
		$table = 'penelitian';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addPengabdian(){
		$id = $this->session->userdata('username');
		$judul_pengabdian = $this->input->post('judul_pengabdian');
		$tahun = $this->input->post('tahun');
		$sumber_dana = $this->input->post('sumber_dana');
		$jumlah_dana = $this->input->post('jumlah_dana');
		$status = $this->input->post('status');
		$data_update = array(
			'username' 			=> $id, 			'judul_pengabdian'	=> $judul_pengabdian,
			'tahun' 			=> $tahun,			'sumber_dana'		=> $sumber_dana,
			'jumlah_dana' 		=> $jumlah_dana,	'status'			=> $status
			);
		$res = $this->mdosen->addAchievement('pengabdian', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editPengabdian(){
		$idAch = $this->input->post('id_pengabdian');
		$id = $this->session->userdata('username');
		$judul_pengabdian = $this->input->post('judul_pengabdian');
		$tahun = $this->input->post('tahun');
		$sumber_dana = $this->input->post('sumber_dana');
		$jumlah_dana = $this->input->post('jumlah_dana');
		$status = $this->input->post('status');
		$data_update = array(
			'judul_pengabdian'	=> $judul_pengabdian,	'tahun'				=> $tahun,
			'sumber_dana'		=> $sumber_dana,		'jumlah_dana' 		=> $jumlah_dana,
			'status'			=> $status
			);
		$res = $this->mdosen->updatePengabdian($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delPengabdian($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_pengabdian';
		$table = 'pengabdian';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addPenghargaan(){
		$id = $this->session->userdata('username');
		$nama_penghargaan = $this->input->post('nama_penghargaan');
		$tahun = $this->input->post('tahun');
		$pemberi = $this->input->post('pemberi');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'username' 	=> $id, 		'nama_penghargaan'	=> $nama_penghargaan,
			'tahun' 	=> $tahun,		'pemberi'			=> $pemberi,
			'tingkat' 	=> $tingkat
			);
		$res = $this->mdosen->addAchievement('penghargaan', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editPenghargaan(){
		$idAch = $this->input->post('id_penghargaan');
		$id = $this->session->userdata('username');
		$nama_penghargaan = $this->input->post('nama_penghargaan');
		$tahun = $this->input->post('tahun');
		$pemberi = $this->input->post('pemberi');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'nama_penghargaan'	=> $nama_penghargaan,	'tahun' 	=> $tahun,
			'pemberi'			=> $pemberi,			'tingkat' 	=> $tingkat
			);
		$res = $this->mdosen->updatePenghargaan($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delPenghargaan($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_penghargaan';
		$table = 'penghargaan';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addProsiding(){
		$id = $this->session->userdata('username');
		$judul_artikel = $this->input->post('judul_artikel');
		$nama_prosiding = $this->input->post('nama_prosiding');
		$tempat = $this->input->post('tempat');
		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$penulis_ke = $this->input->post('penulis_ke');
		$url_prosiding = $this->input->post('url_prosiding');
		$data_update = array(
			'username' 		=> $id, 			'judul_artikel'	=> $judul_artikel,
			'nama_prosiding'=> $nama_prosiding,	'tempat'		=> $tempat,
			'tanggal1' 		=> $tanggal1,		'tanggal2'		=> $tanggal2,
			'penulis_ke'	=> $penulis_ke,		'url_prosiding'	=> $url_prosiding
			);
		$res = $this->mdosen->addAchievement('prosiding', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal);
			redirect('dosen/achievement');
		}
	}

	public function editProsiding(){
		$idAch = $this->input->post('id_prosiding');
		$id = $this->session->userdata('username');
		$judul_artikel = $this->input->post('judul_artikel');
		$nama_prosiding = $this->input->post('nama_prosiding');
		$tempat = $this->input->post('tempat');
		$tanggal1 = $this->input->post('tanggal1');
		$tanggal2 = $this->input->post('tanggal2');
		$penulis_ke = $this->input->post('penulis_ke');
		$url_prosiding = $this->input->post('url_prosiding');
		$data_update = array(
			'judul_artikel'	=> $judul_artikel,	'nama_prosiding'=> $nama_prosiding,
			'tempat'		=> $tempat,			'tanggal1' 		=> $tanggal1,
			'tanggal2'		=> $tanggal2,		'penulis_ke'	=> $penulis_ke,
			'url_prosiding'	=> $url_prosiding
			);
		$res = $this->mdosen->updateProsiding($data_update, $id, $idAch);
		if ($res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/achievement');
		}
	}

	public function delProsiding($idAch = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_prosiding';
		$table = 'prosiding';
		$file = $this->mdosen->getDokumenAch($id, $table, $idAch);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumenAch($idAch, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/achievement');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		} else {
			$res = $this->mdosen->delAch($idAch, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/achievement');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/achievement');
			}
		}
	}

	public function addDokumenAch(){
		$id = $this->session->userdata('username');
		//set preferences
        $config['upload_path'] = './uploads/dokumen/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2024';
        $config['overwrite'] = false;
        //load upload class library
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('docs')){
        	$fileData = $this->upload->data();
        	$uploadData['username'] = $id;
		    $uploadData['jenisAch'] = $this->input->post('jenisAch');
		    $uploadData['idAch'] = $this->input->post('idAch');
	        $uploadData['filename'] = $fileData['file_name'];
        	$file = $this->mdosen->insertDokumenAch($uploadData);
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/achievement');
        } else {
        	// case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('pesan', 
                        '<div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$upload_error['error'].'
                        </div>');
            redirect('dosen/achievement');
        }
	}

	public function delDokumenAch($idDoc){
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumenbyIdAch($idDoc);
		$filename = $file[0]['filename'];
		$del = $this->mdosen->delDokumenbyIdAch($idDoc);
		if ($del = 1){
			unlink('uploads/dokumen/'.$filename);
			$this->session->set_flashdata('pesan', $this->sukseshapus );
			redirect('dosen/achievement');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalhapus );
			redirect('dosen/achievement');
		}
		
	}

	public function aturCV(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Atur CV | Dosen';
		$id = $this->session->userdata('username');
		$this->template->load('TDosen','dosen/aturCV',$data);
	}

	public function printCV(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Curicullum Vitae';
		$id = $this->session->userdata('username');
		$data['pribadi'] = $this->mdosen->getData('pribadi', $id);
		$data['formal'] = $this->mdosen->getFormal($id);
		$data['non_formal'] = $this->mdosen->getNonFormal($id);
		$data['fungsional'] = $this->mdosen->getJabatan('fungsional', $id);
		$data['struktural'] = $this->mdosen->getJabatan('struktural', $id);
		$data['pangkat'] = $this->mdosen->getJabatan('pangkat', $id);
		$data['buku'] = $this->mdosen->getActivity('buku', $id);
		$data['hki'] = $this->mdosen->getAchievement('hki', $id);
		$data['jurnal'] = $this->mdosen->getAchievement('jurnal', $id);
		$data['prosiding'] = $this->mdosen->getAchievement('prosiding', $id);
		$data['penelitian'] = $this->mdosen->getAchievement('penelitian', $id);
		$data['pengabdian'] = $this->mdosen->getAchievement('pengabdian', $id);
		$data['penghargaan'] = $this->mdosen->getAchievement('penghargaan', $id);
		/*---Data Tambahan---*/
		$data['tambahan'] 	= !empty($this->input->post('tambahan')) ? $this->input->post('tambahan') : array();
		$data['org'] = $this->mdosen->getActivity('org', $id); //tambahan1
		$data['review'] = $this->mdosen->getActivity('review', $id); //tambahan2
		$data['narasumber'] = $this->mdosen->getActivity('narasumber', $id); //tambahan3
		$data['leader'] = $this->mdosen->getActivity('leader', $id); //tambahan4

		$this->template->load('TCv','dosen/cv',$data);
		
	}




//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public function updateDeskripsi(){
		$id = $this->session->userdata('username');
		$desk = $this->input->post('deskripsi');
		$data = array('deskripsi' => $desk);
		$res = $this->mdosen->updateDeskripsi($id, $data);
		if($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen');
		}
	}

	public function addDokumen(){
		$id = $this->session->userdata('username');
		//set preferences
        $config['upload_path'] = './uploads/dokumen/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2024';
        $config['overwrite'] = false;
        //load upload class library
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('docs')){
        	$fileData = $this->upload->data();
        	$uploadData['username'] = $id;
		    $uploadData['jenisAct'] = $this->input->post('jenisAct');
		    $uploadData['idAct'] = $this->input->post('idAct');
	        $uploadData['filename'] = $fileData['file_name'];
        	$file = $this->mdosen->insertDokumen($uploadData);
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/activity');
        } else {
        	// case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('pesan', 
                        '<div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$upload_error['error'].'
                        </div>');
            redirect('dosen/activity');
        }
	}

	public function delDokumen($idDoc){
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumenbyId($idDoc);
		$filename = $file[0]['filename'];
		$del = $this->mdosen->delDokumenbyId($idDoc);
		if ($del = 1){
			unlink('uploads/dokumen/'.$filename);
			$this->session->set_flashdata('pesan', $this->sukseshapus );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalhapus );
			redirect('dosen/activity');
		}
		
	}

	public function viewGallery($jenisAct, $idAct){
		$id = $this->session->userdata('username');
		$data['name'] = $this->session->userdata('name');
		$data['title'] ='Gallery page | Dosen';
		$data['image'] = $this->mdosen->getDokumen($id, $jenisAct, $idAct);
		$this->template->load('TDosen','dosen/view_galeri2',$data);
	}

	//ADD Activity
	public function addOrg(){
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jabatan = $this->input->post('jabatan');
		$no = $this->input->post('no_anggota');
		$data_update = array(
			'username' 		=> $id, 	'judul'		=> $judul,
			'tahun' 		=> $tahun,	'jabatan'	=> $jabatan,
			'no_anggota' 	=> $no 
			);
		// print_r($data_update);die;
		$res = $this->mdosen->addActivity('org', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan',  $this->sukses );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan',  $this->gagal );
			redirect('dosen/activity');
		}
	}

	public function addBuku(){
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jml_hlmn = $this->input->post('jml_hlmn');
		$penerbit = $this->input->post('penerbit');
		$data_update = array(
			'username' 		=> $id, 	'judul_buku'=> $judul,
			'tahun_terbit' 	=> $tahun,	'jml_hlmn'	=> $jml_hlmn,
			'penerbit' 		=> $penerbit 
			);
		$res = $this->mdosen->addActivity('buku', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan',  $this->sukses );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan',  $this->gagal );
			redirect('dosen/activity');
		}
	}

	public function addReview(){
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$penyelenggara = $this->input->post('penyelenggara');
		$tanggal = $this->input->post('tanggal');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'username' 		=> $id, 			'judul'		=> $judul,
			'penyelenggara' => $penyelenggara,	'tanggal'	=> $tanggal,
			'tingkat' 		=> $tingkat 
			);
		$res = $this->mdosen->addActivity('review', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/activity');
		}
	}
	
	public function addNarasumber(){
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$penyelenggara = $this->input->post('penyelenggara');
		$tanggal = $this->input->post('tanggal');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'username' 		=> $id, 			'judul'		=> $judul,
			'penyelenggara' => $penyelenggara,	'tanggal'	=> $tanggal,
			'tingkat' 		=> $tingkat 
			);
		$res = $this->mdosen->addActivity('narasumber', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/activity');
		}
	}

	public function addLeader(){
		$id = $this->session->userdata('username');
		$jabatan = $this->input->post('jabatan');
		$tingkat = $this->input->post('tingkat');
		$periode = $this->input->post('periode');
		$data_update = array(
			'username' 	=> $id, 		'jabatan'	=> $jabatan,
			'tingkat' 	=> $tingkat,	'periode'	=> $periode
			);
		$res = $this->mdosen->addActivity('leader', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/activity');
		}
	}
	//Edit ACTIVITY
	public function editOrg(){
		$idAct = $this->input->post('idorg');
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jabatan = $this->input->post('jabatan');
		$no = $this->input->post('no_anggota');
		$data_update = array(
			'judul'		=> $judul,	'no_anggota' 	=> $no, 
			'tahun' 	=> $tahun,	'jabatan'	=> $jabatan
			);
		$res = $this->mdosen->updateOrg($data_update, $id, $idAct);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/activity');
		}
	}

	public function editBuku(){
		$idAct = $this->input->post('idbuku');
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$tahun = $this->input->post('tahun');
		$jml_hlmn = $this->input->post('jml_hlmn');
		$penerbit = $this->input->post('penerbit');
		$data_update = array(
			'judul_buku'	=> $judul,	'penerbit' 	=> $penerbit, 
			'tahun_terbit' 	=> $tahun,	'jml_hlmn'	=> $jml_hlmn			
			);
		$res = $this->mdosen->updateBuku($data_update, $id, $idAct);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/activity');
		}
	}

	public function editReview(){
		$idAct = $this->input->post('idrev');
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$penyelenggara = $this->input->post('penyelenggara');
		$tanggal = $this->input->post('tanggal');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'judul'			=> $judul,			'tingkat' 	=> $tingkat,
			'penyelenggara' => $penyelenggara,	'tanggal'	=> $tanggal
			
			);
		$res = $this->mdosen->updateRev($data_update, $id, $idAct);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan',  $this->gagalubah );
			redirect('dosen/activity');
		}
	}

	public function editNarasumber(){
		$idAct = $this->input->post('idnara');
		$id = $this->session->userdata('username');
		$judul = $this->input->post('judul');
		$penyelenggara = $this->input->post('penyelenggara');
		$tanggal = $this->input->post('tanggal');
		$tingkat = $this->input->post('tingkat');
		$data_update = array(
			'judul'			=> $judul,			'tingkat' 	=> $tingkat, 
			'penyelenggara' => $penyelenggara,	'tanggal'	=> $tanggal
			);
		$res = $this->mdosen->updateNara($data_update, $id, $idAct);
		if ($res){
			$this->session->set_flashdata('pesan',  $this->suksesubah );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/activity');
		}
	}

	public function editLeader(){
		$idAct = $this->input->post('idleader');
		$id = $this->session->userdata('username');
		$jabatan = $this->input->post('jabatan');
		$tingkat = $this->input->post('tingkat');
		$periode = $this->input->post('periode');
		$data_update = array(
			'jabatan'	=> $jabatan,
			'tingkat' 	=> $tingkat,	'periode'	=> $periode
			);
		$res = $this->mdosen->updateLeader($data_update, $id, $idAct);
		if ($res){
			$this->session->set_flashdata('pesan', $this->suksesubah );
			redirect('dosen/activity');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/activity');
		}
	}
	// DELETE ACTIVITY
	public function delOrg($idAct = 0){
		$id = $this->session->userdata('username');
		$kolom = 'id_org';
		$table = 'org';
		$file = $this->mdosen->getDokumen($id, $table, $idAct);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idAct, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/activity');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		} else {
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/activity');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		}
	}

	public function delBuku($idAct = 0){
		$kolom = 'id_buku';
		$table = 'buku';
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumen($id, $table, $idAct);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idAct, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/activity');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		} else {
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/activity');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		}
	}

	public function delRev($idAct = 0){
		$kolom = 'id_review';
		$table = 'review';
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumen($id, $table, $idAct);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idAct, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/activity');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		} else {
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/activity');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		}
	}

	public function delNara($idAct = 0){
		$kolom = 'id_narasumber';
		$table = 'narasumber';
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumen($id, $table, $idAct);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idAct, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/activity');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		} else {
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/activity');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		}
	}

	public function delLeader($idAct = 0){
		$kolom = 'id_leader';
		$table = 'leader';
		$id = $this->session->userdata('username');
		$file = $this->mdosen->getDokumen($id, $table, $idAct);
		if(!empty($file)){
			$filename = $file[0]['filename'];
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$res2 = $this->mdosen->delDokumen($idAct, $table);
				if ($res2 == 1){
					unlink('uploads/dokumen/'.$filename);
					$this->session->set_flashdata('pesan', $this->sukseshapus );
					redirect('dosen/activity');
				}
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		} else {
			$res = $this->mdosen->delAct($idAct, $kolom, $table);
			if ($res){
				$this->session->set_flashdata('pesan', $this->sukseshapus );
				redirect('dosen/activity');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalhapus );
				redirect('dosen/activity');
			}
		}
	}


	public function addpangkat(){
		$id = $this->session->userdata('username');
		$gol = $this->input->post('gol');
		$jenis = $this->input->post('pangkatadd');
		$tmt = $this->input->post('tmt');
		$data_update = array(
			'username' 	=> $id, 	'gol_ruang'	=> $gol,
			'jenis' 	=> $jenis,	'tmt'	=> $tmt
			);
		$cek = $this->mdosen->ifExistPangkat($id, $jenis);
		if(!$cek){
			$res = $this->mdosen->addJabatan('pangkat', $data_update);
			if (!$res){
				$this->session->set_flashdata('pesan', $this->sukses );
				redirect('dosen/dataPribadi');
			} else {
				$this->session->set_flashdata('pesan', $this->gagal );
				redirect('dosen/dataPribadi');
			}
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Pangkat yang akan ditambahkan sudah ada </strong> </div>' );
			redirect('dosen/dataPribadi');
		}
	}

	public function editPangkat(){
		$id = $this->session->userdata('username');
		$gol = $this->input->post('goledit');
		$jenisbaru = $this->input->post('pangkatedit');
		$jenislama = $this->input->post('pangkatlama');
		$tmt = $this->input->post('tmtedit');
		$idpgkt = $this->input->post('idpgkt');
		$data_update = array(
			'gol_ruang'	=> $gol, 'tmt'	=> $tmt,
			'jenis' 	=> $jenisbaru	
			);
		// print_r($data_update);die;
		if($jenisbaru == $jenislama){
			$res = $this->mdosen->editJabatan('pangkat','id_pangkat', $idpgkt, $data_update);
			if ($res = 1){
				$this->session->set_flashdata('pesan', $this->suksesubah );
				redirect('dosen/dataPribadi');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalubah );
				redirect('dosen/dataPribadi');
			}
		}else {
			$cek = $this->mdosen->ifExistPangkat($id, $jenisbaru);
			if(!$cek){
				$res = $this->mdosen->editJabatan('pangkat','id_pangkat', $idpgkt, $data_update);
				if ($res = 1){
					$this->session->set_flashdata('pesan', $this->suksesubah );
					redirect('dosen/dataPribadi');
				} else {
					$this->session->set_flashdata('pesan', $this->gagalubah );
					redirect('dosen/dataPribadi');
				}
			} else {
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Pangkat yang akan diedit sudah ada </strong> </div>' );
				redirect('dosen/dataPribadi');
			}
		}
	}

	public function addstruk(){
		$id = $this->session->userdata('username');
		$jabatan = $this->input->post('jabatan');
		$ket = $this->input->post('keterangan');
		$tmt = $this->input->post('awal');
		$selesai = $this->input->post('selesai');
		$data_update = array(
			'username' 	=> $id, 	'jab_struk'		=> $jabatan,	'keterangan' =>$ket,
			'awal_struk'=> $tmt,	'akhir_struk'	=> $selesai
			);
		$res = $this->mdosen->addJabatan('struktural', $data_update);
		if (!$res){
			$this->session->set_flashdata('pesan', $this->sukses);
			redirect('dosen/dataPribadi');
		} else {
			$this->session->set_flashdata('pesan', $this->gagal );
			redirect('dosen/dataPribadi');
		}
	}

	public function editStruk(){
		$id = $this->session->userdata('username');
		$jabatan = $this->input->post('jabatan');
		$ket = $this->input->post('keterangan');
		$tmt = $this->input->post('awal');
		$selesai = $this->input->post('selesai');
		$idstruk = $this->input->post('idstruk');
		$data_update = array(
			'jab_struk'		=> $jabatan,	'keterangan' => $ket,
			'awal_struk'=> $tmt,			'akhir_struk'	=> $selesai
			);
		$res = $this->mdosen->editJabatan('struktural','id_struk', $idstruk, $data_update);
		if ($res = 1){
			$this->session->set_flashdata('pesan', $this->suksesubah);
			redirect('dosen/dataPribadi');
		} else {
			$this->session->set_flashdata('pesan', $this->gagalubah );
			redirect('dosen/dataPribadi');
		}
	}

	public function addfung(){
		$id = $this->session->userdata('username');
		$jabatan = $this->input->post('jabatan');
		$tmt = $this->input->post('awal');
		$data_update = array(
			'username' 	=> $id, 		'jab_fung'	=> $jabatan,
			'awal_fung' => $tmt
			);
		$cek = $this->mdosen->ifExistFung($id, $jabatan);
		if(!$cek){
			$res = $this->mdosen->addJabatan('fungsional', $data_update);
			if (!$res){
				$this->session->set_flashdata('pesan', $this->sukses );
				redirect('dosen/dataPribadi');
			} else {
				$this->session->set_flashdata('pesan', $this->gagal );
				redirect('dosen/dataPribadi');
			}
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Jabatan yang akan ditambahkan sudah ada </strong> </div>' );
			redirect('dosen/dataPribadi');
		}
		
	}

	public function editFung(){
		$id = $this->session->userdata('username');
		$jabbaru = $this->input->post('jabatanbaru');
		$jablama = $this->input->post('jabatanlama');
		$tmt = $this->input->post('awal');
		$idfung = $this->input->post('idfung');
		$data_update = array(
			'username' 	=> $id, 		'jab_fung'	=> $jabbaru,
			'awal_fung' => $tmt
			);
		if($jabbaru == $jablama){
			$res = $this->mdosen->editJabatan('fungsional','id_fung', $idfung, $data_update);
			if ($res = 1){
				$this->session->set_flashdata('pesan', $this->suksesubah );
				redirect('dosen/dataPribadi');
			} else {
				$this->session->set_flashdata('pesan', $this->gagalubah );
				redirect('dosen/dataPribadi');
			}
		}else{
			$cek = $this->mdosen->ifExistFung($id, $jabatan);
			if(!$cek){
				$res = $this->mdosen->editJabatan('fungsional','id_fung', $idfung, $data_update);
				if ($res = 1){
					$this->session->set_flashdata('pesan', $this->suksesubah );
					redirect('dosen/dataPribadi');
				} else {
					$this->session->set_flashdata('pesan', $this->gagalubah );
					redirect('dosen/dataPribadi');
				}
			} else {
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Jabatan yang akan diedit sudah ada </strong> </div>' );
				redirect('dosen/dataPribadi');
			}
		}
		
		
	}

	public function profilePhoto(){
		$id = $this->session->userdata('username');
		$path = $_FILES['filename']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		//set preferences
        $config['upload_path'] = './uploads/profilePhoto/';
        $config['allowed_types'] = 'png|jpg|gif';
        $config['max_size'] = '2024';
        $config['file_name'] = $id.'.'.$ext;
        $config['overwrite'] = true;
        // print_r($config['file_name']);die;
        // echo $this->mdosen->ifExistProfPic($id);die;
        

        //load upload class library
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('filename')){
            // case - failure
            $upload_error = array('error' => $this->upload->display_errors());
            // print_r($upload_error);die;
            $this->session->set_flashdata('pesan', 
                        '<div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        '.$upload_error['error'].'
                        </div>');
            redirect('dosen/changePassword');
            // $this->load->view('upload_file_view', $upload_error);
        } else {
            // case - success
            $upload_data = $this->upload->data(); //print_r($upload_data);die;
            $id = $this->session->userdata('username');
            $filename = $upload_data['file_name'];
           	
			if ($this->mdosen->ifExistProfPic($id) == 0) {
				$data = array(
					'username' 	=> $id, 	'filename'		=> $filename
					);
				$res = $this->mdosen->addProfPic($data);
				if (!$res){
					$this->session->set_flashdata('pesan', $this->sukses );
					redirect('dosen/changePassword');
				} else {
					$this->session->set_flashdata('pesan', $this->gagal );
					redirect('dosen/changePassword');
				}
			} else {
				$data = array(
					'filename'		=> $filename
					);
				$res = $this->mdosen->updateProfpic($data, $id);
				if ($res){
					$this->session->set_flashdata('pesan',  $this->sukses );
					redirect('dosen/changePassword');
				} else {
					$this->session->set_flashdata('pesan', $this->gagal );
					redirect('dosen/changePassword');
				}
			}
        }
	}



	public function idcard(){
		$id = $this->session->userdata('username');
		$id_profil = $this->mdosen->getIdProfil($id);
		$dosen = $this->mdosen->getData('pribadi', $id);
		$data= array (
			'nama' 		=> $dosen[0]['nm_dosen'], 	'depan' 		=> $dosen[0]['glr_dpn'],
			'blkg' 		=> $dosen[0]['glr_blkg'],	'username' 		=> $dosen[0]['username'],
			'hp1' 		=> $dosen[0]['hp1'], 		'hp2' 			=> $dosen[0]['hp2'],
			'email' 	=> $dosen[0]['email'],	 	'id_profil' 	=> $id_profil,
			'name' 		=> $this->session->userdata('name'), 'title' =>'Home page | Dosen'
			);
		// print_r($da
		$this->load->view('id', $data);	
		// $this->template->load('TDosen','dosen/viewCard',$data);
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

	public function tema(){
		$id = $this->session->userdata('username');
		$ppdata = $this->mdosen->getProfpic($id);
        $data['profpic'] = $ppdata[0]['filename'];
        $data['pribadi'] = $this->mdosen->getData('pribadi', $id);
        $data['fungakhir'] = $this->mdosen->getFungFront($id);
		$data['formal'] = $this->mdosen->getFormal($id);
		$data['non_formal'] = $this->mdosen->getNonFormal($id);
		$data['fungsional'] = $this->mdosen->getJabatan('fungsional', $id);
		$data['struktural'] = $this->mdosen->getJabatan('struktural', $id);
		$data['pangkat'] = $this->mdosen->getJabatan('pangkat', $id);
		$data['org'] = $this->mdosen->getActivity('org', $id);
		$data['buku'] = $this->mdosen->getActivity('buku', $id);
		$data['review'] = $this->mdosen->getActivity('review', $id);
		$data['narasumber'] = $this->mdosen->getActivity('narasumber', $id);
		$data['leader'] = $this->mdosen->getActivity('leader', $id);
		$data['hki'] = $this->mdosen->getAchievement('hki', $id);
		$data['jurnal'] = $this->mdosen->getAchievement('jurnal', $id);
		$data['prosiding'] = $this->mdosen->getAchievement('prosiding', $id);
		$data['penelitian'] = $this->mdosen->getAchievement('penelitian', $id);
		$data['pengabdian'] = $this->mdosen->getAchievement('pengabdian', $id);
		$data['penghargaan'] = $this->mdosen->getAchievement('penghargaan', $id);
		$data['persen_hki'] = $this->mdosen->countAch('hki', $id);
		$data['persen_jurnal'] = $this->mdosen->countAch('jurnal', $id);
		$data['persen_prosiding'] = $this->mdosen->countAch('prosiding', $id);
		$data['persen_penelitian'] = $this->mdosen->countAch('penelitian', $id);
		$data['persen_pengabdian'] = $this->mdosen->countAch('pengabdian', $id);
		$data['persen_penghargaan'] = $this->mdosen->countAch('penghargaan', $id);
		// print_r($data['persen_pengabdian']);die;
		$this->load->view('tema',$data);
	}
}
?>