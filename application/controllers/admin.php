<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('auth');
			
		} else if ($this->session->userdata('grup') == '2') {
			redirect('dosen');
		}
		$this->load->helper('text');
		$this->load->model('madmin');
		$this->load->library('email');
		$this->load->helper('string');
	}
	
	public function index() {
		$data['name'] = $this->session->userdata('name');
		$data['title']='Home Page | Admin';
		$data['jmlDosen'] = $this->madmin->countUser();
		$data['jmlPenulis'] = $this->madmin->countData('buku');
		$data['jmlOrg'] = $this->madmin->countData('org');
		$data['jmlNarasumber'] = $this->madmin->countData('narasumber');
		$data['jmlReviewer'] = $this->madmin->countData('review');
		$data['jmlLeader'] = $this->madmin->countData('leader');
		//$this->load->view('dashboard1');
		$this->template->load('TAdmin','admin/dash',$data);
	}


	public function lihatdosen(){
		$data['name'] = $this->session->userdata('name');
		$data['title'] ='User Table | Admin';
		$data['data'] = $this->madmin->getDosen();
		$this->template->load('TAdmin','admin/daftar_dosen',$data);
	} 


	public function insertDosen(){
		$this->load->helper('string');
		$username = $this->input->post('username');
		$cek = $this->madmin->getUser($username);
		if (count($cek) == 1){
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Username sudah terdaftar</strong>
					</div>');
			redirect('admin/lihatdosen');
		} else {
			$nama = $this->input->post('nama');
			//$pass = random_string('alnum',8);
			$pass = random_string('alnum',8);
			$grup = 2;
			$log  = 0;

			$data_insert = array(
					'username' 	=> $username,
					'name' 		=> $nama,
					'password' 	=> md5($pass),
					'grup' 		=> $grup,
					'logged' 	=> $log
				);
			$res = $this->madmin->insertDosen($data_insert);
			if ($res >=1){
				if($this->sendEmail($username, $pass)){
					$this->session->set_flashdata('pesan', 
						'<div class="alert alert-success" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>User baru berhasil ditambahkan! dan Email terkirim</strong>
						</div>');
					// $this->sendEmail($username);
					redirect('admin/lihatdosen');
				}else{
					$this->session->set_flashdata('pesan', 
						'<div class="alert alert-danger" role="alert">
		  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  				<strong>User baru berhasil ditambahkan, namun email gagal dikirim kan</strong>
						</div>');
					// $this->sendEmail($username);
					redirect('admin/lihatdosen');
				}
			} else {
				//echo "<script>alert('User gagal bertambah');</script>";
				$this->session->set_flashdata('pesan', 
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>User Gagal bertambah</strong>
					</div>');
				redirect('admin/lihatdosen');
			}
		}			
	}

	public function resetPass($id){
		$cek = $this->madmin->ifexist($id);
		if($cek){
			$pass = random_string('alnum',8);
			$data_insert = array(
					'password' 	=> md5($pass),	'logged' 	=> 0
				);
			$res = $this->madmin->resetPassDosen($data_insert,$id);
			$this->resetEmail($id, $pass);
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-success" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password berhasil direset!</strong>
					</div>');
			redirect('admin/lihatdosen');
		}else{
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password gagal di reset!</strong>
					</div>');
			redirect('admin/lihatdosen');
		}
	}

	public function sendEmail($to, $pass){
		$login = base_url('auth');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer();
        $mail->IsSMTP(); // we are going to use SMTP
        $mail->SMTPAuth   = true; // enabled SMTP authentication
        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
        $mail->SMTPOptions = array(
								    'ssl' => array(
								        'verify_peer' => false,
								        'verify_peer_name' => false,
								        'allow_self_signed' => true
								    )
								);
        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
        $mail->Port       = 587;                   // SMTP port to connect to GMail
        $mail->Username   = "staff.ifundip@gmail.com";  // user email address
        $mail->Password   = "staffifundip";            // password in GMail
        $mail->SetFrom('staff.ifundip@gmail.com', 'Admin Staff Site Informatika');  //Who is sending 
        $mail->isHTML(true);
        $mail->Subject    = "Registrasi Akun Staff Site";
        $mail->Body      = "
            <html>
            <head>
                <title>Registrasi Akun Staff Site Informatika</title>
            </head>
            <body>
            <h3>Staff Site Informatika Universitas Diponegoro</h3>
            <br>
            <p>Akun Anda telah siap untuk digunakan, berikut ini informasi yang Anda butuhkan untuk mengakses Staff Site Informatika:</p>
            <table>
            	<tr>
            		<td>Username</td>
            		<td>:</td>
            		<td>$to</td>
            	</tr>
            	<tr>
            		<td>Password</td>
            		<td>:</td>
            		<td>$pass</td>
            	</tr>
            </table>
            <p>Anda dapat melakukan login dengan mengakses link berikut: $login</p><br>
            <p>Terima Kasih</p>
            </body>
            </html>
        ";
        // Who is addressed the email to
        $mail->AddAddress($to, $to);
        if(!$mail->Send()) {
        	echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }

    public function resetEmail($to, $pass){
		$login = base_url('auth');
		require_once(APPPATH.'third_party/PHPMailer-master/PHPMailerAutoload.php');
        $mail = new PHPMailer(true);
        try{
        	$mail->IsSMTP(); // we are going to use SMTP
	        $mail->SMTPAuth   = true; // enabled SMTP authentication
	        $mail->SMTPSecure = "tls";  // prefix for secure protocol to connect to the server
	        $mail->SMTPOptions = array(
								    'ssl' => array(
								        'verify_peer' => false,
								        'verify_peer_name' => false,
								        'allow_self_signed' => true
								    )
								);
	        $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
	        $mail->Port       = 587;                   // SMTP port to connect to GMail
	        $mail->Username   = "staff.ifundip@gmail.com";  // user email address
	        $mail->Password   = "staffifundip";            // password in GMail
	        $mail->SetFrom('staff.ifundip@gmail.com', 'Admin Staff Site Informatika');  //Who is sending 
	        $mail->isHTML(true);
	        $mail->Subject    = "Reset Password Akun Staff Site";
	        $mail->Body      = "
	            <html>
	            <head>
	                <title>Password Reset</title>
	            </head>
	            <body>
	            <h3>Staff Site Informatika Universitas Diponegoro</h3>
	            <br>
	            <p>Password akun Anda telah direset, berikut informasi baru mengenai akun Anda:</p>
	            <table>
	            	<tr>
	            		<td>Username</td>
	            		<td>:</td>
	            		<td>$to</td>
	            	</tr>
	            	<tr>
	            		<td>Password Baru</td>
	            		<td>:</td>
	            		<td>$pass</td>
	            	</tr>
	            </table>
	            <p>Anda dapat melakukan login dengan mengakses link berikut: <a href='$login'>Staff Site</a></p>
	            <p>Mohon diperhatikan bahwa saat Anda login dengan password yang baru, Anda akan diminta untuk mengganti password dan mengupload foto profil kembali.</p>
	            <p>Terima Kasih</p>
	            </body>
	            </html>
	        ";
	        // Who is addressed the email to
	        $mail->AddAddress($to, $to);
	        $mail->Send();
	        echo "Message sent";
        } catch(phpmailerException $e){
        	echo $e->errorMessage();
        } catch(Exception $e){
        	echo $e->getMessage();
        }
    }

	public function edit($username){
		$edit = $this->madmin->getUser($username);
		$data['edit'] = array(
			'username' 	=> $edit[0]['username'],	 'name' => $edit[0]['name']
			);
		// $this->template->load('TAdmin');
		$data['name'] = $this->session->userdata('name');
		$data['title']='Home Page | Admin';
		//$this->load->view('dashboard1');
		$this->template->load('TAdmin','admin/edit_view',$data);
	}

	public function editDosen(){
		$name = $this->input->post('name');
		$id = $this->input->post('username');
		$data_update = array(
			'name' => $name
		);
		$update_pribadi = array('nm_dosen' => $name);
		$res2 = $this->madmin->updatePribadi($update_pribadi, $id);
		$res = $this->madmin->updateDataDosen($data_update, $id);

		if ($res){
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-success" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Data berhasil diubah</strong>
					</div>');
			redirect('admin/lihatdosen');
		} else {
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Data berhasil diubah</strong>
					</div>');
			redirect('admin/lihatdosen');
		}
	}

	public function changePassword(){
		$data['name'] = $this->session->userdata('name');
		$data['title']='Change Password | Admin';
		$this->template->load('TAdmin','admin/password',$data);
	}

	public function updatePassword(){
		$newpass = md5($this->input->post('password'));
		$id = $this->session->userdata('username');
		$data_update = array(
			'password' => $newpass
 		);
		
		$res = $this->madmin->updateDataDosen($data_update, $id);
		if ($res){
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-success" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password berhasil diubah</strong>
					</div>');
			redirect('admin');
		} else {
			$this->session->set_flashdata('pesan', 
					'<div class="alert alert-danger" role="alert">
	  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  				<strong>Password gagal diubah</strong>
					</div>');
			redirect('admin/changePassword');
		}
	} 


	public function deleteDosen($username){
		$where = array('username' => $username);
		//hapus semua data yang usernamenya $username
		$del1 = $this->madmin->deleteDosen('user', $where);
		$del2 = $this->madmin->deleteDosen('pribadi', $where);
		$this->madmin->deleteDosen('profpic', $where);
		// riwayat jabatan
		$this->madmin->deleteDosen('fungsional', $where);
		$this->madmin->deleteDosen('struktural', $where);
		$this->madmin->deleteDosen('pangkat', $where);
		// dokumen
		$this->madmin->deleteDosen('dokumen', $where);
		$this->madmin->deleteDosen('dokumen_achievement', $where);
		$this->madmin->deleteDosen('dokumen_pendidikan', $where);
		// riwayat pendidikan
		$this->madmin->deleteDosen('formal', $where);
		$this->madmin->deleteDosen('non_formal', $where);
		// kegiatan
		$this->madmin->deleteDosen('org', $where);
		$this->madmin->deleteDosen('buku', $where);
		$this->madmin->deleteDosen('review', $where);
		$this->madmin->deleteDosen('narasumber', $where);
		$this->madmin->deleteDosen('leader', $where);
		// pencapaian
		$this->madmin->deleteDosen('hki', $where);
		$this->madmin->deleteDosen('jurnal', $where);
		$this->madmin->deleteDosen('prosiding', $where);
		$this->madmin->deleteDosen('penelitian', $where);
		$this->madmin->deleteDosen('pengabdian', $where);
		$this->madmin->deleteDosen('penghargaan', $where);

		//hapus hapus hapus
		if ($del1 >= 1){
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-success" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>User berhasil dihapus</strong>
				</div>');
			redirect('admin/lihatdosen');
		} else {
			$this->session->set_flashdata('pesan', 
				'<div class="alert alert-danger" role="alert">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>User gagal dihapus</strong>
				</div>');
			redirect('admin/lihatdosen');
		}
	}

	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect(base_url());
	}

}
?>