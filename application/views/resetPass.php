<form action ="<?php echo base_url('auth/lupapassword')?>" class="form-horizontal" method="post">
   
    <div class="form-group">
        <label class="col-sm-2 control-label">Username : </label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Username Anda" name="username" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <!-- <button type="reset" class="btn btn-danger pull-right">Reset</button> -->
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Reset password</button>
        </div>
    </div>
</form>      
<p style="text-align: justify; padding-left: 5%">
    Petunjuk:<br>
    1. Silahkan isikan username/email akun Anda, kemudian pilih tombol reset password<br>
    2. Staff Site akan mengirimkan email yang berisi password baru Anda pada email yang Anda isikan<br>
    3. Periksa folder inbox email Anda, jika tidak ada, periksa pada bagian spam/junk folder<br>
    4. Gunakan password tersebut untuk masuk ke dalam Staff Site IF<br>
    5. Saat Anda masuk, Anda akan diminta untuk mengubah password dan mengupload foto kembali<br>
</p>   