<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Staff Site Informatika Undip | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/square/blue.css') ?>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/logo undip.png')?>"/>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="login-page" style="background: #b8deea">
        <div class="login-box" style="">

            <div class="login-logo" style="margin-bottom: 5px">
                <img src="<?php echo base_url('assets/img/logo undip.png') ?>" style="width: 70px;" alt="Universitas Diponegoro" /><br>
                <a href="#"><b>STAFF</b> SITE</a> 
            </div><!-- /.login-logo -->
            <div class="login-box-body col-lg-12" style="align-content: center;">
                <p class="login-box-msg col-lg-12">
                    <b>Departemen <br>Ilmu Komputer/ Informatika </b><br>Universitas Diponegoro
                </p>
                <div class="col-lg-12" style="padding-top: 0%">
                    <form action="<?php echo site_url('auth/cek_login') ?>" method="post">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" placeholder="Masukkan Username Anda" id="username" name="username" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required />
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" placeholder="Masukkan Password Anda" id="password" name="password" pattern="[\S]{8,}$" required />
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>

                        <div class="row">
                            <div class="col-xs-5" >
                                <button type="submit" class="btn btn-primary btn-block btn-flat ">Masuk</button>
                            </div>
                            <div></div>
                            <div class="col-xs-7" align="right">
                                <a class="btn btn-primary btn-block btn-flat pull-right" data-toggle="modal" data-target="#resetpassword"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Lupa password</a>
                            </div>
                        </div>
                    </form>
                </div>
                
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

<!-- Modal Button Edit-->
<div class="modal fade" id="resetpassword" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Lupa Password</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('resetPass');?>
              </div>
              <div class="modal-footer">
                
              </div>
          </div>
      </div>
  </div>




        <!-- jQuery 2.1.3 -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js') ?>" type="text/javascript"></script>
    </body>
</html>