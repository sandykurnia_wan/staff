<!DOCTYPE html>
<html>
    <head>
        <?php 
            $id = $this->session->userdata('username');
            $ppdata = $this->mdosen->getProfpic($id);
            $profpic = $ppdata[0]['filename'];
            // print_r();die;
        ?>
        <meta charset="UTF-8">
        <title>Staff Dosen | <?php echo $title; ?> </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/ionicons-2.0.1/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css')?>" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css')?>" rel="stylesheet" type="text/css" /> 
        <!-- Daterange picker -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker.css')?>" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="<?php echo base_url('assets/ckeditor/ckeditor.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/ckeditor/styles.js')?>"></script>
        
        <!-- Input Masking -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskedinput.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.priceformat.min.js')?>"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .profpic{
                object-fit: cover;
                width: 150px;
                height: 100px;
            }
        </style>
    </head>
    <body class="skin-blue">
        <!-- Site wrapper -->
        <div class="wrapper">

            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div id="contents"><?= $contents ?></div>
            </div><!-- /.content-wrapper -->

            </div><!-- ./wrapper -->

            <!-- jQuery 2.1.3 -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
            <!-- SlimScroll -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
            <!-- FastClick -->
            <script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
            <script type="text/javascript">
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 4000);

                $(".org").click(function(){
                    var id = $(this).attr("data-idorg");
                    var judul = $(this).attr("data-judul");
                    var tahun = $(this).attr("data-tahun");
                    var jab = $(this).attr("data-jab");
                    var no = $(this).attr("data-no");
                    $("#judul").val(judul);
                    $("#tahun").val(tahun);
                    $("#jab").val(jab);
                    $("#no").val(no);
                    $("#idorg").val(id);
                // alert(id);
                })

                $(".buku").click(function(){
                    var id = $(this).attr("data-idbuku");
                    var judulbuku = $(this).attr("data-judulbuku");
                    var tahunterbit = $(this).attr("data-tahunterbit");
                    var halaman = $(this).attr("data-halaman");
                    var penerbit = $(this).attr("data-penerbit");
                    $("#judulbuku").val(judulbuku);
                    $("#tahunterbit").val(tahunterbit);
                    $("#halaman").val(halaman);
                    $("#penerbit").val(penerbit);
                    $("#idbuku").val(id);
                // alert(id);
                })

                $(".review").click(function(){
                    var id = $(this).attr("data-idrev");
                    var judul = $(this).attr("data-judul");
                    var penyelenggara = $(this).attr("data-penyelenggara");
                    var tingkat = $(this).attr("data-tingkat");
                    var tanggal = $(this).attr("data-tanggal");
                    $("#judulrev").val(judul);
                    $("#penyelenggara").val(penyelenggara);
                    $("#tingkat").val(tingkat);
                    $("#tanggal").val(tanggal);
                    $("#idrev").val(id);
                // alert(id);
                })

                $(".narasumber").click(function(){
                    var id = $(this).attr("data-idnara");
                    var judul = $(this).attr("data-judul");
                    var penyelenggara = $(this).attr("data-penyelenggara");
                    var tanggal = $(this).attr("data-tanggal");
                    var tingkat = $(this).attr("data-tingkat");
                    $("#judulNara").val(judul);
                    $("#penyelenggaraNara").val(penyelenggara);
                    $("#tanggalNara").val(tanggal);
                    $("#tingkatNara").val(tingkat);
                    $("#idnara").val(id);
                // alert(id);
                })

                $(".leadership").click(function(){
                    var id = $(this).attr("data-idleader");
                    var jabatan = $(this).attr("data-jabatan");
                    var tingkat = $(this).attr("data-tingkat");
                    var periode = $(this).attr("data-periode");
                    $("#jabatan").val(jabatan);
                    $("#tingkatLeadership").val(tingkat);
                    $("#periode").val(periode);
                    $("#idleader").val(id);
                // alert(id);
                })

                $(".viewformal").click(function(){
                    var id = $(this).attr("data-idformal-view");
                    var jenjang = $(this).attr("data-jenjang-view");
                    var nama_univ = $(this).attr("data-nama-univ-view");
                    var bidang = $(this).attr("data-bidang-view");
                    var thn_masuk = $(this).attr("data-thn-masuk-view");
                    var thn_lulus = $(this).attr("data-thn-lulus-view");
                    var judul_ta = $(this).attr("data-judul-ta-view");
                    var dosbing1 = $(this).attr("data-dosbing1-view");
                    var dosbing2 = $(this).attr("data-dosbing2-view");
                    $("#id_formal_view").val(id);
                    $("#jenjang_view").val(jenjang);
                    $("#nama_univ_view").val(nama_univ);
                    $("#bidang_view").val(bidang);
                    $("#thn_masuk_view").val(thn_masuk);
                    $("#thn_lulus_view").val(thn_lulus);
                    $("#judul_ta_view").val(judul_ta);
                    $("#dosbing1_view").val(dosbing1);
                    $("#dosbing2_view").val(dosbing2);
                // alert(id);
                })

                $(".editformal").click(function(){
                    var id = $(this).attr("data-idformal-edit");
                    var jenjang = $(this).attr("data-jenjang-edit");
                    var nama_univ = $(this).attr("data-nama-univ-edit");
                    var bidang = $(this).attr("data-bidang-edit");
                    var thn_masuk = $(this).attr("data-thn-masuk-edit");
                    var thn_lulus = $(this).attr("data-thn-lulus-edit");
                    var judul_ta = $(this).attr("data-judul-ta-edit");
                    var dosbing1 = $(this).attr("data-dosbing1-edit");
                    var dosbing2 = $(this).attr("data-dosbing2-edit");
                    $("#id_formal_edit").val(id);
                    $("#jenjang_edit").val(jenjang);
                    $("#nama_univ_edit").val(nama_univ);
                    $("#bidang_edit").val(bidang);
                    $("#thn_masuk_edit").val(thn_masuk);
                    $("#thn_lulus_edit").val(thn_lulus);
                    $("#judul_ta_edit").val(judul_ta);
                    $("#dosbing1_edit").val(dosbing1);
                    $("#dosbing2_edit").val(dosbing2);
                // alert(id);
                })

                $(".viewnonformal").click(function(){
                    var id = $(this).attr("data-idnonformal-view");
                    var nama_kegiatan = $(this).attr("data-nama-kegiatan-view");
                    var tgl_kegiatan = $(this).attr("data-tgl-kegiatan-view");
                    var penyelenggara = $(this).attr("data-penyelenggara-view");
                    var tempat = $(this).attr("data-tempat-view");
                    var pola = $(this).attr("data-pola-view");
                    $("#id_formal_view").val(id);
                    $("#nama_kegiatan_view").val(nama_kegiatan);
                    $("#tgl_kegiatan_view").val(tgl_kegiatan);
                    $("#penyelenggara_view").val(penyelenggara);
                    $("#tempat_view").val(tempat);
                    $("#pola_view").val(pola);
                // alert(id);
                })

                $(".editnonformal").click(function(){
                    var id = $(this).attr("data-idnonformal-edit");
                    var nama_kegiatan = $(this).attr("data-nama-kegiatan-edit");
                    var tgl_kegiatan = $(this).attr("data-tgl-kegiatan-edit");
                    var penyelenggara = $(this).attr("data-penyelenggara-edit");
                    var tempat = $(this).attr("data-tempat-edit");
                    var pola = $(this).attr("data-pola-edit");
                    $("#id_formal_edit").val(id);
                    $("#nama_kegiatan_edit").val(nama_kegiatan);
                    $("#tgl_kegiatan_edit").val(tgl_kegiatan);
                    $("#penyelenggara_edit").val(penyelenggara);
                    $("#tempat_edit").val(tempat);
                    $("#pola_edit").val(pola);
                // alert(id);
                })

                $(".hki").click(function(){
                    var id = $(this).attr("data-idhki");
                    var judul_hki = $(this).attr("data-judul-hki");
                    var tahun = $(this).attr("data-tahun");
                    var jenis_hki = $(this).attr("data-jenis");
                    var no_hki = $(this).attr("data-no-hki");
                    $("#id_hki").val(id);
                    $("#judul_hki").val(judul_hki);
                    $("#tahun").val(tahun);
                    $("#jenis_hki").val(jenis_hki);
                    $("#no_hki").val(no_hki);
                // alert(id);
                })

                $(".jurnal").click(function(){
                    var id = $(this).attr("data-idjurnal");
                    var judul_jurnal = $(this).attr("data-judul-jurnal");
                    var tahun_jurnal = $(this).attr("data-tahun-jurnal");
                    var volume = $(this).attr("data-volume");
                    var no_jurnal = $(this).attr("data-no-jurnal");
                    var tanggal = $(this).attr("data-tanggal");
                    var indeks1 = $(this).attr("data-indeks1");
                    var indeks2 = $(this).attr("data-indeks2");
                    var tingkat = $(this).attr("data-tingkat");
                    var penulis_ke = $(this).attr("data-penulis");
                    var url_jurnal = $(this).attr("data-url");
                    $("#id_jurnal").val(id);
                    $("#judul_jurnal").val(judul_jurnal);
                    $("#tahun_jurnal").val(tahun_jurnal);
                    $("#volume").val(volume);
                    $("#no_jurnal").val(no_jurnal);
                    $("#tgl_terbit").val(tanggal);
                    $("#indeks1").val(indeks1);
                    $("#indeks2").val(indeks2);
                    $("#tingkat").val(tingkat);
                    $("#penulis_ke").val(penulis_ke);
                    $("#url_jurnal").val(url_jurnal);
                // alert(id);
                })

                $(".penelitian").click(function(){
                    var id = $(this).attr("data-idpenelitian");
                    var judul_penelitian = $(this).attr("data-judul-penelitian");
                    var tahun_penelitian = $(this).attr("data-tahun-penelitian");
                    var sumber_dana_penelitian = $(this).attr("data-sumber-dana-penelitian");
                    var jumlah_dana_penelitian = $(this).attr("data-jumlah-dana-penelitian");
                    var status_penelitian = $(this).attr("data-status-penelitian");
                    var url_penelitian = $(this).attr("data-url-penelitian");
                    $("#id_penelitian").val(id);
                    $("#judul_penelitian").val(judul_penelitian);
                    $("#tahun_penelitian").val(tahun_penelitian);
                    $("#sumber_dana_penelitian").val(sumber_dana_penelitian);
                    $("#jumlah_dana_penelitian").val(jumlah_dana_penelitian);
                    $("#status_penelitian").val(status_penelitian);
                    $("#url_penelitian").val(url_penelitian);
                // alert(id);
                })

                $(".pengabdian").click(function(){
                    var id = $(this).attr("data-idpengabdian");
                    var judul_pengabdian = $(this).attr("data-judul-pengabdian");
                    var tahun_pengabdian = $(this).attr("data-tahun-pengabdian");
                    var sumber_dana_pengabdian = $(this).attr("data-sumber-dana-pengabdian");
                    var jumlah_dana_pengabdian = $(this).attr("data-jumlah-dana-pengabdian");
                    var status_pengabdian = $(this).attr("data-status-pengabdian");
                    var url_pengabdian = $(this).attr("data-url-pengabdian");
                    $("#id_pengabdian").val(id);
                    $("#judul_pengabdian").val(judul_pengabdian);
                    $("#tahun_pengabdian").val(tahun_pengabdian);
                    $("#sumber_dana_pengabdian").val(sumber_dana_pengabdian);
                    $("#jumlah_dana_pengabdian").val(jumlah_dana_pengabdian);
                    $("#status_pengabdian").val(status_pengabdian);
                    $("#url_pengabdian").val(url_pengabdian);
                // alert(id);
                })

                $(".penghargaan").click(function(){
                    var id = $(this).attr("data-idpenghargaan");
                    var nama_penghargaan = $(this).attr("data-nama-penghargaan");
                    var tahun_penghargaan = $(this).attr("data-tahun-penghargaan");
                    var pemberi = $(this).attr("data-pemberi");
                    var tingkat_penghargaan = $(this).attr("data-tingkat-penghargaan");
                    $("#id_penghargaan").val(id);
                    $("#nama_penghargaan").val(nama_penghargaan);
                    $("#tahun_penghargaan").val(tahun_penghargaan);
                    $("#pemberi").val(pemberi);
                    $("#tingkat_penghargaan").val(tingkat_penghargaan);
                // alert(id);
                })

                $(".prosiding").click(function(){
                    var id = $(this).attr("data-idprosiding");
                    var nama_prosiding = $(this).attr("data-nama-prosiding");
                    var judul_artikel = $(this).attr("data-judul-artikel");
                    var tempat_prosiding = $(this).attr("data-tempat-prosiding");
                    var tanggal1 = $(this).attr("data-tanggal1");
                    var tanggal2 = $(this).attr("data-tanggal2");
                    var penulis_prosiding = $(this).attr("data-penulis-prosiding");
                    var url_prosiding = $(this).attr("data-url-prosiding");
                    $("#id_penghargaan").val(id);
                    $("#nama_prosiding").val(nama_prosiding);
                    $("#judul_artikel").val(judul_artikel);
                    $("#tempat_prosiding").val(tempat_prosiding);
                    $("#tanggal1").val(tanggal1);
                    $("#tanggal2").val(tanggal2);
                    $("#penulis_prosiding").val(penulis_prosiding);
                    $("#url_prosiding").val(url_prosiding);
                // alert(id);
                })

                $(".datafileAch").click(function(){
                    var idAch = $(this).attr("data-idAch");
                    var jenisAch = $(this).attr("data-jenisAch");
                    $("#jenisAch").val(jenisAch);
                    $("#idAch").val(idAch);
                // alert(id);
                })

                $(".datafilePend").click(function(){
                    var idPend = $(this).attr("data-idPend");
                    var jenisPend = $(this).attr("data-jenisPend");
                    $("#jenisPend").val(jenisPend);
                    $("#idPend").val(idPend);
                // alert(id);
                })

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // sandy nambah dibawah
                $(".pangkatdata").click(function(){
                    var idpgkt = $(this).attr("data-idpgkt");
                    var jenispgkt = $(this).attr("data-jenispgkt");
                    var golpgkt = $(this).attr("data-golpgkt");
                    var tmtpgkt = $(this).attr("data-tmtpgkt");
                    switch(golpgkt){
                        case 'I/a': var golpgktedit = 1; break;    case 'I/b': var golpgktedit = 2; break;
                        case 'I/c': var golpgktedit = 3; break;    case 'I/d': var golpgktedit = 4; break;
                        case 'II/a': var golpgktedit = 5; break;   case 'II/b': var golpgktedit = 6; break;
                        case 'II/c': var golpgktedit = 7; break;   case 'II/d': var golpgktedit = 8; break;
                        case 'III/a': var golpgktedit = 9; break;  case 'III/b': var golpgktedit = 10; break;
                        case 'III/c': var golpgktedit = 11; break; case 'III/d': var golpgktedit = 12; break;
                        case 'IV/a': var golpgktedit = 13; break;  case 'IV/b': var golpgktedit = 14; break;
                        case 'IV/c': var golpgktedit = 15; break;  case 'IV/d': var golpgktedit = 16; break;
                        case 'IV/e': var golpgktedit = 17; break; 
                        default: var golpgktedit = 0;
                    }

                    $("#pangkatedit").val(jenispgkt);
                    $("#editgolruang").val(golpgktedit);
                    $("#tmtpgkt").val(tmtpgkt);
                    $("#idpgkt").val(idpgkt);
                // alert(id);
                })

                $(".fungsionaldata").click(function(){
                    var idfung = $(this).attr("data-idfung");
                    var jabfung = $(this).attr("data-jabfung");
                    var awalfung = $(this).attr("data-awalfung");
                    switch(jabfung){
                        case 'Staff Pengajar': var editjabfung = 1; break;    
                        case 'Asisten Ahli': var editjabfung = 2; break;
                        case 'Lektor': var editjabfung = 3; break;    
                        case 'Lektor Kepala': var editjabfung = 4; break;
                        case 'Guru Besar': var editjabfung = 5; break;    
                        default: var editjabfung = 0;
                    }

                    $("#idfung").val(idfung);
                    $("#jabfung").val(jabfung);
                    $("#awalfung").val(awalfung);
                    $("#editjabfung").val(editjabfung);
                // alert(id);
                })

                $(".strukturaldata").click(function(){
                    var idstruk = $(this).attr("data-idstruk");
                    var jabstruk = $(this).attr("data-jabstruk");
                    var ketstruk = $(this).attr("data-ketstruk");
                    var awalstruk = $(this).attr("data-awalstruk");
                    var akhirstruk = $(this).attr("data-akhirstruk");
                    switch(jabstruk){
                        case 'Ketua Prodi': var editjabstruk = 1; break;    
                        case 'Sekretaris Prodi': var editjabstruk = 2; break;
                        case 'Ketua Departemen': var editjabstruk = 3; break;    
                        case 'Sekretaris Departemen': var editjabstruk = 4; break;
                        case 'Ketua Laboratorium': var editjabstruk = 5; break;   
                        case 'Wakil Dekan': var editjabstruk = 6; break;    
                        case 'Dekan': var editjabstruk = 7; break;
                        case 'Wakil Rektor': var editjabstruk = 8; break;    
                        case 'Rektor': var editjabstruk = 9; break;
                        case 'Wakil Direktur': var editjabstruk = 10; break; 
                        case 'Direktur': var editjabstruk = 11; break;    
                        default: var editjabstruk = 0;
                    }

                    $("#idstruk").val(idstruk);
                    $("#jabstrukedit").val(jabstruk);
                    $("#ketstruk").val(ketstruk);
                    $("#awalstruk").val(awalstruk);
                    $("#akhirstruk").val(akhirstruk);
                    $("#editjabstruk").val(editjabstruk);
                // alert(id);
                })

                $(".datafile").click(function(){
                    var idAct = $(this).attr("data-idAct");
                    var jenisAct = $(this).attr("data-jenisAct");
                    $("#jenisAct").val(jenisAct);
                    $("#idAct").val(idAct);
                // alert(id);
                })

            </script>
    </body>
</html>