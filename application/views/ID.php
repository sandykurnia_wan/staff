<!DOCTYPE html>
<html>
<head>
	<title>ID Card <?php //echo $name ?> </title>
</head>
<style type="text/css">
	body{
		height: 550px;
		width: 900px;
	}
	.image { 
	   	position: relative; 
	   	width: 100%; /* for IE 6 */
	}

	h2 { 
	   	position: absolute; 
	   	top: 135px; 
	   	left: 0; 
	   	width: 100%; 
	   	text-align: center;
	   	align-items: center;
	}
	h4.email { 
	   	position: absolute; 
	   	top: 430px; 
	   	left: -290px; 
	   	width: 100%; 
	   	text-align: center;
	   	align-items: left;
	   	color: white;
	}
	h4.hp { 
	   	position: absolute; 
	   	top: 430px; 
	   	left: 290px; 
	   	width: 100%; 
	   	text-align: center;
	   	align-items: left;
	   	color: white;
	}
	
	.front{
	  	width: 100%;
	  	max-width: 100%;
	}

	.qr { 
		position:absolute; 
		right: 44%;
		top: 67.5%;
		width: 10%;
		max-width: 10%;
		/*top: 950px; 
		left: 415px; 
		z-index: 2;*/ 
	} 
</style>
<body onload="printID()" >
	<div class="image">
		<img src="<?php echo base_url('assets/img/ID/BackSide.jpg')?>" class="back" alt="" />
		<h2> <?php echo $depan.' '.$nama.', '.$blkg ?></h2>
		<h4 class="email">
			<?php echo $username ?> </br>
			<?php echo $email ?>
		</h4>
		<h4 class="hp">
			<?php echo $hp1 ?> <br/>
			<?php echo $hp2 ?>
		</h4>
	</div>

	<div class="image" style="margin-top: 5px">
		<img src="<?php echo base_url('assets/img/ID/FrontSide.jpg')?>" class="front" alt="" />
		<img src="https://api.qrserver.com/v1/create-qr-code/?data=<?php echo site_url('/visitor/profile/'.$id_profil);?>&size=100x100" class ="qr" alt="" title="Scan Me" />
	</div>

	 


	<!-- Script print -->
    <script>
	    function printID() {
	    	window.print();
	    }
    </script>
</body>
</html>