<!DOCTYPE html>
<html>
    <head>
        <?php 
            $id = $this->session->userdata('username');
            $ppdata = $this->mdosen->getProfpic($id);
            $profpic = $ppdata[0]['filename'];
            // print_r();die;
        ?>
        <meta charset="UTF-8">
        <title>Staff Site | <?php echo $title; ?> </title>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/logo undip.png')?>"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/ionicons-2.0.1/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/flat/blue.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/morris/morris.css')?>" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css')?>" rel="stylesheet" type="text/css" /> 
        <!-- Daterange picker --> 
        <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/daterangepicker/daterangepicker-bs3.css')?>">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/datepicker3.css')?>">

        <script type="text/javascript" src="<?php echo base_url('assets/ckeditor/ckeditor.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/ckeditor/styles.js')?>"></script>
        
        <!-- Input Masking -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskedinput.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.priceformat.min.js')?>"></script>
        
        <!-- iCheck 1.0.1 -->
        <script type="text/javascript" src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/iCheck/icheck.min.js')?>"></script>

        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.css')?>" type="text/css" />
        <!-- DataTables -->
        <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>"> </script>
        <script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>" > </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .profpic{
                object-fit: cover;
                width: 150px;
                height: 100px;
            }
        </style>
    </head>
    <body class="skin-blue">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <a href="<?php echo site_url('dosen');?>" class="logo"><b>STAFF</b>SITE</a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" class="user-image profpic" alt="User Image"/>
                                    <span class="hidden-xs"><?php echo $name?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" class="img-circle profpic" alt="User Image" />
                                        <p>
                                            <?php echo $name?>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('dosen/changePassword') ?>" class="btn btn-default btn-flat">Kelola Akun</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="image" align="center">
                            <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" class="img-circle profpic" alt="User Image" />
                        </div>
                        
                        </br>
                        <div class="info" align="center">
                            <p><?php echo $name?></p>

                            <a href="#"> Dosen</a>
                        </div>
                    </div>
                    <!-- search form -->
<!--                     <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form> -->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Navigasi</li>
                        <li class="treeview">
                            <a href="<?php echo site_url('dosen') ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?php echo site_url('dosen/dataPribadi') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Data Pribadi</span>
                            </a>
                            <!-- <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Boxed</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Fixed</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
                            </ul> -->
                        </li>
                        <li>
                            <a href="<?php echo site_url('dosen/riwayatPend') ?>">
                                <i class="glyphicon glyphicon-education"></i> <span>Riwayat Pendidikan</span>
                            </a>
                        </li>            
                        <li class="treeview">
                            <a href="<?php echo site_url('dosen/activity') ?>">
                                <i class="glyphicon glyphicon-music"></i>
                                <span>Kegiatan</span>
                                <!-- <i class="fa fa-angle-left pull-right"></i> -->
                            </a>
                            <!-- <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Morris</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Flot</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Inline charts</a></li>
                            </ul> -->
                        </li>            
                        <li class="treeview">
                            <a href="<?php echo site_url('dosen/achievement') ?>">
                                <i class="glyphicon glyphicon-star"></i>
                                <span>Pencapaian</span>
                                <!-- <i class="fa fa-angle-left pull-right"></i> -->
                            </a>
                            <!-- <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> General</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Icons</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Buttons</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Sliders</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Timeline</a></li>
                                <li><a href="#"><i class="fa fa-circle-o"></i> Modals</a></li>
                            </ul> -->
                        </li>
                        <li class="treeview">
                            <a href=" <?php echo site_url('dosen/idcard') ?>" target="_blank">
                                <i class="glyphicon glyphicon-qrcode"></i>
                                <span>Kartu Nama</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href=" <?php echo site_url('dosen/tema') ?>" target="_blank">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Profil Dosen</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href=" <?php echo site_url('dosen/aturCV') ?>">
                                <i class="glyphicon glyphicon-list"></i> 
                                <span>Curiculum Vitae</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div id="contents"><?= $contents ?></div>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2017 <a href="http://if.undip.ac.id">Informatika Undip</a>.</strong> All rights reserved.
            </footer>
            </div><!-- ./wrapper -->

            <!-- jQuery 2.1.3 -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
            <!-- datepicker -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
            <!-- SlimScroll -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
            <!-- FastClick -->
            <script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
            <!-- sweet alert -->
            <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">
            <!-- confirm delete Kegiatan -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confDelKeg').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus kegiatan?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <!-- confirm delete dokumen kegiatan -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confDelDok').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus dokumen kegiatan?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <!-- confirm delete Pendidikan -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confDelPend').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus Pendidikan?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <!-- confirm delete Pencapaian -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confDelAch').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus Pencapaian?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <!-- confirm delete dokumen Pencapaian -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confDelDokAch').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus Dokumen Pencapaian?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <script type="text/javascript">
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 2000);

                $(".org").click(function(){
                    var id = $(this).attr("data-idorg");
                    var judul = $(this).attr("data-judul");
                    var tahun = $(this).attr("data-tahun");
                    var jab = $(this).attr("data-jab");
                    var no = $(this).attr("data-no");
                    $("#judul").val(judul);
                    $("#tahun").val(tahun);
                    $("#jab").val(jab);
                    $("#no").val(no);
                    $("#idorg").val(id);
                // alert(id);
                })

                $(".buku").click(function(){
                    var id = $(this).attr("data-idbuku");
                    var judulbuku = $(this).attr("data-judulbuku");
                    var tahunterbit = $(this).attr("data-tahunterbit");
                    var halaman = $(this).attr("data-halaman");
                    var penerbit = $(this).attr("data-penerbit");
                    $("#judulbuku").val(judulbuku);
                    $("#tahunterbit").val(tahunterbit);
                    $("#halaman").val(halaman);
                    $("#penerbit").val(penerbit);
                    $("#idbuku").val(id);
                // alert(id);
                })

                $(".review").click(function(){
                    var id = $(this).attr("data-idrev");
                    var judul = $(this).attr("data-judul");
                    var penyelenggara = $(this).attr("data-penyelenggara");
                    var tingkat = $(this).attr("data-tingkat");
                    var tanggal = $(this).attr("data-tanggal");
                    $("#judulrev").val(judul);
                    $("#penyelenggara").val(penyelenggara);
                    $("#tingkat").val(tingkat);
                    $("#tanggal").val(tanggal);
                    $("#idrev").val(id);
                // alert(id);
                })

                $(".narasumber").click(function(){
                    var id = $(this).attr("data-idnara");
                    var judul = $(this).attr("data-judul");
                    var penyelenggara = $(this).attr("data-penyelenggara");
                    var tanggal = $(this).attr("data-tanggal");
                    var tingkat = $(this).attr("data-tingkat");
                    $("#judulNara").val(judul);
                    $("#penyelenggaraNara").val(penyelenggara);
                    $("#tanggalNara").val(tanggal);
                    $("#tingkatNara").val(tingkat);
                    $("#idnara").val(id);
                // alert(id);
                })

                $(".leadership").click(function(){
                    var id = $(this).attr("data-idleader");
                    var jabatan = $(this).attr("data-jabatan");
                    var tingkat = $(this).attr("data-tingkat");
                    var periode = $(this).attr("data-periode");
                    $("#jabatan").val(jabatan);
                    $("#tingkatLeadership").val(tingkat);
                    $("#periode").val(periode);
                    $("#idleader").val(id);
                // alert(id);
                })

                $(".viewformal").click(function(){
                    var id = $(this).attr("data-idformal-view");
                    var jenjang = $(this).attr("data-jenjang-view");
                    var nama_univ = $(this).attr("data-nama-univ-view");
                    var bidang = $(this).attr("data-bidang-view");
                    var thn_masuk = $(this).attr("data-thn-masuk-view");
                    var thn_lulus = $(this).attr("data-thn-lulus-view");
                    var judul_ta = $(this).attr("data-judul-ta-view");
                    var dosbing1 = $(this).attr("data-dosbing1-view");
                    var dosbing2 = $(this).attr("data-dosbing2-view");
                    $("#id_formal_view").val(id);
                    $("#jenjang_view").val(jenjang);
                    $("#nama_univ_view").val(nama_univ);
                    $("#bidang_view").val(bidang);
                    $("#thn_masuk_view").val(thn_masuk);
                    $("#thn_lulus_view").val(thn_lulus);
                    $("#judul_ta_view").val(judul_ta);
                    $("#dosbing1_view").val(dosbing1);
                    $("#dosbing2_view").val(dosbing2);
                // alert(id);
                })

                $(".editformal").click(function(){
                    var id = $(this).attr("data-idformal-edit");
                    var jenjang = $(this).attr("data-jenjang-edit");
                    var nama_univ = $(this).attr("data-nama-univ-edit");
                    var bidang = $(this).attr("data-bidang-edit");
                    var thn_masuk = $(this).attr("data-thn-masuk-edit");
                    var thn_lulus = $(this).attr("data-thn-lulus-edit");
                    var judul_ta = $(this).attr("data-judul-ta-edit");
                    var dosbing1 = $(this).attr("data-dosbing1-edit");
                    var dosbing2 = $(this).attr("data-dosbing2-edit");
                    $("#id_formal_edit").val(id);
                    $("#jenjang_edit").val(jenjang);
                    $("#nama_univ_edit").val(nama_univ);
                    $("#bidang_edit").val(bidang);
                    $("#thn_masuk_edit").val(thn_masuk);
                    $("#thn_lulus_edit").val(thn_lulus);
                    $("#judul_ta_edit").val(judul_ta);
                    $("#dosbing1_edit").val(dosbing1);
                    $("#dosbing2_edit").val(dosbing2);
                // alert(id);
                })

                $(".viewnonformal").click(function(){
                    var id = $(this).attr("data-idnonformal-view");
                    var nama_kegiatan = $(this).attr("data-nama-kegiatan-view");
                    var tgl_kegiatan = $(this).attr("data-tgl-kegiatan-view");
                    var tgl_kegiatan_2 = $(this).attr("data-tgl-kegiatan-2-view");
                    var penyelenggara = $(this).attr("data-penyelenggara-view");
                    var tempat = $(this).attr("data-tempat-view");
                    var pola = $(this).attr("data-pola-view");
                    $("#id_non_formal_view").val(id);
                    $("#nama_kegiatan_view").val(nama_kegiatan);
                    $("#tgl_kegiatan_view").val(tgl_kegiatan);
                    $("#tgl_kegiatan_2_view").val(tgl_kegiatan_2);
                    $("#penyelenggara_view").val(penyelenggara);
                    $("#tempat_view").val(tempat);
                    $("#pola_view").val(pola);
                // alert(id);
                })

                $(".editnonformal").click(function(){
                    var id = $(this).attr("data-idnonformal-edit");
                    var nama_kegiatan = $(this).attr("data-nama-kegiatan-edit");
                    var tgl_kegiatan = $(this).attr("data-tgl-kegiatan-edit");
                    var tgl_kegiatan_2 = $(this).attr("data-tgl-kegiatan-2-edit");
                    var penyelenggara = $(this).attr("data-penyelenggara-edit");
                    var tempat = $(this).attr("data-tempat-edit");
                    var pola = $(this).attr("data-pola-edit");
                    $("#id_non_formal_edit").val(id);
                    $("#nama_kegiatan_edit").val(nama_kegiatan);
                    $("#tgl_kegiatan_edit").val(tgl_kegiatan);
                    $("#tgl_kegiatan_2_edit").val(tgl_kegiatan_2);
                    $("#penyelenggara_edit").val(penyelenggara);
                    $("#tempat_edit").val(tempat);
                    $("#pola_edit").val(pola);
                // alert(id);
                })

                $(".hki").click(function(){
                    var id = $(this).attr("data-idhki");
                    var judul_hki = $(this).attr("data-judul-hki");
                    var tahun = $(this).attr("data-tahun");
                    var jenis_hki = $(this).attr("data-jenis");
                    var no_hki = $(this).attr("data-no-hki");
                    $("#id_hki").val(id);
                    $("#judul_hki").val(judul_hki);
                    $("#tahun").val(tahun);
                    $("#jenis_hki").val(jenis_hki);
                    $("#no_hki").val(no_hki);
                // alert(id);
                })

                $(".viewjurnal").click(function(){
                    var id = $(this).attr("data-idjurnal-view");
                    var judul_jurnal = $(this).attr("data-judul-jurnal-view");
                    var judul_artikel = $(this).attr("data-judul-artikel-view");
                    var tahun_jurnal = $(this).attr("data-tahun-jurnal-view");
                    var volume = $(this).attr("data-volume-view");
                    var no_jurnal = $(this).attr("data-no-jurnal-view");
                    var tanggal = $(this).attr("data-tanggal-view");
                    var indeks = $(this).attr("data-indeks-view");
                    var tingkat = $(this).attr("data-tingkat-view");
                    var penulis_ke = $(this).attr("data-penulis-view");
                    var url_jurnal = $(this).attr("data-url-view");
                    var indeks1_view = $("#indeks1_view").val();
                    var indeks2_view = $("#indeks2_view").val();
                    if (indeks1_view == indeks) {
                        // alert(indeks);
                        radiobtn = document.getElementById("indeks1_view");
                        radiobtn.checked = true;
                    }else if (indeks2_view == indeks) {
                        // alert(indeks);
                        radiobtn = document.getElementById("indeks2_view");
                        radiobtn.checked = true;
                    }
                    $("#id_jurnal_view").val(id);
                    $("#judul_jurnal_view").val(judul_jurnal);
                    $("#judul_artikel_view").val(judul_artikel);
                    $("#tahun_jurnal_view").val(tahun_jurnal);
                    $("#volume_view").val(volume);
                    $("#no_jurnal_view").val(no_jurnal);
                    $("#tgl_terbit_view").val(tanggal);
                    $("#tingkat_view").val(tingkat);
                    $("#penulis_ke_view").val(penulis_ke);
                    $("#url_jurnal_view").val(url_jurnal);
                // alert(id);
                })

                $(".editjurnal").click(function(){
                    var id = $(this).attr("data-idjurnal-edit");
                    var judul_jurnal = $(this).attr("data-judul-jurnal-edit");
                    var judul_artikel = $(this).attr("data-judul-artikel-edit");
                    var tahun_jurnal = $(this).attr("data-tahun-jurnal-edit");
                    var volume = $(this).attr("data-volume-edit");
                    var no_jurnal = $(this).attr("data-no-jurnal-edit");
                    var tanggal = $(this).attr("data-tanggal-edit");
                    var indeks = $(this).attr("data-indeks-edit");
                    var tingkat = $(this).attr("data-tingkat-edit");
                    var penulis_ke = $(this).attr("data-penulis-edit");
                    var url_jurnal = $(this).attr("data-url-edit");
                    var indeks1_edit = $("#indeks1_edit").val();
                    var indeks2_edit = $("#indeks2_edit").val();
                    if (indeks1_edit == indeks) {
                        // alert(indeks);
                        radiobtn = document.getElementById("indeks1_edit");
                        radiobtn.checked = true;
                    }else if (indeks2_edit == indeks) {
                        // alert(indeks);
                        radiobtn = document.getElementById("indeks2_edit");
                        radiobtn.checked = true;
                    }
                    $("#id_jurnal_edit").val(id);
                    $("#judul_jurnal_edit").val(judul_jurnal);
                    $("#judul_artikel_edit").val(judul_artikel);
                    $("#tahun_jurnal_edit").val(tahun_jurnal);
                    $("#volume_edit").val(volume);
                    $("#no_jurnal_edit").val(no_jurnal);
                    $("#tgl_terbit_edit").val(tanggal);  
                    $("#tingkat_edit").val(tingkat);
                    $("#penulis_ke_edit").val(penulis_ke);
                    $("#url_jurnal_edit").val(url_jurnal);
                // alert(id);
                })

                $(".penelitian").click(function(){
                    var id = $(this).attr("data-idpenelitian");
                    var judul_penelitian = $(this).attr("data-judul-penelitian");
                    var tahun_penelitian = $(this).attr("data-tahun-penelitian");
                    var sumber_dana_penelitian = $(this).attr("data-sumber-dana-penelitian");
                    var jumlah_dana_penelitian = $(this).attr("data-jumlah-dana-penelitian");
                    var status_penelitian = $(this).attr("data-status-penelitian");
                    $("#id_penelitian").val(id);
                    $("#judul_penelitian").val(judul_penelitian);
                    $("#tahun_penelitian").val(tahun_penelitian);
                    $("#sumber_dana_penelitian").val(sumber_dana_penelitian);
                    $("#jumlah_dana_penelitian").val(jumlah_dana_penelitian);
                    $("#status_penelitian").val(status_penelitian);
                // alert(id);
                })

                $(".pengabdian").click(function(){
                    var id = $(this).attr("data-idpengabdian");
                    var judul_pengabdian = $(this).attr("data-judul-pengabdian");
                    var tahun_pengabdian = $(this).attr("data-tahun-pengabdian");
                    var sumber_dana_pengabdian = $(this).attr("data-sumber-dana-pengabdian");
                    var jumlah_dana_pengabdian = $(this).attr("data-jumlah-dana-pengabdian");
                    var status_pengabdian = $(this).attr("data-status-pengabdian");
                    $("#id_pengabdian").val(id);
                    $("#judul_pengabdian").val(judul_pengabdian);
                    $("#tahun_pengabdian").val(tahun_pengabdian);
                    $("#sumber_dana_pengabdian").val(sumber_dana_pengabdian);
                    $("#jumlah_dana_pengabdian").val(jumlah_dana_pengabdian);
                    $("#status_pengabdian").val(status_pengabdian);
                // alert(id);
                })

                $(".penghargaan").click(function(){
                    var id = $(this).attr("data-idpenghargaan");
                    var nama_penghargaan = $(this).attr("data-nama-penghargaan");
                    var tahun_penghargaan = $(this).attr("data-tahun-penghargaan");
                    var pemberi = $(this).attr("data-pemberi");
                    var tingkat_penghargaan = $(this).attr("data-tingkat-penghargaan");
                    $("#id_penghargaan").val(id);
                    $("#nama_penghargaan").val(nama_penghargaan);
                    $("#tahun_penghargaan").val(tahun_penghargaan);
                    $("#pemberi").val(pemberi);
                    $("#tingkat_penghargaan").val(tingkat_penghargaan);
                // alert(id);
                })

                $(".viewprosiding").click(function(){
                    var id = $(this).attr("data-idprosiding-view");
                    var nama_prosiding = $(this).attr("data-nama-prosiding-view");
                    var judul_artikel = $(this).attr("data-judul-artikel-view");
                    var tempat_prosiding = $(this).attr("data-tempat-prosiding-view");
                    var tanggal1 = $(this).attr("data-tanggal1-view");
                    var tanggal2 = $(this).attr("data-tanggal2-view");
                    var penulis_ke = $(this).attr("data-penuliske-view");
                    var url_prosiding = $(this).attr("data-url-prosiding-view");
                    $("#id_prosiding_view").val(id);
                    $("#nama_prosiding_view").val(nama_prosiding);
                    $("#judul_artikel_view").val(judul_artikel);
                    $("#tempat_prosiding_view").val(tempat_prosiding);
                    $("#tanggal1_view").val(tanggal1);
                    // document.getElementById('penulis_ke_view').value=coba;
                    $("#tanggal2_view").val(tanggal2);
                    $("#penuliske_view").val(penulis_ke);
                    $("#url_prosiding_view").val(url_prosiding);
                // alert(id);
                })

                $(".editprosiding").click(function(){
                    var id = $(this).attr("data-idprosiding-edit");
                    var nama_prosiding = $(this).attr("data-nama-prosiding-edit");
                    var judul_artikel = $(this).attr("data-judul-artikel-edit");
                    var tempat_prosiding = $(this).attr("data-tempat-prosiding-edit");
                    var tanggal1 = $(this).attr("data-tanggal1-edit");
                    var tanggal2 = $(this).attr("data-tanggal2-edit");
                    var penulis_ke = $(this).attr("data-penuliske-edit");
                    // alert(penulis_ke);
                    var url_prosiding = $(this).attr("data-url-prosiding-edit");
                    $("#id_prosiding_edit").val(id);
                    $("#nama_prosiding_edit").val(nama_prosiding);
                    $("#judul_artikel_edit").val(judul_artikel);
                    $("#tempat_prosiding_edit").val(tempat_prosiding);
                    $("#tanggal1_edit").val(tanggal1);
                    $("#tanggal2_edit").val(tanggal2);
                    $("#penuliske_edit").val(penulis_ke);
                    $("#url_prosiding_edit").val(url_prosiding);
                // alert(id);
                })

                $(".datafileAch").click(function(){
                    var idAch = $(this).attr("data-idAch");
                    var jenisAch = $(this).attr("data-jenisAch");
                    $("#jenisAch").val(jenisAch);
                    $("#idAch").val(idAch);
                // alert(id);
                })

                $(".datafilePend").click(function(){
                    var idPend = $(this).attr("data-idPend");
                    var jenisPend = $(this).attr("data-jenisPend");
                    $("#jenisPend").val(jenisPend);
                    $("#idPend").val(idPend);
                // alert(id);
                })

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // sandy nambah dibawah
                $(".pangkatdata").click(function(){
                    var idpgkt = $(this).attr("data-idpgkt");
                    var jenispgkt = $(this).attr("data-jenispgkt");
                    var golpgkt = $(this).attr("data-golpgkt");
                    var tmtpgkt = $(this).attr("data-tmtpgkt");
                    switch(golpgkt){
                        case 'I/a': var golpgktedit = 1; break;    case 'I/b': var golpgktedit = 2; break;
                        case 'I/c': var golpgktedit = 3; break;    case 'I/d': var golpgktedit = 4; break;
                        case 'II/a': var golpgktedit = 5; break;   case 'II/b': var golpgktedit = 6; break;
                        case 'II/c': var golpgktedit = 7; break;   case 'II/d': var golpgktedit = 8; break;
                        case 'III/a': var golpgktedit = 9; break;  case 'III/b': var golpgktedit = 10; break;
                        case 'III/c': var golpgktedit = 11; break; case 'III/d': var golpgktedit = 12; break;
                        case 'IV/a': var golpgktedit = 13; break;  case 'IV/b': var golpgktedit = 14; break;
                        case 'IV/c': var golpgktedit = 15; break;  case 'IV/d': var golpgktedit = 16; break;
                        case 'IV/e': var golpgktedit = 17; break; 
                        default: var golpgktedit = 0;
                    }

                    $("#pangkatedit").val(jenispgkt);
                    $("#editgolruang").val(golpgktedit);
                    $("#goledit").val(golpgkt);
                    $("#tmtpgkt").val(tmtpgkt);
                    $("#idpgkt").val(idpgkt);
                    $("#pangkatlama").val(jenispgkt);
                // alert(id);
                })

                $(".fungsionaldata").click(function(){
                    var idfung = $(this).attr("data-idfung");
                    var jabfung = $(this).attr("data-jabfung");
                    var awalfung = $(this).attr("data-awalfung");
                    switch(jabfung){
                        case 'Staff Pengajar': var editjabfung = 1; break;    
                        case 'Asisten Ahli': var editjabfung = 2; break;
                        case 'Lektor': var editjabfung = 3; break;    
                        case 'Lektor Kepala': var editjabfung = 4; break;
                        case 'Guru Besar': var editjabfung = 5; break;    
                        default: var editjabfung = 0;
                    }

                    $("#idfung").val(idfung);
                    $("#jabfungedit").val(jabfung);
                    $("#awalfung").val(awalfung);
                    $("#editjabfung").val(editjabfung);
                    $("#jabatanlama").val(jabfung);
                // alert(id);
                })

                $(".strukturaldata").click(function(){
                    var idstruk = $(this).attr("data-idstruk");
                    var jabstruk = $(this).attr("data-jabstruk");
                    var ketstruk = $(this).attr("data-ketstruk");
                    var awalstruk = $(this).attr("data-awalstruk");
                    var akhirstruk = $(this).attr("data-akhirstruk");
                    switch(jabstruk){
                        case 'Ketua Prodi': var editjabstruk = 1; break;    
                        case 'Sekretaris Prodi': var editjabstruk = 2; break;
                        case 'Ketua Departemen': var editjabstruk = 3; break;    
                        case 'Sekretaris Departemen': var editjabstruk = 4; break;
                        case 'Ketua Laboratorium': var editjabstruk = 5; break;   
                        case 'Wakil Dekan': var editjabstruk = 6; break;    
                        case 'Dekan': var editjabstruk = 7; break;
                        case 'Wakil Rektor': var editjabstruk = 8; break;    
                        case 'Rektor': var editjabstruk = 9; break;
                        case 'Wakil Direktur': var editjabstruk = 10; break; 
                        case 'Direktur': var editjabstruk = 11; break;    
                        default: var editjabstruk = 0;
                    }

                    $("#idstruk").val(idstruk);
                    $("#jabstrukedit").val(jabstruk);
                    $("#ketstruk").val(ketstruk);
                    $("#awalstruk").val(awalstruk);
                    $("#akhirstruk").val(akhirstruk);
                    $("#editjabstruk").val(editjabstruk);
                // alert(id);
                })

                $(".datafile").click(function(){
                    var idAct = $(this).attr("data-idAct");
                    var jenisAct = $(this).attr("data-jenisAct");
                    $("#jenisAct").val(jenisAct);
                    $("#idAct").val(idAct);
                // alert(id);
                })

                //Date picker
                $('.datepicker').datepicker({
                    autoclose: true,
                    orientation: "bottom auto",
                    endDate: "0d",
                    todayBtn: true,
                    todayHighlight: true,
                    format: "yyyy-mm-dd"
                })

                $('.input-daterange').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true,
                    endDate: "0d",
                    todayBtn: true,
                    todayHighlight: true,
                    format: "yyyy-mm-dd"
                });

                $('.yearpicker').datepicker({            
                    // endYear: "0y",
                    autoclose: true,
                    //minViewMode: 2,
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years",
                    // startDate: '2014',
                    endDate: new Date()
                });

                $('.input-yearrange').datepicker({
                    format: "yyyy",
                    autoclose: true,
                    viewMode: "years", 
                    minViewMode: "years",
                    format: "yyyy",
                    endDate: new Date()
                });

                //buat range tanggal data struktural
                $('.fromDate').datepicker({
                    
                // update "toDate" defaults whenever "fromDate" changes
                }).on('changeDate', function(){
                    // set the "toDate" start to not be later than "fromDate" ends:
                    $('.toDate').datepicker('setStartDate', new Date($(this).val()));
                }); 

                $('.toDate').datepicker({
                // update "fromDate" defaults whenever "toDate" changes
                }).on('changeDate', function(){
                    // set the "fromDate" end to not be later than "toDate" starts:
                    $('.fromDate').datepicker('setEndDate', new Date($(this).val()));
                });

                $('.fromYear').datepicker({
                    
                // update "toDate" defaults whenever "fromDate" changes
                }).on('changeYear', function(){
                    // set the "toDate" start to not be later than "fromDate" ends:
                    $('.toYear').datepicker(
                        {
                            'setStartYear': new Date($(this).val()).getFullYear().toString()
                        }
                    )
                }); 

                $('.toYear').datepicker({
                // update "fromDate" defaults whenever "toDate" changes
                }).on('changeYear', function(){
                    // set the "fromDate" end to not be later than "toDate" starts:
                    $('.fromYear').datepicker(
                        {
                            'setEndYear': new Date($(this).val()).getFullYear().toString()
                        }
                    )
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                  checkboxClass: 'icheckbox_minimal-blue',
                  radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                  checkboxClass: 'icheckbox_minimal-red',
                  radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass: 'iradio_flat-green'
                });

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                    {
                      ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                      },
                      startDate: moment().subtract(29, 'days'),
                      endDate: moment()
                    },
                    function (start, end) {
                      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    }
                );

            </script>

            <!-- DATA TABES SCRIPT -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" type="text/javascript"></script>
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.js') ?>" type="text/javascript"></script>
             <script type="text/javascript">
              $(function () {
                $('#example1').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
                $('#example2').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
                $('#example3').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
                $('#example4').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
                $('#example5').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
                $('#example6').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
              });
            </script>

    </body>
</html>