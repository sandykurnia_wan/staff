<form class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-3 control-label">Jenjang : </label>
        <div class="col-sm-8">
            <select class="form-control" name="jenjang" id="jenjang_view" disabled required>
                <option value="">-Pilih Jenjang Pendidikan-</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Nama Universitas : </label>
        <div class="col-sm-8">
            <input type="text" class="form-control" placeholder="ex: Universitas Diponegoro" name="nama_univ" id="nama_univ_view" disabled required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Bidang : </label>
        <div class="col-sm-8">
            <input type="text" class="form-control" placeholder="ex: Sistem Informasi" name="bidang" id="bidang_view" disabled required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tahun Masuk : </label>
        <div class="col-sm-8">
            <input type="year" maxlength="4" pattern="[0-9]{4,4}" class="form-control" placeholder="Masukkan Tahun Masuk Anda" name="thn_masuk" id="thn_masuk_view" disabled required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tahun Lulus : </label>
        <div class="col-sm-8">
            <input type="year" maxlength="4" pattern="[0-9]{4,4}" class="form-control" placeholder="Masukkan Tahun Lulus Anda" name="thn_lulus" id="thn_lulus_view" disabled required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Judul Tugas Akhir : </label>
        <div class="col-sm-8">
            <textarea type="text" class="form-control" placeholder="ex: Pengaruh Bermain DoTA Terhadap Perilaku Remaja" name="judul_ta" id="judul_ta_view" disabled required></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Dosen Pembimbing 1 : </label>
        <div class="col-sm-8">
            <input type="text" class="form-control" placeholder="Masukkan Nama Dosen Pembimbing 1 Anda" name="dosbing1" id="dosbing1_view" disabled required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Dosen Pembimbing 2 : </label>
        <div class="col-sm-8">
            <input type="text" class="form-control" placeholder="Masukkan Nama Dosen Pembimbing 2 Anda" name="dosbing2" id="dosbing2_view" disabled>
            <input type="hidden" class="form-control"  name="id_formal" id="id_formal_view" >
        </div>
    </div>
</form>         