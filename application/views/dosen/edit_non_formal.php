<form action ="<?php echo base_url('dosen/editNonFormal')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-3 control-label">Nama Kegiatan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="ex: Seminar Data Mining" name="nama_kegiatan" id="nama_kegiatan_edit" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tanggal Pelaksanaan</label>
        <div class="col-sm-8">
            <div class="input-daterange input-group col-sm-12">
                <input type="text" style="text-align: left;" class="form-control fromDate" name="tgl_kegiatan" id="tgl_kegiatan_edit" required/>
                <span class="input-group-addon">s/d </span>
                <input type="text" style="text-align: left;" class="form-control toDate" name="tgl_kegiatan_2" id="tgl_kegiatan_2_edit" required/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Penyelenggara</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="ex: Badan Pusat Penelitian TIK" name="penyelenggara" id="penyelenggara_edit" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tempat Pelaksanaan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="ex: Gedung Prof. Imam Barjo Pleburan" name="tempat" id="tempat_edit" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Pola Kegiatan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="ex: 2 Hari" name="pola" id="pola_edit" required>
            <input type="hidden" class="form-control"  name="id_non_formal" id="id_non_formal_edit" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>         