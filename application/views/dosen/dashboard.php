<!-- Content Header (Page header) -->
<section class="content-header">
    
    <h1>
        Dashboard
        <small>Staff Site</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Staff</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<script type="text/javascript">
$(document).ready(function(){
  
function text_counter2 (input_text, target) {
  var max = 900;//$(input_text).attr("maxlength");
    $(input_text).keydown (function () {
      var text = $(input_text).val();
      var current = max - text.length;
      $(target).text("Karakter yang tersisa "+current+" karakter");
    });
}               
text_counter2 ("#deskripsi", "#description_counter2");
text_counter2 ("#deskripsiedit", "#descedit");

});
</script>
<!-- Main content -->
<section class="content">
<?php echo $this->session->flashdata('pesan') ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Selamat datang pada Staffsite Informatika Universitas Diponegoro</h3>
        </div>
        <div class="box-body" style="text-align: justify;">
            Staffsite Informatika Universitas Diponegoro merupakan sebuah website yang dapat menampilkan informasi profile Staff dari Departemen Informatika Undip. Selain menampilkan informasi, Staffsite juga dapat mengelola informasi mengenai dosen terkait tentang dapat pribadi, riwayat pendidikan, kegiatan serta pencapaian yang sudah dosen capai.
            <br>
        </div><!-- /.box-body -->
        <div class="box-footer">
            
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Deskripsi diri</h3>
        </div>
        <div class="box-body">
            <?php 
                // $deskripsi = 'halu';
                if(empty($deskripsi[0]['deskripsi'])){
                    echo 'Mohon mengisi deskripsi diri yang akan ditampilkan pada halaman profil Anda.<br>';
                    echo form_open('dosen/updateDeskripsi');
            ?>
                    <div class="form-group">
                        <textarea name="deskripsi" rows="5" id="deskripsi" cols="50" placeholder="Deskripsikan diri Anda" class="form-control"></textarea>
                        <span id="description_counter2"></span>
                    </div>
                    <script type="text/javascript">
                        // CKEDITOR.replace('deskripsi');
                    </script>
                    <div class="form-group">
                        <div class="col-sm-11">
                            <button type="reset" class="btn btn-danger pull-right">Reset</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </div>
                    <br><br>
            <?php 
                echo form_close();
            } else { ?>
            <p style="text-align: justify;  ">
                <?php
                    echo ($deskripsi[0]['deskripsi']);
                ?>
            </p>
                    <a class="btn btn-sm btn-primary org pull-right" data-toggle="modal" data-idorg="<?php //echo $o['id_org']?>" data-judul="<?php //echo $o['judul']?>" data-tahun="<?php //echo $o['tahun']?>" data-jab="<?php //echo $o['jabatan']?>" data-no="<?php //echo $o['no_anggota']?>" data-target="#editdeskripsi"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>
                    <br><hr>
            <?php
            }
            ?>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</section><!-- /.content -->
<!-- Modal Button Edit-->
<div class="modal fade" id="editdeskripsi" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Deskripsi</h4>
              </div>
              <div class="modal-body">
                <?php 
                    echo form_open('dosen/updateDeskripsi');
                ?>
                <div class="form-group">
                    <textarea name="deskripsi" id="deskripsiedit" rows="5" cols="50" placeholder="Deskripsikan diri Anda" class="form-control"><?php echo $deskripsi[0]['deskripsi'];?></textarea>
                    <span id="descedit"></span>
                </div>
                    <script type="text/javascript">
                        // CKEDITOR.replace('deskripsiedit');
                    </script>
                <div class="form-group">
                    <div class="col-sm-11">
                        <button type="reset" class="btn btn-danger pull-right">Reset</button>
                    </div>
                    <div class="col-sm-1">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                    </div>
                </div>
                <?php
                    echo form_close();
                ?>
                <br>
                <br>
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>