<!DOCTYPE html>
<html>
<section class="content-header">
    <h1>
        Riwayat
        <small>Pendidikan Formal</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Riwayat Pendidikan</a></li>
        <li class="active">Pendidikan Formal</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Pendidikan Formal</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
            <form class="form-horizontal">
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenjang Pendidikan : </label>
                        <div class="col-sm-9">
                          <select class="form-control select2" style="width: 100%;">
                            <option selected="selected"> </option>
                              <option>Strata 1</option>
                              <option>Strata 2</option>
                              <option>Strata 3</option>
                          </select>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perguruan Tinggi  : </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  placeholder="Masukkan Nama Perguruan Tinggi Anda">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Bidang Ilmu : </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  placeholder="Masukkan Bidang Ilmu Anda">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun Masuk : </label>
                            <div class="col-sm-9">
                                <input type="year" class="form-control"  placeholder="Masukkan Tahun Anda Memulai Pendidikan">
                            </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun Keluar : </label>
                        <div class="col-sm-9">
                            <input type="year" class="form-control"  placeholder="Masukkan Tahun Anda Lulus Pendidikan">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Judul Tugas Akhir : </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="3" placeholder="Masukkan Tahun Anda Lulus Pendidikan"></textarea>
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Dosbing 1 : </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  placeholder="Masukkan Nama Dosen Pembimbing Anda">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Dosbing 2 : </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control"  placeholder="Masukkan Nama Dosen Pembimbing Anda">
                        </div>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  <div class="form-group">  
                    <div class="col-sm-1 pull-right">
                      <button type="submit" class="btn btn-default pull-right">
                        <i class="fa fa-save"></i> Save
                      </button>
                    </div>
                    <div class="col-sm-1 pull-right">  
                      <button type="submit" class="btn btn-primary pull-right" disabled>
                        <i class="fa fa-edit"></i> Edit
                      </button>
                    </div>
                  </div>
                </div><!-- /. box-footer -->
            </form>
    </div><!-- /.box -->
  </section><!-- /.content -->
</html>