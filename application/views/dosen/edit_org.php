<?php echo form_open_multipart('dosen/editOrg');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Judul/ Nama Organisasi </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Judul/ Nama Organisasi Anda" name="judul" id="judul" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Tahun  </label>
        <div class="col-sm-9" >
            <input type="text" class="form-control yearpicker"  placeholder="Masukkan Tahun Anda Berorganisasi" name="tahun" id="tahun" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Jabatan  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Jabatan Anda" name="jabatan" id="jab" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">No Keanggotaan  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="30" placeholder="Masukkan Nomor Keanggotaan Anda" name="no_anggota" id="no" required>
            <input type="hidden" class="form-control"  name="idorg" id="idorg" >
        </div>
    </div>
    <div class="form-group" >
        <div  style="padding-right: 25px">
            <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>       