<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
  <!-- DataTables -->
  <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" </script>
  <script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>" </script>
</head>
<section class="content-header">
    <h1>
        Riwayat Pendidikan
        <small>Formal</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Riwayat Pendidikan</a></li>
        <li class="active">Formal</li>
    </ol>
</section>

<?php echo $this->session->flashdata('pesan') ?>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Diri</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <div class="tab-content">
                    <form class="form-horizontal">
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">No KTP : </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control"  value="<?php echo $dosen['no_ktp']?>" disabled>
                          </div>
                        </div>
                      </div>
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Tempat Lahir : </label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" value="<?php echo $dosen['tmpt_lahir']?>" disabled>
                          </div>
                        </div>
                      </div>
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Tanggal Lahir : </label>
                          <div class="col-sm-5">
                            <div class="input-group date" style="width: 73.5%">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="date" class="form-control pull-right" id="datepicker" value="<?php echo $dosen['tgl_lahir']?>" disabled>
                            </div>
                          </div>
                        </div>
                      </div>
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Alamat Rumah : </label>
                            <div class="col-sm-9">
                              <textarea class="form-control" rows="3"  disabled><?php echo $dosen['almt_rmh']?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Nomor Handphone : </label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value="<?php echo $dosen['hp1']?>" disabled>
                            </div>
                          </div>
                        </div>
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Nomor Handphone : (Alternatif) </label>
                            <div class="col-sm-9">
                              <input type="text" class="form-control" value="<?php echo $dosen['hp2']?>" disabled>
                            </div>
                          </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">  
                            <div class="col-sm-1 pull-right">
                                    <a href="<?php echo base_url('dosen/editpribadi'); ?>"
                                    <button type="submit" class="btn btn-primary pull-right">
                                      <i class="fa fa-edit"></i> Edit </a>
                                    </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->
                    
                    <div class="tab-pane" id="tab_2">
                      <form class="form-horizontal">    
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Nama : </label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" value="<?php echo $dosen['nm_dosen']?>" disabled>
                              </div>
                          </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Gelar Depan : </label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" value="<?php echo $dosen['glr_dpn']?>" disabled>
                              </div>
                          </div>
                        </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Gelar Belakang : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['glr_blkg']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat Kantor : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['almt_ktr']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telepon : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['no_tlp']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Fax : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['fax']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Email : </label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" value="<?php echo $dosen['email1']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Email(Alternatif) : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['email2']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">NIP : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['nip']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">NIDN : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['nidn']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">NPWP : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['npwp']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Kartu Pegawai : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['no_krt_peg']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Laboratory : </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="<?php echo $dosen['lab']?>" disabled>
                                    </div>
                            </div>
                          </div><!-- /.box-body -->
                          <div class="box-body">
                            <div class="form-group">  
                              <div class="col-sm-1 pull-right">
                                  <a href="<?php echo base_url('dosen/editkantor'); ?>"
                                  <button type="submit" class="btn btn-primary pull-right">
                                    <i class="fa fa-edit"></i> Edit </a>
                                  </button>
                              </div>
                            </div>
                          </div><!-- /. box-footer -->
                        </form>
                    </div>
                      <!-- /.tab-pane -->
                    
                    <div class="tab-pane" id="tab_3">
                      <form class="form-horizontal">
                        <div class="box-body">
                          <div class="form-group">
                                  <label class="col-sm-2 control-label">Scopus ID : </label>
                                  <div class="col-sm-9">
                                      <input type="text" class="form-control" value="<?php echo $dosen['scopus_id']?>" disabled>
                                  </div>
                              </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">
                                  <label class="col-sm-2 control-label">LinkedIn : </label>
                                  <div class="col-sm-9">
                                      <input type="text" class="form-control" value="<?php echo $dosen['linkedin']?>" disabled>
                                  </div>
                              </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">
                                  <label class="col-sm-2 control-label">Interest Subject: </label>
                                  <div class="col-sm-9">
                                      <textarea class="form-control" rows="3" disabled><?php echo $dosen['interest']?></textarea>
                                  </div>
                              </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">
                                  <label class="col-sm-2 control-label">Scholar : </label>
                                  <div class="col-sm-9">
                                      <input type="text" class="form-control" value="<?php echo $dosen['scholar']?>" disabled>
                                  </div>
                              </div>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                          <div class="form-group">  
                            <div class="col-sm-1 pull-right">
                                <a href="<?php echo base_url('dosen/editlain'); ?>"
                                <button type="submit" class="btn btn-primary pull-right">
                                  <i class="fa fa-edit"></i> Edit </a>
                                </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->

                  </div>
                  <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
          </div>          
    </div><!-- /.box -->
</section><!-- /.content -->
</html>

<form action ="<?php echo base_url('dosen/detFormal')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Judul Buku : </label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Buku Anda" name="judul" id="judulbuku" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tahun : </label>
        <div class="col-sm-9">
            <input type="text" pattern="[0-9]{4,4}" class="form-control"  placeholder="Masukkan Tahun Terbit Buku Anda" name="tahun" id="tahunterbit" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jumlah halaman : </label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Jumlah Halaman Buku Anda" name="jml_hlmn" id="halaman" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Penerbit : </label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Penerbit Buku Anda" name="penerbit" id="penerbit" required>
            <input type="text" class="form-control"  name="idbuku" id="idbuku" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>         