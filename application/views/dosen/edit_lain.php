          <?php echo form_open_multipart('dosen/updatelain');?>
          <fieldset>
              <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                        <label class="col-sm-3 control-label">Scopus ID </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="scopus_id" value="<?php echo $dosen[0]['scopus_id']?>" maxlength="30" placeholder="Masukkan Scopus ID Anda" data-toggle="tooltip" data-placement="bottom" title="Misal Retno_Kusumaningrum">
                            <!-- <p class="help-block">Misal: Retno_Kusumaningrum</p> -->
                        </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                        <label class="col-sm-3 control-label">Research gate  </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="rgate" value="<?php echo $dosen[0]['rgate']?>" maxlength="30" placeholder="Masukkan ID Research gate Anda" data-toggle="tooltip" data-placement="bottom" title="Misal Retno_Kusumaningrum">
                            <!-- <p class="help-block">Misal: Retno_Kusumaningrum</p> -->
                        </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                        <label class="col-sm-3 control-label">Url profil Google Scholar </label>
                        <div class="col-sm-9">
                            <input type="url" class="form-control" name="scholar" value="<?php echo $dosen[0]['scholar']?>" maxlength="250" placeholder="Masukkan Alamat Scholar Anda" data-toggle="tooltip" data-placement="bottom" title="Misal https://scholar.google.co.id/citations?user=_8i3FooAAAAJ&hl=id">
                            <!-- <p class="help-block">Misal: https://scholar.google.co.id/citations?user=_8i3FooAAAAJ&hl=id</p> -->
                        </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                        <label class="col-sm-3 control-label">Url profil Sinta </label>
                        <div class="col-sm-9">
                            <input type="url" class="form-control" name="sinta" value="<?php echo $dosen[0]['sinta']?>" maxlength="250" placeholder="Masukkan Alamat Sinta Anda" data-toggle="tooltip" data-placement="bottom" title="Misal http://sinta.ristekdikti.go.id/authors/detail?id=5045&view=network">
                            <!-- <p class="help-block">Misal: http://sinta.ristekdikti.go.id/authors/detail?id=5045&view=network</p> -->
                        </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="box-body">
                <div class="form-group" style="padding-bottom: 50px;">
                        <label class="col-sm-3 control-label">Interest Subject </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" maxlength="250" rows="3" name="interest" placeholder="ex: Data Mining, Data Warehouse" data-toggle="tooltip" data-placement="bottom" title="Masukkan minat penelitian Anda"><?php echo $dosen[0]['interest']?></textarea>
                        </div>
                    </div>
              </div><!-- /.box-body -->
              <div class="col-sm-12" style="padding-top: 20px">
                <div  style="padding-right: 25px">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
                </div>
              </div>
              
            </fieldset>
            <?php echo form_close(); ?>