<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" </script>
    <script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>" </script>
</head>
<section class="content-header">
    <h1>
        Riwayat Pendidikan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Riwayat Pendidikan</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

  <?php echo $this->session->flashdata('pesan') ?>

    <br/>
    <!-- Modal HKI -->
    <div class="modal fade" id="modal_formal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pendidikan Formal</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_formal'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Jurnal -->
    <div class="modal fade" id="modal_non_formal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pendidikan non-Formal</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_non_formal'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    
    <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Riwayat</h3>
        </div>
        <br>
        <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" style="width: 150px;" data-toggle="modal" data-target="#modal_formal"><i class="fa fa-fw fa-plus"></i> Pendidikan Formal</button>
        </div>
        <div class="col-md-2">
            <button type="button" class="btn btn-primary btn-sm" style="width: 170px;" data-toggle="modal" data-target="#modal_non_formal"><i class="fa fa-fw fa-plus"></i> Pendidikan Non-Formal</button>
        </div>
        <div class="col-md-1"></div>
        <br><br>
        <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#formal" data-toggle="tab">Pendidikan Formal</a></li>
                    <li><a href="#non_formal" data-toggle="tab">Pendidikan Non-Formal</a></li>
                  </ul>

                  <div class="tab-content">
                  
                    <div class="tab-pane active" id="formal">
                        <div class="box-body">
                          <table id="tabel_formal" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th style="text-align: center;">Jenjang</th>
                              <th style="text-align: center;">Nama Perguruan Tinggi</th>
                              <th style="text-align: center;">Kontrol</th>
                              <th style="text-align: center;">Ijazah</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php 
                                if(empty($dt_formal)){
                            ?>
                                <tr>
                                    <td colspan="6" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                  foreach($dt_formal as $d){ 
                              ?>
                              <tr style="text-align: center;">
                                <td><?php echo $d['jenjang'] ?></td>
                                <td><?php echo $d['nama_univ'] ?></td>
                                <td>
                                  <a class="btn btn-sm btn-primary viewformal" data-toggle="modal"
                                  data-idformal-view="<?php echo $d['id_formal']?>"
                                  data-jenjang-view="<?php echo $d['jenjang']?>"
                                  data-nama-univ-view="<?php echo $d['nama_univ']?>"
                                  data-bidang-view="<?php echo $d['bidang']?>"
                                  data-thn-masuk-view="<?php echo $d['thn_masuk']?>"
                                  data-thn-lulus-view="<?php echo $d['thn_lulus']?>"
                                  data-judul-ta-view="<?php echo $d['judul_ta']?>"
                                  data-dosbing1-view="<?php echo $d['dosbing1']?>"
                                  data-dosbing2-view="<?php echo $d['dosbing2']?>"
                                  data-target="#myModalViewFormal">
                                    <i class="fa fa-fw fa-bars"></i>  Detail</a>
                                  <a class="btn btn-sm bg-maroon editformal" data-toggle="modal"
                                  data-idformal-edit="<?php echo $d['id_formal']?>"
                                  data-jenjang-edit="<?php echo $d['jenjang']?>"
                                  data-nama-univ-edit="<?php echo $d['nama_univ']?>"
                                  data-bidang-edit="<?php echo $d['bidang']?>"
                                  data-thn-masuk-edit="<?php echo $d['thn_masuk']?>"
                                  data-thn-lulus-edit="<?php echo $d['thn_lulus']?>"
                                  data-judul-ta-edit="<?php echo $d['judul_ta']?>"
                                  data-dosbing1-edit="<?php echo $d['dosbing1']?>"
                                  data-dosbing2-edit="<?php echo $d['dosbing2']?>"
                                  data-target="#myModalEditFormal">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                  </a> 
                                  </td>
                                <td>
                                  <?php 
                                        $file = $this->mdosen->getfilePend('formal',$d['id_formal']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafilePend" data-toggle="modal" data-idPend="<?php echo $d['id_formal']?>" data-jenisPend='1' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Lihat</a> 
                                        <a class="btn btn-sm btn-danger formal" onclick="return confirmDelete()" href="<?php echo base_url('dosen/delDokumenPend/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                  ?>
                                </td>
                              </tr>
                            <?php
                                  }
                              } 
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="non_formal">
                        <div class="box-body">
                          <table <?php if(!empty($dt_non_formal)) echo 'id="example1"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th style="text-align: center;">Nama Kegiatan</th>
                              <th style="text-align: center;">Penyelenggara</th>
                              <th style="text-align: center;">Kontrol</th>
                              <th style="text-align: center;">Sertifikat</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php 
                                if(empty($dt_non_formal)){
                            ?>
                                <tr>
                                    <td colspan="6" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                  foreach($dt_non_formal as $c) { 
                              ?>
                              <tr style="text-align: center;">
                                <td><?php echo $c['nama_kegiatan'] ?></td>
                                <td><?php echo $c['penyelenggara'] ?></td>
                                <td>
                                  <a class="btn btn-sm btn-primary viewnonformal" data-toggle="modal"
                                  data-idnonformal-view="<?php echo $c['id_non_formal']?>"
                                  data-nama-kegiatan-view="<?php echo $c['nama_kegiatan']?>"
                                  data-tgl-kegiatan-view="<?php echo $c['tgl_kegiatan']?>"
                                  data-tgl-kegiatan-2-view="<?php echo $c['tgl_kegiatan_2']?>"
                                  data-penyelenggara-view="<?php echo $c['penyelenggara']?>"
                                  data-tempat-view="<?php echo $c['tempat']?>"
                                  data-pola-view="<?php echo $c['pola']?>"
                                  data-target="#myModalViewNonFormal">
                                    <i class="fa fa-fw fa-bars"></i>  Detail</a>
                                  <a class="btn btn-sm bg-maroon editnonformal" data-toggle="modal"
                                  data-idnonformal-edit="<?php echo $c['id_non_formal']?>"
                                  data-nama-kegiatan-edit="<?php echo $c['nama_kegiatan']?>"
                                  data-tgl-kegiatan-edit="<?php echo $c['tgl_kegiatan']?>"
                                  data-tgl-kegiatan-2-edit="<?php echo $c['tgl_kegiatan_2']?>"
                                  data-penyelenggara-edit="<?php echo $c['penyelenggara']?>"
                                  data-tempat-edit="<?php echo $c['tempat']?>"
                                  data-pola-edit="<?php echo $c['pola']?>"
                                  data-target="#myModalEditNonFormal">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah</a>
                                  <a href="" data-url="<?php echo base_url('dosen/delNonFormal/'.$c['id_non_formal']); ?>" class="btn btn-danger btn-sm confDelPend"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                </td>
                                <td>
                                  <?php 
                                        $file = $this->mdosen->getfilePend('non_formal',$c['id_non_formal']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafilePend" data-toggle="modal" data-idPend="<?php echo $c['id_non_formal']?>" data-jenisPend='2' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Lihat</a> 
                                        <a class="btn btn-sm btn-danger nonformal" onclick="return confirmDelete()" href="<?php echo base_url('dosen/delDokumenPend/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                  ?>
                                </td>
                              </tr>
                            <?php
                                  }
                              } 
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                  </div><!-- /.box -->
                </div>
            </div>
        </div>
    </div>
  </section><!-- /.content -->

<!-- Modal -->
  <div class="modal fade" id="myModalEditFormal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Pendidikan Formal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_formal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="myModalViewFormal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Detail Data Pendidikan Formal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/view_formal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="myModalEditNonFormal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Pendidikan Non Formal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_non_formal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="myModalViewNonFormal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Detail Data Pendidikan Non Formal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/view_non_formal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="modaluploadfile" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Tambah Ijazah / Sertifikat</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/add_dokumen_pend'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
<script type="text/javascript">
  function confirmDelete(){
      conf = confirm("Anda Yakin Akan Menghapus Data ?");
      if (conf == true)
          return true;
      else
          return false;
  };


</script>
<!-- page script -->
</html>