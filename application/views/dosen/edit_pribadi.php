
<!-- <section class="content-header">
    <h1>
        Edit
        <small>Data Pribadi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Info</a></li>
        <li class="active">Data Diri</li>
    </ol>
</section> -->

<!-- Main content -->
<!-- <section class="content"> -->

    <!-- Default box -->
    <!-- <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Pribadi</h3>
            
        </div> -->
        <?php echo form_open_multipart('dosen/updatepribadi');?>
        <fieldset >
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px;">
              <label class="col-sm-3 control-label">No KTP  </label>
              <div class="col-sm-9">
                <input type="text" pattern="\d*" maxlength="20" name="no_ktp" class="form-control" value="<?php echo $dosen[0]['no_ktp']?>" placeholder="Masukkan Nomor KTP Anda">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px;">
              <label class="col-sm-3 control-label">Tempat Lahir  </label>
              <div class="col-sm-9">
                <input type="text" name="tmpt_lahir" maxlength="30" class="form-control" value="<?php echo $dosen[0]['tmpt_lahir']?>" pattern="^[a-zA-Z\s]+$" placeholder="Masukkan Tempat Lahir Anda">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px;">
              <label class="col-sm-3 control-label">Tanggal Lahir  </label>
              <div class="col-sm-5">
                <div class="input-group date" style="width: 73.5%">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right datepicker" name="tgl_lahir" value="<?php if ($dosen[0]['tgl_lahir'] != '0000-00-00') echo $dosen[0]['tgl_lahir']?>" max="<?php echo date('Y-m-d');?>" placeholder="Masukkan tanggal lahir Anda" >
                </div>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px;">
              <label class="col-sm-3 control-label">Alamat Rumah </label>
              <div class="col-sm-9">
                <textarea  class="form-control" rows="3" name="almt_rmh" maxlength="250" placeholder="Masukkan Alamat Rumah Anda"><?php echo $dosen[0]['almt_rmh']?></textarea>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px; padding-top: 40px;">
              <label class="col-sm-3 control-label">Nomor Handphone  </label>
              <div class="col-sm-9">
                <input type="text" pattern="\d*" maxlength="20" name="hp1" class="form-control" value="<?php echo $dosen[0]['hp1']?>" placeholder="Masukkan Nomor Handphone Anda">
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="form-group" style="padding-bottom: 10px;">
              <label class="col-sm-3 control-label">Nomor Handphone (Alternatif)   </label>
              <div class="col-sm-9">
                <input type="text" pattern="\d*" maxlength="20" name="hp2" class="form-control" value="<?php echo $dosen[0]['hp2']?>" placeholder="Masukkan Nomor Handphone Anda">
              </div>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <div class="form-group" style="padding-bottom: 25px;">
                <div  style="padding-right: 25px">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
                </div>
            </div>
          </div><!-- /. box-footer -->
        </fieldset>
        <?php echo form_close(); ?>    
        <!-- <br>       -->
    <!-- </div>/.box -->
<!-- </section>/.content -->
