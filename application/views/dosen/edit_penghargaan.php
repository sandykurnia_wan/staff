<form action ="<?php echo base_url('dosen/editPenghargaan')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-3 control-label">Nama Penghargaan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Pengabdian Anda" name="nama_penghargaan" id="nama_penghargaan" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tahun</label>
        <div class="col-sm-8">
            <input type="year" maxlength="4" pattern="[0-9]{4,4}" class="form-control yearpicker" placeholder="Masukkan Tahun Pelaksanaan Pengabdian Anda" name="tahun" id="tahun_penghargaan" required>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Pemberi Penghargaan</label>
        <div class="col-sm-8">
            <input type="text" class="form-control"  placeholder="Masukkan Nama Pemberi Penghargaan" name="pemberi" id="pemberi" required>
            <input type="hidden" class="form-control"  name="id_penghargaan" id="id_penghargaan" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Tingkat : </label>
        <div class="col-sm-8">
            <select class="form-control" name="tingkat" id="tingkat_penghargaan">
                <option value="Internal Universitas">Internal Universitas</option>
                <option value="Nasional">Nasional</option>
                <option value="Internasional">Internasional</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>         