<!-- <!DOCTYPE html>
<html>
<section class="content-header">
    <h1>
        Edit
        <small>Data Kantor</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Info</a></li>
        <li class="active">Data Diri</li>
    </ol>
</section> -->

<!-- Main content -->
<!-- <section class="content"> -->
        
    <!-- Default box -->
    <!-- <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Kantor</h3>
        </div> -->

        <?php echo form_open_multipart('dosen/updatekantor');?>
        <fieldset >
            <div class="box-body" >
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Nama  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Masukkan Nama Lengkap Anda" class="form-control" name="nm_dosen" value="<?php echo $dosen[0]['nm_dosen']?>" maxlength="60" placeholder="Masukkan Nama Anda">
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Gelar Depan  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal Dr. atau Prof. " maxlength="20" class="form-control" name="glr_dpn" value="<?php echo $dosen[0]['glr_dpn']?>" placeholder="Masukkan Gelar Depan Anda">
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
               <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Gelar Belakang  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal S. Kom. atau M. Sc." maxlength="20" class="form-control" name="glr_blkg" value="<?php echo $dosen[0]['glr_blkg']?>" placeholder="Masukkan Gelar Belakang Anda">
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
               <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Alamat Kantor  </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" data-toggle="tooltip" data-placement="bottom" title="Masukkan alamat kantor Anda dengan lengkap" rows="3" maxlength="250" name="almt_ktr" placeholder="Masukkan Alamat Kantor Anda"><?php echo $dosen[0]['almt_ktr']?></textarea>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px; padding-top: 40px;">
                    <label class="col-sm-3 control-label">No Telepon  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal 024 70594104" pattern="\d*" maxlength="20" name="no_tlp" value="<?php echo $dosen[0]['no_tlp']?>" class="form-control"  placeholder="Masukkan Nomor Telepon Anda">
                            <p class="help-block">Hanya boleh angka</p>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 55px;">
                    <label class="col-sm-3 control-label">Fax  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal 024 70594104" pattern="\d*" maxlength="20" name="fax" class="form-control" value="<?php echo $dosen[0]['fax']?>" placeholder="Masukkan Nomor Fax Anda">
                            <p class="help-block">Hanya boleh angka</p>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Email  </label>
                        <div class="col-sm-9">
                            <input type="email" data-toggle="tooltip" data-placement="bottom" title="Misal if@undip.ac.id" class="form-control" name="email1" value="<?php echo $dosen[0]['username']?>" disabled maxlength="50" placeholder="Masukkan Alamat Email Anda" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">Email (Alternatif)  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal if@undip.ac.id" class="form-control" name="email" value="<?php echo $dosen[0]['email']?>" maxlength="30" placeholder="Masukkan Alamat Email Anda" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">NIP  </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal 199903032000031001" pattern="\d*" maxlength="30" name="nip" class="form-control" value="<?php echo $dosen[0]['nip']?>" placeholder="Masukkan NIP Anda">
                            <p class="help-block">Hanya boleh angka</p>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">NIDN </label>
                        <div class="col-sm-9">
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal 0030069306" class="form-control" name="nidn" value="<?php echo $dosen[0]['nidn']?>" pattern="\d*" maxlength="30" placeholder="Masukkan NIDN Anda">
                            <p class="help-block">Hanya boleh angka</p>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">NPWP  </label>
                        <div class="col-sm-9">
                            <!-- <input type="text" class="form-control" id="tester"/> -->
                            <input type="text" data-toggle="tooltip" data-placement="bottom" title="Misal 99999999999" pattern="\d*" maxlength="20" class="form-control" name="npwp" value="<?php echo $dosen[0]['npwp']?>" >
                            <p class="help-block">Hanya boleh angka</p>
                        </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 10px;">
                    <label class="col-sm-3 control-label">No. Kartu Pegawai  </label>
                    <div class="col-sm-9">
                        <input type="text" data-toggle="tooltip" data-placement="bottom" title="Masukkan nomor kartu pegawai Anda" pattern="^[a-zA-Z0-9]*$" maxlength="20" class="form-control" name="no_krt_peg" value="<?php echo $dosen[0]['no_krt_peg']?>" >
                        <p class="help-block"></p>
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-body">
                <div class="form-group" style="padding-bottom: 25px;">
                    <label class="col-sm-3 control-label">Laboratory  </label>
                    <div class="col-sm-9">
                        <input type="text" data-toggle="tooltip" data-placement="bottom" title="Masukkan lab kategori Anda" maxlength="20" class="form-control" name="lab" value="<?php echo $dosen[0]['lab']?>" >
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer" style="padding-top: 90px">
                <div  style="padding-right: 25px">
                    <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
                    <button type="reset" class="btn btn-danger pull-right">Reset</button>
                </div>
            </div><!-- /. box-footer -->
        </fieldset>
        <?php echo form_close(); ?>   
    <!-- </div>/.box -->
<!-- </section>
</html> -->
<script type="text/javascript">
    jQuery(function($){
        $("#tester").mask({
            prefix: 'Rp ',
            thousandsSeparator: '.',
            centsLimit: 0
        });
    });
</script>