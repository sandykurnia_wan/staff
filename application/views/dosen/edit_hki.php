<form action ="<?php echo base_url('dosen/editHKI')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Judul HKI</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Judul HKI Anda" name="judul_hki" id="judul_hki" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-9">
            <input type="year" maxlength="4" pattern="[0-9]{4,4}" class="form-control yearpicker" placeholder="Masukkan Tahun Berlaku HKI Anda" name="tahun" id="tahun" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jenis HKI</label>
        <div class="col-sm-9">
            <select class="form-control" name="jenis_hki" id="jenis_hki">
                <option value="Paten">Paten</option>
                <option value="Paten Sederhana">Paten Sederhana</option>
                <option value="Hak Cipta">Hak Cipta</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nomor HKI</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="15" placeholder="Masukkan Nomor Dokumen HKI Anda" name="no_hki" id="no_hki" required>
            <input type="hidden" class="form-control"  name="id_hki" id="id_hki" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>         