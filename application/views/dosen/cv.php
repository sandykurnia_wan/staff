<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" </script>
    <script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>" </script>

    <title>Curicullum Vitae <?php //echo $name ?> </title>
</head>
<style type="text/css">
    body{
        height: 550px;
        width: 900px;
    }
    .image { 
        position: relative; 
        width: 100%; /* for IE 6 */
    }

    h2 { 
        width: 100%; 
        text-align: center;
        align-items: center;
    }

    h4.email { 
        position: absolute; 
        top: 430px; 
        left: -290px; 
        width: 100%; 
        text-align: center;
        align-items: left;
        color: white;
    }
    p.pernyataan { 
        text-align: justify;
        font-size: 12;
        overflow: auto;
    }
    
    h4.ttd{
        position: relative;
        width: 100%; 
        text-align: left;
        left: 600px;
        font-size: 12;
        overflow: auto;
    }

    .qr { 
        position:absolute; 
        top: 950px; 
        left: 415px; 
        z-index: 2; 
    } 
</style>
<body onload="printCV()" >
    <h2>CURICULLUM VITAE</h2>
    <div class="box-body">
        <br><br>
        <td><b>A. DATA PRIBADI</b></td>
        <hr style="width: 12%" align="left" >
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th hidden></th>
                <th hidden></th>
            </tr>
            <tr>
                <td><b>Nama Lengkap</b></td>
                <td>
                    <?php echo '&nbsp'.$pribadi[0]['glr_dpn'].' '.$pribadi[0]['nm_dosen'].', '.$pribadi[0]['glr_blkg']?>
                </td>
            </tr>
            <tr>
                <td><b>Jabatan Fungsional</b></td>
                <td><?php echo '&nbsp'.$fungsional[0]['jab_fung'] ?></td>
            </tr>
            <tr>
                <td><b>NIP</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['nip'] ?></td>
            </tr>
            <tr>
                <td><b>NIDN</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['nidn'] ?></td>
            </tr>
            <tr>
                <td><b>Tempat, Tanggal Lahir</b></td>
                <?php
                    setlocale (LC_ALL, 'IND');
                    $date=date_create($pribadi[0]['tgl_lahir']);
                    $bulan = strftime("%B", $date->getTimestamp());
                    $tanggal = strftime("%d", $date->getTimestamp());
                    $tahun = strftime("%Y", $date->getTimestamp());                                
                ?>
                <td><?php echo '&nbsp'.$pribadi[0]['tmpt_lahir'] ?>, <?php echo $tanggal.' '.$bulan.' '.$tahun;?> </td>
            </tr>
            <tr>
                <td><b>Email</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['email'] ?></td>
            </tr>
            <tr>
                <td><b>No Telepon / HP</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['hp1'] ?></td>
            </tr>
            <tr>
                <td><b>Alamat Kantor</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['almt_ktr'] ?></td>
            </tr>
            <tr>
                <td><b>No Telpon / Fax</b></td>
                <td><?php echo '&nbsp'.$pribadi[0]['no_tlp'].' / '.$pribadi[0]['fax'] ?></td>
            </tr>
        <br>
        </table>
        <br><br>
        <td><b>B. RIWAYAT PENDIDIKAN</b></td>
        <hr style="width: 18%" align="left">
            <th><b>Pendidikan Formal</b></th>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center;"></th>
                <th style="text-align: center;">S-1</th>
                <th style="text-align: center;">S-2</th>
                <th style="text-align: center;">S-3</th>
            </tr>
            <tr>
                <th>Nama Perguruan Tinggi</th>
                <td><?php if(!empty($formal[0])) {?>
                    <?php echo $formal[0]['nama_univ']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[1])) {?>
                    <?php echo $formal[1]['nama_univ']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[2])) {?>
                    <?php echo $formal[2]['nama_univ']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
            </tr>
            <tr>
                <th>Bidang Ilmu</th>
                <td><?php if(!empty($formal[0])) {?>
                    <?php echo $formal[0]['bidang']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[1])) {?>
                    <?php echo $formal[1]['bidang']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[2])) {?>
                    <?php echo $formal[2]['bidang']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
            </tr>
            <tr>
                <th>Tahun Masuk - Tahun Lulus</th>
                <td><?php if(!empty($formal[0])) {?>
                    <?php echo $formal[0]['thn_masuk'].' - '.$formal[0]['thn_lulus']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[1])) {?>
                    <?php echo $formal[1]['thn_masuk'].' - '.$formal[1]['thn_lulus']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[2])) {?>
                    <?php echo $formal[0]['thn_masuk'].' - '.$formal[0]['thn_lulus']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
            </tr>
            <tr>
                <th>Judul Tugas Akhir</th>
                <td><?php if(!empty($formal[0])) {?>
                    <?php echo $formal[0]['judul_ta']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[1])) {?>
                    <?php echo $formal[1]['judul_ta']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
                <td><?php if(!empty($formal[2])) {?>
                    <?php echo $formal[2]['judul_ta']?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
            </tr>
            <tr>
                <th>Dosen Pembimbing</th>
                <td><?php if(!empty($formal[0])) {?>
                    <?php echo $formal[0]['dosbing1']?>
                        <?php if(!empty($formal[0]['dosbing2'])) {?>
                            <?php echo ', '.$formal[0]['dosbing2']?>
                        <?php } ?>
                    <?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </th>
                <td><?php if(!empty($formal[1])) {?>
                    <?php echo $formal[1]['dosbing1']?>
                        <?php if(!empty($formal[1]['dosbing2'])) {?>
                            <?php echo ', '.$formal[1]['dosbing2']?>
                        <?php } ?><?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </th>
                <td><?php if(!empty($formal[2])) {?>
                    <?php echo $formal[2]['dosbing1']?>
                        <?php if(!empty($formal[2]['dosbing2'])) {?>
                            <?php echo ', '.$formal[2]['dosbing2']?>
                        <?php } ?><?php } else {?>
                    (Data Belum Diinputkan)
                    <?php }?>    
                </td>
            </tr>
            <br>
        </table>
        <br>
        <th><b>Pendidikan Non Formal</b></th>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center;">Nama Kegiatan</th>
                <th style="text-align: center;">Penyelenggara</th>
                <th style="text-align: center;">Tanggal</th>
                <th style="text-align: center;">Tempat</th>
            </tr>
            <tr>
                <?php if(!empty($non_formal)) {?>
                <?php foreach($non_formal as $non){ ?>
                    <tr>
                        <td><?php echo $non['nama_kegiatan'] ?></td>
                        <td><?php echo $non['penyelenggara'] ?></td>
                        <?php
                            setlocale (LC_ALL, 'IND');
                            $date=date_create($non_formal[0]['tgl_kegiatan']);
                            $bulan = strftime("%B", $date->getTimestamp());
                            $tanggal = strftime("%d", $date->getTimestamp());
                            $tahun = strftime("%Y", $date->getTimestamp());                                
                        ?>
                        <td><?php echo $tanggal.' '.$bulan.' '.$tahun;?> </td>
                        <td><?php echo $non['tempat'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <br><br>
        <td><b>C. PENGALAMAN PENELITIAN 5 TAHUN TERAKHIR</b></td>
        <td>(Bukan Skripsi, Tesis, maupun Disertasi)</td>
        <hr style="width: 62%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th rowspan="2" style="text-align: center">Tahun</th>
                <th rowspan="2" style="text-align: center">Judul Penelitian</th>
                <th colspan="2" style="text-align: center">Pendanaan</th>
            </tr>
            <tr>
                <td style="text-align: center">Sumber Dana</td>
                <td style="text-align: center">Jumlah Dana</td>
            </tr>
            <tr>
                <?php if(!empty($penelitian)) {?>    
                <?php foreach($penelitian as $pl){ ?>
                    <tr>
                        <td><?php echo $pl['tahun'] ?></td>
                        <td><?php echo $pl['judul_penelitian'] ?></td>
                        <td><?php echo $pl['sumber_dana'] ?></td>
                        <td><?php echo 'Rp. '.$pl['jumlah_dana'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <br><br>
        <td><b>D. PENGALAMAN PENGABDIAN PADA MASYARAKAT 5 TAHUN TERAKHIR</b></td>
        <hr style="width: 50%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th rowspan="2" style="text-align: center">Tahun</th>
                <th rowspan="2" style="text-align: center">Judul Pengabdian</th>
                <th colspan="2" style="text-align: center">Pendanaan</th>
            </tr>
            <tr>
                <td style="text-align: center">Sumber Dana</td>
                <td style="text-align: center">Jumlah Dana</td>
            </tr>
            <tr>
                <?php if(!empty($pengabdian)) {?>
                <?php foreach($pengabdian as $pd){ ?>
                    <tr>
                        <td><?php echo $pd['tahun'] ?></td>
                        <td><?php echo $pd['judul_pengabdian'] ?></td>
                        <td><?php echo $pd['sumber_dana'] ?></td>
                        <td><?php echo 'Rp. '.$pd['jumlah_dana'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <br><br>
        <td><b>E. KARYA BUKU DALAM 5 TAHUN TERAKHIR</b></td>
        <hr style="width: 31%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Judul Buku</th>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Jumlah Halaman</th>
                <th style="text-align: center">Penerbit</th>
            </tr>
            <tr>
                <?php if(!empty($buku)) {?>
                <?php foreach($buku as $b){ ?>
                    <tr>
                        <td><?php echo $b['judul_buku'] ?></td>
                        <td><?php echo $b['tahun_terbit'] ?></td>
                        <td><?php echo $b['jml_hlmn'] ?></td>
                        <td><?php echo $b['penerbit'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <br><br>
        <td><b>F. PEROLEHAN HKI DALAM 5-10 TAHUN TERAKHIR</b></td>
        <hr style="width: 35%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Judul/ Tema HKI</th>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Jenis</th>
                <th style="text-align: center">Nomor P/ID</th>
            </tr>
            <tr>
                <?php if(!empty($hki)) {?>
                <?php foreach($hki as $h){ ?>
                    <tr>
                        <td><?php echo $h['judul_hki'] ?></td>
                        <td><?php echo $h['tahun'] ?></td>
                        <td><?php echo $h['jenis_hki'] ?></td>
                        <td><?php echo $h['no_hki'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <br><br>
        <td><b>G. PENGHARGAAN DALAM 10 TAHUN TERAKHIR</b></td>
        <hr style="width: 34%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Nama Penghargaan</th>
                <th style="text-align: center">Institusi Pemberi Penghargaan</th>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Tingkat</th>
            </tr>
            <tr>
                <?php if(!empty($penghargaan)) {?>
                <?php foreach($penghargaan as $ph){ ?>
                    <tr>
                        <td><?php echo $ph['nama_penghargaan'] ?></td>
                        <td><?php echo $ph['pemberi'] ?></td>
                        <td><?php echo $ph['tahun'] ?></td>
                        <td><?php echo $ph['tingkat'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <?php $huruf = array("H", "I", "J", "K");$start = 0; ?>
        <?php if(in_array(1, $tambahan)){ ?>
        <br><br>
        <td><b><?= $huruf[$start];$start++; ?>. ORGANISASI PROFESI / ILMIAH</b></td>
        <hr style="width: 24%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Jenis / Nama Organisasi</th>
                <th style="text-align: center">Jabatan</th>
            </tr>
            <tr>
                <?php if(!empty($org)) {?>
                <?php foreach($org as $o){ ?>
                    <tr>
                        <td><?php echo $o['tahun'] ?></td>
                        <td><?php echo $o['judul'] ?></td>
                        <td><?php echo $o['jabatan'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="3" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <?php } ?>
        <?php if(in_array(2, $tambahan)){ ?>
        <br><br>
        <td><b><?= $huruf[$start];$start++; ?>. PENGALAMAN SEBAGAI REVIEWER DALAM 5-10 TAHUN TERAKHIR</b></td>
        <hr style="width: 48%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Bidang</th>
                <th style="text-align: center">Penyelenggara</th>
                <th style="text-align: center">Tingkat</th>
            </tr>
            <tr>
                <?php if(!empty($review)) {?>
                <?php foreach($review as $r){ ?>
                    <tr>
                        <td><?php echo date("Y", strtotime($review[0]['tanggal'])) ?></td>
                        <td><?php echo $r['judul'] ?></td>
                        <td><?php echo $r['penyelenggara'] ?></td>
                        <td><?php echo $r['tingkat'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <?php } ?>
        <?php if(in_array(3, $tambahan)){ ?>
        <br><br>
        <td><b><?= $huruf[$start];$start++; ?>. PENGALAMAN SEBAGAI NARASUMBER DALAM 5-10 TAHUN TERAKHIR</b></td>
        <hr style="width: 50%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Tahun</th>
                <th style="text-align: center">Judul / Tema</th>
                <th style="text-align: center">Penyelenggara</th>
                <th style="text-align: center">Tingkat</th>
            </tr>
            <tr>
                <?php if(!empty($narasumber)) {?>
                <?php foreach($narasumber as $n){ ?>
                    <tr>
                        <td><?php echo date("Y", strtotime($narasumber[0]['tanggal'])) ?></td>
                        <td><?php echo $n['judul'] ?></td>
                        <td><?php echo $n['penyelenggara'] ?></td>
                        <td><?php echo $n['tingkat'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <?php } ?>
        <?php if(in_array(4, $tambahan)){ ?>
        <br><br>
        <td><b><?= $huruf[$start];$start++; ?>. PENGALAMAN KEPEMIMPINAN</b></td>
        <hr style="width: 23%" align="left"><br>
        <table class="table table-bordered table-striped" border="1">
            <tr>
                <th style="text-align: center">Tingkat</th>
                <th style="text-align: center">Jabatan</th>
                <th style="text-align: center">Periode</th>
            </tr>
            <tr>
                <?php if(!empty($leader)) {?>
                <?php foreach($leader as $l){ ?>
                    <tr>
                        <td><?php echo $l['tingkat'] ?></td>
                        <td><?php echo $l['jabatan'] ?></td>
                        <td><?php echo $l['periode'] ?></td>
                    </tr>
                <?php }?>
                <?php } else {?>
                    <tr>
                        <td colspan="3" style="text-align: center"><?php echo "(Data Belum Diinputkan)" ?></td>
                    </tr>
                <?php }?>
            </tr>
        </table>
        <?php } ?>
        <br><br>
        <br><br><br>
        <p>Semua data yang  saya  isikan dan tercantum  dalam biodata ini adalah benar dan dapat dipertanggungjawabkan secara  hukum. Apabila  dikemudian</p>
        <p>hari ternyata dijumpai ketidaksesuaian dengan kenyataan, saya sanggup menerima sanksi.</p>
        <br><br><br>
        <h4 class="ttd">
            <?php
                setlocale (LC_ALL, 'IND');
                $date=date_create($today = date("y-m-d"));
                $bulan = strftime("%B", $date->getTimestamp());
                $tanggal = strftime("%d", $date->getTimestamp());
                $tahun = strftime("%Y", $date->getTimestamp());                                
            ?>
            <?php echo 'Semarang, '?> <?php echo $tanggal.' '.$bulan.' '.$tahun;?><br>
            Dosen,
            <br><br><br><br><br><br>
            <?php echo '&nbsp'.$pribadi[0]['glr_dpn'].' '.$pribadi[0]['nm_dosen'].', '.$pribadi[0]['glr_blkg']?>
        </h4>
    </div>
    <br/>
    <!-- Script print -->
    <script>
        function printCV() {
            window.print();
        }
    </script>

</body>
</html>