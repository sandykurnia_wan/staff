<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Kelola
        <small>Akun</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kelola Akun</li>
    </ol>
</section>
<?php
    $id = $this->session->userdata('username');
            $ppdata = $this->mdosen->getProfpic($id);
            if(empty($ppdata)){
                $profpic = null;
            }else{
                $profpic = $ppdata[0]['filename'];
            }
            
 ?>
<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Ubah Password</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_open_multipart('dosen/firstUpdate', array('onsubmit' => 'return validatePass()'));?>

            <fieldset>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3 control-label">Password baru  </label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control"  placeholder="Masukkan password baru Anda" name="password" pattern='[a-zA-Z0-9/S]{8,}$' id='pass1' required data-toggle="tooltip" data-placement="bottom" title="Panjang password minimal 8 karakter">
                        </div>
                    </div>
                    
                </div>
                <div class="form-group">
                    <div class="row">
                        <label class="col-sm-3 control-label">Konfirmasi password baru </label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control"  placeholder="Masukkan password lagi" name="confirm_password" pattern='[a-zA-Z0-9/S]{8,}$' id='pass2' required data-toggle="tooltip" data-placement="bottom" title="Masukkan password baru Anda lagi">
                        </div>
                    </div>
                </div>
                <?php 
                    if (empty($profpic)){
                ?>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="filename" class="control-label">Pilih gambar untuk diunggah</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <input id="uploadImage" type="file" name="filename" accept=".jpg,.jpeg,.png,.gif" onchange="PreviewImage();" /><br>
                            <img id="uploadPreview" width="250" />
                            <span class="text-danger"><?php if (isset($error)) { echo $error; } ?></span>
                        </div>
                    </div>
                </div>
                <?php
                    }else{
                ?>

                <?php
                    }
                ?> 

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Simpan" class="btn btn-primary"/>
                        </div>
                    </div>
                </div>
            </fieldset>
            
            <?php echo form_close(); ?>
            <?php if (isset($success_msg)) { echo $success_msg; } ?>
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->
<script type="text/javascript">
    function validatePass() {
        var pass1 = document.getElementById("pass1").value;
        var pass2 = document.getElementById("pass2").value;
        var lgth = document.getElementById("pass1").value.length;
        if (lgth <8){
            alert("Panjang password kurang dari 8 karakter!");
            document.getElementById("pass1").style.borderColor = "#E34234";
            //document.getElementById("pass2").style.borderColor = "#E34234";
            return false;
        } else if (pass1 != pass2) {
            //alert("Passwords Do not match");
            alert("Passwords Tidak Sama!");
            //document.getElementById("pass1").style.borderColor = "#E34234";
            document.getElementById("pass2").style.borderColor = "#E34234";
            return false;
        }
        
    }
</script>
<script type="text/javascript">
    function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
    oFReader.onload = function (oFREvent)
        {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>
