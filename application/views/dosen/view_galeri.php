<style type="text/css">

.thumbnails img {
	height: 80px;
	border: 4px solid #555;
	padding: 1px;
	margin: 0 10px 10px 0;
}

.thumbnails img:hover {
	border: 4px solid #00ccff;
	cursor:pointer;
}

.preview img {
	border: 4px solid #444;
	padding: 1px;
	width: 200;
	height: 200px;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Gallery Page
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Activity</a></li>
        <li class="active">Gallery</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Title</h3>
        </div>
        <div class="box-body">
            <div class="gallery" align="center">
				<h2>Simple Photo Gallery with HTML and JavaScript</h2>
				<p>Created by <a target="_blank" href="http://html-tuts.com/">HTML-TUTS.com</a>. View full tutorial <a href="http://html-tuts.com/?p=2337" target="_blank">here</a>.</p>
				<br />
				<div class="preview" align="center">
					<img name="preview" src="images/img1.jpg" alt=""/>
				</div><br/>
				<div class="thumbnails">
					<?php 
						$i=1;
						for($i=1; $i<=20;$i++){ 
							if ($i%2 == 0) { ?>
								<img onmouseover="<?php echo ('preview.src=img'.$i.'.src'); ?>" name="<?php echo('img'.$i)?>" src="<?php echo base_url('uploads/galeri/mhs1.png') ?>" alt=""/>
							<?php  } else { ?>
								<img onmouseover="<?php echo ('preview.src=img'.$i.'.src'); ?>" name="<?php echo('img'.$i)?>" src="<?php echo base_url('uploads/galeri/limbah1.jpg') ?>" alt=""/>
							<?php 
							}?>
							
					<?php
						}

					?>
					
				</div><br/>
				
			</div>
        </div><!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->
