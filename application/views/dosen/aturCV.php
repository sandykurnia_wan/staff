<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
    <!-- DataTables -->
    <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" </script>
    <script src="<?php echo base_url('plugins/datatables/dataTables.bootstrap.min.js') ?>" </script>
</head>
<section class="content-header">
    <h1>
        Pengaturan CV
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Curicullum Vitae</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Trigger the modal with a button -->
    <?php echo $this->session->flashdata('pesan') ?>
    <br/>
    
  <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Curicullum Vitae</h3>
        </div>
        <div class="row">
            <div class="box-content">
                <div class="col-md-5">
                    <h5>&nbsp&nbspApakah anda ingin mencetak CV Default yang dihasilkan oleh sistem?</h5>
                </div>
                <br><br>
                <div class="col-md-1">
                    <div class="col-md-2">
                        <a type="button" href="<?php echo base_url('dosen/printCV');?>" class="btn btn-primary btn-sm pull-left" style="width: 60px;" data-toggle="modal" target="_blank" >
                            <i class="fa fa-fw fa-check"></i> 
                            Ya
                        </a>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="col-md-2">
                        <button type="button" class="btn btn-danger btn-sm pull-left" style="width: 80px;" data-toggle="modal" data-target="#tambahanCV" target="_blank">
                            <i class="fa fa-fw fa-close"></i> 
                            Tidak
                        </button>
                      </div>                
                    </div>
                </div>
                <br><br>
            </div>
        </div>          
    </div><!-- /.box -->
</section><!-- /.content -->

<div class="modal fade" id="tambahanCV" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambahan Konten CV</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/tambahanCV'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
<script type="text/javascript">
  function confirmDelete(){
      conf = confirm("Anda Yakin Akan Menghapus Data ?");
      if (conf == true)
          return true;
      else
          return false;
  };


</script>
<!-- page script -->
</html>