<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript">
    jQuery(function($){
        $("#add_jumlah_penelitian").priceFormat({
            prefix: 'Rp ',
            thousandsSeparator: '.',
            centsLimit: 0
        });
    });
</script>
</head>
<body>

<form action ="<?php echo base_url('dosen/addPenelitian')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Judul Penelitian</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Penelitian Anda" name="judul_penelitian" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-9">
            <input type="year" maxlength="4" pattern="[0-9]{4,4}" class="form-control yearpicker" placeholder="Masukkan Tahun Penelitian Anda" name="tahun" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Sumber Dana</label>
        <div class="col-sm-9">
            <select class="form-control" name="sumber_dana">
                <option value="DIKTI">DIKTI</option>
                <option value="Internal Undip">Internal Undip</option>
                <option value="Internal Fakultas">Internal Fakultas</option>
                <option value="Kementrian Pertanian">Kementrian Pertanian</option>
                <option value="LPDP">LPDP</option>
                <option value="Lain-lain">Lain-lain</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jumlah Dana</label>
        <div class="col-sm-9">
            <input type="text" maxlength="14" class="form-control"  placeholder="Masukkan Jumlah Dana Penelitian Anda" name="jumlah_dana" id="add_jumlah_penelitian" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Status dalam Penelitian</label>
        <div class="col-sm-9">
            <select class="form-control" name="status" required>
                <option value="Ketua">Ketua Penelitian</option>
                <option value="Anggota">Anggota Penelitian</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>
</body>
</html>         