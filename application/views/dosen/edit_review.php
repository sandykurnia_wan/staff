<?php echo form_open_multipart('dosen/editreview');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Judul Kegiatan  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Kegiatan Anda" maxlength="100" name="judul" id="judulrev" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Tanggal  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Kegiatan Anda" name="tanggal" id="tanggal" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Penyelenggara  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Nama Penyelenggara Reviewer" name="penyelenggara" id="penyelenggara" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Tingkat  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Tingkat Kegiatan Reviewer Anda" name="tingkat" id="tingkat" required>
            <input type="hidden" class="form-control"  name="idrev" id="idrev" >
        </div>
    </div>
    <div class="form-group">
        <div  style="padding-right: 25px">
            <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>          