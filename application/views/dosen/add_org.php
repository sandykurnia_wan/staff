<?php echo form_open_multipart('dosen/addorg');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-lg-3 control-label">Judul/ Nama Organisasi</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Judul/ Nama Organisasi Anda" name="judul" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-lg-3 control-label">Tahun</label>
        <div class="col-lg-9">
            <input type="text" class="form-control yearpicker"  placeholder="Masukkan Tahun Anda Berorganisasi" name="tahun" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-lg-3 control-label">Jabatan</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Jabatan Anda" name="jabatan" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-lg-3 control-label">No Keanggotaan</label>
        <div class="col-lg-9">
            <input type="text" class="form-control" maxlength="30" placeholder="Masukkan Nomor Keanggotaan Anda" name="no_anggota" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div  style="padding-right: 25px">
            <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>      

<script type="text/javascript">
  function previewImages() {

  var $preview = $('#preview').empty();
  if (this.files) $.each(this.files, readAndPreview);

  function readAndPreview(i, file) {
        
        if (!/\.(jpe?g|png|gif)$/i.test(file.name)){
          return alert(file.name +" is not an image");
        } // else...
        
        var reader = new FileReader();

        $(reader).on("load", function() {
          $preview.append($("<img/>", {src:this.result, height:100}));
        });

        reader.readAsDataURL(file);
        
      }

    }

    $('#file-input').on("change", previewImages);
</script>