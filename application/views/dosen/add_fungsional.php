<?php echo form_open_multipart('dosen/addfung');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Jabatan </label>
        <div class="col-sm-9">
            <select class="form-control" id="addjabatan" required name="addjabatan" onchange="addFung();">
                <option value= >--- Pilih Jabatan Fungsional Anda ---</option>
                <option value="1">Staff Pengajar</option>
                <option value="2">Asisten Ahli</option>
                <option value="3">Lektor</option>
                <option value="4">Lektor Kepala</option>
                <option value="5">Guru Besar</option>
            </select>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Terhitung Mulai Tanggal  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control datepicker" name="awal" required>
            <input type="hidden" id="jabfung" name="jabatan">
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div class="col-sm-11">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>
<script type="text/javascript">
    var jab = new Array();

    jab[0] = "";                jab[3] = 'Lektor';
    jab[1] = 'Staff Pengajar';  jab[4] = 'Lektor Kepala';
    jab[2] = 'Asisten Ahli';    jab[5] = 'Guru Besar';

    function addFung(){
        y = document.getElementById("addjabatan");
        document.getElementById("jabfung").value = jab[y.selectedIndex];
    };

</script>