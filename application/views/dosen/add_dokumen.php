<?php echo form_open_multipart('dosen/addDokumen');?>
<fieldset>
    <div class="form-group">
        <label class="col-sm-3 control-label">Dokumen : </label>
        <div class="col-sm-9">
            <input type="file" name="docs" accept=".pdf">
            <input type="hidden" name="idAct" id="idAct">
            <input type="hidden" name="jenisAct" id="jenisAct">
        </div>
        <br>
        <div class="col-sm-9">
            <td>Ukuran File Maksimal 2.000 KB</td>
        </div>
    </div><br><br>
    <div class="form-group">
        <div class="col-sm-11">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>      

