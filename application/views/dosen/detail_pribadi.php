<section class="content-header">
    <h1>
        Info
        <small>Data Diri</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Site_url('dosen');?>"><i class="fa fa-dashboard"></i> Staff</a></li>
        <li class="active">Data Pribadi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
<?php echo $this->session->flashdata('pesan') ?>
  <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Diri</h3>
        </div>
        <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#pribadi" data-toggle="tab">Data Pribadi</a></li>
                    <li><a href="#kantor" data-toggle="tab">Data Kantor</a></li>
                    <li><a href="#lain" data-toggle="tab">Data Lain</a></li>
                    <li><a href="#jabatan" data-toggle="tab">Riwayat Jabatan</a></li>
                  </ul>
                  
                  <div class="tab-content">
                    
                    <div class="tab-pane active" id="pribadi">
                      <div class="box-body">
                        <div class="col-lg-11">
                          <div class="form-group">
                            <table class="table table-hover">
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">No KTP </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['no_ktp']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Tempat Lahir </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['tmpt_lahir']?></label></td>
                              </tr>
                              <tr>
                                <?php 
                                  setlocale (LC_ALL, 'IND');
                                  $date=date_create($dosen[0]['tgl_lahir']);
                                  $bulan = strftime("%B", $date->getTimestamp());
                                  $tanggal = strftime("%d", $date->getTimestamp());
                                  $tahun = strftime("%Y", $date->getTimestamp());
                                ?>
                                <td class="col-lg-3 "><label class="control-label">Tanggal Lahir </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php 
                                  if ($dosen[0]['tgl_lahir'] == '0000-00-00') {
                                      echo '';
                                  }else{
                                      echo $tanggal.' '.$bulan.' '.$tahun;
                                  }?> </label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Alamat Rumah </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['almt_rmh']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Nomor Handphone </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['hp1']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Nomor Handphone (Alternatif) </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['hp2']?></label></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <a class="btn btn-primary" data-toggle="modal" data-target="#editpribadi"><i class="fa fa-edit"></i>&nbsp;&nbsp; Ubah</a>
                        </div>
                      </div>
                      <form class="form-horizontal">
                        <div class="box-body">
                          <div class="form-group">  
                            
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->
                    
                    <div class="tab-pane" id="kantor">
                      <div class="box-body">
                        <div class="col-lg-11">
                          <div class="form-group">
                            <table class="table table-hover">
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Nama </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['nm_dosen']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Gelar Depan </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['glr_dpn']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Gelar Belakang </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['glr_blkg']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Alamat Kantor </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['almt_ktr']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Nomor Telepon </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['no_tlp']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Fax </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['fax']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Email </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['username']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Email Alternatif </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['email']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">NIP </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['nip']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">NIDN </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['nidn']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">NPWP </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['npwp']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Nomor Kartu Pegawai </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['no_krt_peg']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Laboratorium </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['lab']?></label></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <a class="btn btn-primary" data-toggle="modal" data-target="#editkantor"><i class="fa fa-edit"></i>&nbsp;&nbsp; Ubah</a>
                        </div>
                         
                      </div>
                      <form class="form-horizontal">    
                          <div class="box-body">
                            <div class="form-group">  
                            </div>
                          </div><!-- /. box-footer -->
                        </form>
                    </div>
                      <!-- /.tab-pane -->
                    
                    <div class="tab-pane" id="lain">
                      <div class="box-body">
                        <div class="col-lg-11">
                          <div class="form-group">
                            <table class="table table-hover">
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">ID Scopus </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['scopus_id']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">ID Research Gate </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['rgate']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Link Profil Google Scholar </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['scholar']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Link Profil Sinta </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['sinta']?></label></td>
                              </tr>
                              <tr>
                                <td class="col-lg-3 "><label class="control-label">Interest Subject </label></td>
                                <td>:</td>
                                <td class="col-lg-10"><label class="control-label"><?php echo $dosen[0]['interest']?></label></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <a class="btn btn-primary" data-toggle="modal" data-target="#editlain"><i class="fa fa-edit"></i>&nbsp;&nbsp; Ubah</a>
                        </div>
                      </div>
                      <form class="form-horizontal">
                        <div class="box-body">
                          <div class="form-group"> 
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="jabatan">                      
                      <div class="box-body">
                        <div class="col-lg-12">
                          Tabel ini berisi tabel riwayat jabatan yang pernah dijabat.
                        </div> 
                        <div class="col-lg-12"><br></div>
                        <div class="col-lg-12" style="margin: 15px">
                          <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#gol">
                          <i class="fa fa-fw fa-plus"></i> Pangkat Golongan </a>
                          <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#fung">
                          <i class="fa fa-fw fa-plus"></i> Jabatan Fungsional </a>
                          <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#struk">
                          <i class="fa fa-fw fa-plus"></i> Jabatan Struktural </a>
                        </div>
                        <div class="col-lg-12"><br></div>
                        <div class="col-lg-12" style="margin-top: 0px">
                        <!-- Custom Tabs -->
                          <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                              <li class="active"><a href="#tab_1" data-toggle="tab">Pangkat</a></li>
                              <li><a href="#tab_2" data-toggle="tab">Jabatan Fungsional</a></li>
                              <li><a href="#tab_3" data-toggle="tab">Jabatan Struktural</a></li>
                            </ul>
                            <div class="tab-content">
                              <!-- Pangkat -->
                              <div class="tab-pane active" id="tab_1">
                                  <div class="box-body">
                                      <table <?php if(!empty($pangkat)) echo 'id="example1"' ?> class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                          <th class="text-center">Jenis</th>
                                          <th class="text-center">Gol/Ruang</th>
                                          <th class="text-center">TMT</th>
                                          <th class="text-center">Kontrol</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                            if (empty($pangkat)) {
                                        ?>
                                            <tr>
                                              <td colspan="4" style="text-align: center">Data belum ada</td>
                                            </tr>
                                        <?php
                                            }else{
                                              foreach ($pangkat as $p) {
                                        ?>
                                            <tr>
                                                <?php 
                                                  setlocale (LC_ALL, 'IND');
                                                  $date=date_create($p['tmt']);
                                                  $bulan = strftime("%B", $date->getTimestamp());
                                                  $tanggal = strftime("%d", $date->getTimestamp());
                                                  $tahun = strftime("%Y", $date->getTimestamp());
                                                ?>
                                                <td><?php echo $p['jenis']?></td>
                                                <td align="center"><?php echo $p['gol_ruang']?></td>
                                                <td align="center"><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                <td align="center"><a class="btn btn-sm btn-primary pangkatdata" data-toggle="modal" data-idpgkt="<?php echo $p['id_pangkat']?>" data-jenispgkt="<?php echo $p['jenis']?>" data-golpgkt="<?php echo $p['gol_ruang']?>" data-tmtpgkt="<?php echo $p['tmt']?>"  data-target="#ModalPangkat"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a></td>

                                            </tr>
                                        <?php
                                              }
                                            }
                                        ?>
                                        </tbody>
                                      </table>
                                  </div>
                                  <!-- /.box-body -->
                              </div>
                              <!-- Jabatan Fungsional -->
                              <div class="tab-pane" id="tab_2">
                                  <div class="box-body">
                                    <table <?php if(!empty($fungsional)) echo 'id="example2"' ?> class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                        <th class="text-center">Jabatan Fungsional</th>
                                        <th class="text-center">Terhitung Mulai Tanggal</th>
                                        <th class="text-center">Kontrol</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                            if (empty($fungsional)) {
                                        ?>
                                            <tr>
                                              <td colspan="3" style="text-align: center">Data belum ada</td>
                                            </tr>
                                        <?php
                                            }else{
                                              foreach ($fungsional as $f) {
                                        ?>
                                            <tr>
                                                <?php 
                                                  setlocale (LC_ALL, 'IND');
                                                  $date=date_create($f['awal_fung']);
                                                  $bulan = strftime("%B", $date->getTimestamp());
                                                  $tanggal = strftime("%d", $date->getTimestamp());
                                                  $tahun = strftime("%Y", $date->getTimestamp());
                                                ?>
                                                <td ><?php echo $f['jab_fung']?></td>
                                                <td align="center"><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                <td align="center"><a class="btn btn-sm btn-primary fungsionaldata" data-toggle="modal" data-idfung="<?php echo $f['id_fung']?>" data-jabfung="<?php echo $f['jab_fung']?>" data-awalfung="<?php echo $f['awal_fung']?>" data-target="#ModalFungsional"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a></td>
                                            </tr>
                                        <?php
                                            }
                                          }
                                        ?>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!-- /.box-body -->
                              </div>
                              <!-- Jabatan Struktural -->
                              <div class="tab-pane" id="tab_3">
                                  <div class="box-body">
                                    <table <?php if(!empty($struktural)) echo 'id="example3"' ?> class="table table-bordered table-striped">
                                      <thead>
                                      <tr>
                                        <th class="text-center">Jabatan Struktural</th>
                                        <th class="text-center">Terhitung Mulai Tanggal</th>
                                        <th class="text-center">Selesai</th>
                                        <th class="text-center">Kontrol</th>
                                      </tr>
                                      </thead>
                                      <tbody>
                                        <?php
                                            if (empty($struktural)) {
                                        ?>
                                            <tr>
                                              <td colspan="4" style="text-align: center">Data belum ada</td>
                                            </tr>
                                        <?php
                                            }else{
                                              foreach ($struktural as $s) {
                                        ?>
                                            <tr>

                                                <td><?php echo $s['jab_struk']?> <?php echo $s['keterangan']?></td>
                                                <?php 
                                                  setlocale (LC_ALL, 'IND');
                                                  $date=date_create($s['awal_struk']);
                                                  $bulan = strftime("%B", $date->getTimestamp());
                                                  $tanggal = strftime("%d", $date->getTimestamp());
                                                  $tahun = strftime("%Y", $date->getTimestamp());
                                                ?>
                                                <td align="center"><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                <?php 
                                                  setlocale (LC_ALL, 'IND');
                                                  $date=date_create($s['akhir_struk']);
                                                  $bulan = strftime("%B", $date->getTimestamp());
                                                  $tanggal = strftime("%d", $date->getTimestamp());
                                                  $tahun = strftime("%Y", $date->getTimestamp());
                                                ?>
                                                <td align="center"><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                <td align="center"><a class="btn btn-sm btn-primary strukturaldata" data-toggle="modal" data-idstruk="<?php echo $s['id_struk']?>" data-jabstruk="<?php echo $s['jab_struk']?>" data-ketstruk="<?php echo $s['keterangan']?>" data-awalstruk="<?php echo $s['awal_struk']?>" data-akhirstruk="<?php echo $s['akhir_struk']?>" data-target="#ModalStruktural"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a></td>
                                            </tr>
                                        <?php
                                            }
                                          }
                                        ?>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!-- /.box-body -->
                              </div>
                            </div><!-- /.box -->
                            <!-- /.tab-content -->
                          </div>
                          <!-- nav-tabs-custom -->
                        </div>
                      </div>
                      <form class="form-horizontal">
                        <div class="box-body">
                          <div class="form-group"> 
                          </div>
                        </div>
                      </form>
                    </div>
                    <!-- /.tab-pane -->

                  </div>
                  <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
          </div>          
    </div><!-- /.box -->
</section><!-- /.content -->
    <!-- Modal edit data pribadi -->
    <div class="modal fade" id="editpribadi" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Data Pribadi</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/edit_pribadi'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal edit data kantor -->
    <div class="modal fade" id="editkantor" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Data Kantor</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/edit_kantor'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal edit data lain -->
    <div class="modal fade" id="editlain" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Data Lain</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/edit_lain'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Organisasi -->
    <div class="modal fade" id="gol" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Riwayat Pangkat</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_pangkat'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Buku -->
    <div class="modal fade" id="struk" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Riwayat Jabatan Struktural</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_struktural'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Review -->
    <div class="modal fade" id="fung" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Riwayat Jabatan Fungsional</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_fungsional'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Edit Pangkat -->
    <!-- Modal Button Edit-->
    <div class="modal fade" id="ModalPangkat" role="dialog">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Data Pangkat</h4>
                  </div>
                  <div class="modal-body">
                    <?php $this->load->view('dosen/edit_pangkat'); ?> 
                  </div>
                  <div class="modal-footer">
                  </div>
              </div>
          </div>
    </div>
    <!-- Modal Button Edit-->
    <div class="modal fade" id="ModalStruktural" role="dialog">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Data Jabatan Struktural</h4>
                  </div>
                  <div class="modal-body">
                    <?php $this->load->view('dosen/edit_struktural'); ?> 
                  </div>
                  <div class="modal-footer">
                  </div>
              </div>
          </div>
    </div>
    <!-- Modal Button Edit-->
    <div class="modal fade" id="ModalFungsional" role="dialog">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Data Jabatan Fungsional</h4>
                  </div>
                  <div class="modal-body">
                    <?php $this->load->view('dosen/edit_fungsional'); ?> 
                  </div>
                  <div class="modal-footer">
                  </div>
              </div>
          </div>
    </div>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

</script>

