<style type="text/css">
    body{
        height: 100%;
        width: 100%;
    }
    .image { 
        position: relative; 
        width: 100%; /* for IE 6 */
    }

    h2 { 
        position: absolute; 
        top: 135px; 
        left: 0; 
        width: 100%; 
        text-align: center;
        align-items: center;
    }
    h4.email { 
        position: absolute; 
        top: 430px; 
        left: -290px; 
        width: 100%; 
        text-align: center;
        align-items: left;
        color: white;
    }
    h4.hp { 
        position: absolute; 
        top: 430px; 
        left: 290px; 
        width: 100%; 
        text-align: center;
        align-items: left;
        color: white;
    }
    
    .qr { 
        position:absolute; 
        top: 950px; 
        left: 415px; 
        z-index: 2; 
    } 
</style><!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Kartu
        <small>Nama</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kartu Nama</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Kartu Nama</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <div class="image">
                <img src="<?php echo base_url('assets/img/ID/BackSide.jpg')?>" alt="" />
                <h2> <?php echo $depan.' '.$nama;if(!empty($blkg)) echo ', '.$blkg; ?></h2>
                <h4 class="email">
                    <?php echo $username ?> </br>
                    <?php echo $email ?>
                </h4>
                <h4 class="hp">
                    <?php echo $hp1 ?> <br/>
                    <?php echo $hp2 ?>
                </h4>
            </div>
            <br/>
            <div class="image">
                <img src="<?php echo base_url('assets/img/ID/FrontSide.jpg')?>" alt="" />
            </div>

             <img src="https://api.qrserver.com/v1/create-qr-code/?data=<?php echo '/visitor/profile/';?>&size=100x100" class ="qr" alt="" title="QR" />
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->