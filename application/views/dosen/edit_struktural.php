<?php echo form_open_multipart('dosen/editstruk');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Jabatan </label>
        <div class="col-sm-9">
            <select class="form-control" required name="jabatan" id="editjabstruk" onchange="editStruk();" >
                <option value= >--- Pilih Jabatan Struktural Anda ---</option>
                <option value="1">Ketua Prodi</option>
                <option value="2">Sekretaris Prodi</option>
                <option value="3">Ketua Departemen</option>
                <option value="4">Sekretaris Departemen</option>
                <option value="5">Ketua Laboratorium</option>
                <option value="6">Wakil Dekan</option>
                <option value="7">Dekan</option>
                <option value="8">Wakil Rektor</option>
                <option value="9">Rektor</option>
                <option value="10">Wakil Direktur</option>
                <option value="11">Direktur</option>
            </select>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Keterangan </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" name="keterangan" id="ketstruk" placeholder="Keterangan yang diperlukan" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Terhitung Mulai Tanggal  </label>
        <div class="col-sm-9">
            <div class="input-daterange input-group col-sm-12" id="datepicker">
                <input type="text" class="input-sm form-control fromDate" name="awal" required id="awalstruk" />
                <span class="input-group-addon"> Hingga </span>
                <input type="text" class="input-sm form-control toDate" name="selesai" required id="akhirstruk" />
            </div>
            <input type="hidden" name="jabatan" id="jabstrukedit" >
            <input type="hidden" name="idstruk" id="idstruk" >

        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div class="col-sm-11">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>        
<script type="text/javascript">
    var struk = new Array();

    struk[0] = "";                      struk[6] = 'Wakil Dekan';
    struk[1] = 'Ketua Prodi';           struk[7] = 'Dekan';
    struk[2] = 'Sekretaris Prodi';      struk[8] = 'Wakil Rektor';
    struk[3] = 'Ketua Departemen';      struk[9] = 'Rektor';
    struk[4] = 'Sekretaris Departemen'; struk[10] = 'Wakil Direktur';
    struk[5] = 'Ketua Laboratorium';    struk[11] = 'Direktur';
    

    function editStruk(){
        y = document.getElementById("editjabstruk");
        document.getElementById("jabstrukedit").value = struk[y.selectedIndex];
    };

</script>