<?php echo form_open_multipart('dosen/addleader');?>
    <fieldset >
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Nama Jabatan  </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Jabatan Anda" name="jabatan" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Tingkat  </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Tingkat " name="tingkat" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Periode  </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Periode " name="periode" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <div  style="padding-right: 25px">
                <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
                <button type="reset" class="btn btn-danger pull-right">Reset</button>
            </div>
        </div>
    </fieldset>
<?php echo form_close(); ?>      