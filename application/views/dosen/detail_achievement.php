<section class="content-header">
    <h1>
        Pencapaian
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pencapaian</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Trigger the modal with a button -->
    <?php echo $this->session->flashdata('pesan') ?>
    <br/>
    <!-- Modal HKI -->
    <div class="modal fade" id="hki" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah HKI Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_hki'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Jurnal -->
    <div class="modal fade" id="jurnal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Jurnal Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_jurnal'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Penelitian -->
    <div class="modal fade" id="penelitian" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Penelitian Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_penelitian'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Pengabdian -->
    <div class="modal fade" id="pengabdian" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Pengabdian Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_pengabdian'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Penghargaan -->
    <div class="modal fade" id="penghargaan" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Penghargaan Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_penghargaan'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Prosiding -->
    <div class="modal fade" id="prosiding" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Prosiding Baru</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_prosiding'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

  <!-- Default box -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Data Pencapaian</h3>
        </div>
        <div class="col-md-2"></div>
        <br><br>
        <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">HKI</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Jurnal</a></li>
                    <li><a href="#tab_6" data-toggle="tab">Prosiding</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Penelitian</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Pengabdian</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Penghargaan</a></li>
                  </ul>
                  
                  <div class="tab-content">
                  
                    <div class="tab-pane active" id="tab_1">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 80px;" data-toggle="modal" data-target="#hki"><i class="fa fa-fw fa-plus"></i>HKI</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($hki)) echo 'id="example1"'?> class="table table-bordered table-striped dataTable">
                            <thead>
                            <tr style="text-align: center;">
                              <th>Judul HKI</th>
                              <th>Tahun</th>
                              <th>Jenis HKI</th>
                              <th>Nomor HKI</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($hki)){
                            ?>
                                <tr>
                                    <td colspan="6" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($hki as $h) {
                            ?>
                                <tr>
                                    <td><?php echo $h['judul_hki']?></td>
                                    <td><?php echo $h['tahun']?></td>
                                    <td><?php echo $h['jenis_hki']?></td>
                                    <td><?php echo $h['no_hki']?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('hki',$h['id_hki']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $h['id_hki']?>" data-jenisAch='1' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                        
                                    </td>
                                    <td style="text-align: center;">
                                      <a class="btn btn-sm bg-maroon hki" data-toggle="modal"
                                      data-idhki="<?php echo $h['id_hki']?>"
                                      data-judul-hki="<?php echo $h['judul_hki']?>"
                                      data-tahun="<?php echo $h['tahun']?>"
                                      data-jenis="<?php echo $h['jenis_hki']?>"
                                      data-no-hki="<?php echo $h['no_hki']?>"
                                      data-target="#myModalHKI">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                      </a>
                                      <a href="" data-url="<?php echo base_url('dosen/delHKI/'.$h['id_hki']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="tab_2">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 80px;" data-toggle="modal" data-target="#jurnal"><i class="fa fa-fw fa-plus"></i>Jurnal</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($jurnal)) echo 'id="example2"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr style="text-align: center;">
                              <th>Judul Jurnal</th>
                              <th>Tahun</th>
                              <th>Volume</th>
                              <th>No Jurnal</th>
                              <th>Tingkat</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($jurnal)){
                            ?>
                                <tr>
                                    <td colspan="7" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($jurnal as $j) {
                            ?>
                                <tr>
                                    <td><?php echo $j['judul_jurnal']?></td>
                                    <td><?php echo $j['tahun']?></td>
                                    <td><?php echo $j['volume']?></td>
                                    <td><?php echo $j['no_jurnal']?></td>
                                    <td><?php echo $j['tingkat']?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('jurnal',$j['id_jurnal']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $j['id_jurnal']?>" data-jenisAch='2' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                    <a class="btn btn-sm btn-primary viewjurnal"
                                        data-toggle="modal"
                                        data-idjurnal-view="<?php echo $j['id_jurnal']?>"
                                        data-judul-jurnal-view="<?php echo $j['judul_jurnal']?>"
                                        data-judul-artikel-view="<?php echo $j['judul_artikel']?>"
                                        data-tahun-jurnal-view="<?php echo $j['tahun']?>"
                                        data-volume-view="<?php echo $j['volume']?>"
                                        data-no-jurnal-view="<?php echo $j['no_jurnal']?>"
                                        data-tanggal-view="<?php echo $j['tgl_terbit']?>"
                                        data-indeks-view="<?php echo $j['indeks']?>"
                                        data-tingkat-view="<?php echo $j['tingkat']?>"
                                        data-penulis-view="<?php echo $j['penulis_ke']?>"
                                        data-url-view="<?php echo $j['url_jurnal']?>"
                                        data-target="#myModalViewJurnal">
                                        <i class="fa fa-fw fa-bars"></i>
                                        &nbspDetail
                                    </a>  
                                    <a class="btn btn-sm bg-maroon editjurnal"
                                        data-toggle="modal"
                                        data-idjurnal-edit="<?php echo $j['id_jurnal']?>"
                                        data-judul-jurnal-edit="<?php echo $j['judul_jurnal']?>"
                                        data-judul-artikel-edit="<?php echo $j['judul_artikel']?>"
                                        data-tahun-jurnal-edit="<?php echo $j['tahun']?>"
                                        data-volume-edit="<?php echo $j['volume']?>"
                                        data-no-jurnal-edit="<?php echo $j['no_jurnal']?>"
                                        data-tanggal-edit="<?php echo $j['tgl_terbit']?>"
                                        data-indeks-edit="<?php echo $j['indeks']?>"
                                        data-tingkat-edit="<?php echo $j['tingkat']?>"
                                        data-penulis-edit="<?php echo $j['penulis_ke']?>"
                                        data-url-edit="<?php echo $j['url_jurnal']?>"
                                        data-target="#myModalJurnal">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                    </a>
                                      <a href="" data-url="<?php echo base_url('dosen/delJurnal/'.$j['id_jurnal']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="tab_3">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 90px;" data-toggle="modal" data-target="#penelitian"><i class="fa fa-fw fa-plus"></i>Penelitian</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($penelitian)) echo 'id="example3"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr style="text-align: center;">
                              <th>Judul Penelitian</th>
                              <th>Tahun</th>
                              <th>Sumber Dana</th>
                              <th>Jumlah Dana</th>
                              <th>Status</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($penelitian)){
                            ?>
                                <tr>
                                    <td colspan="8" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($penelitian as $pl) {
                            ?>
                                <tr>
                                    <td><?php echo $pl['judul_penelitian']?></td>
                                    <td><?php echo $pl['tahun']?></td>
                                    <td><?php echo $pl['sumber_dana']?></td>
                                    <td><?php echo $pl['jumlah_dana']?></td>
                                    <td><?php echo $pl['status']?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('penelitian',$pl['id_penelitian']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $pl['id_penelitian']?>" data-jenisAch='4' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                      <a class="btn btn-sm bg-maroon penelitian" data-toggle="modal" data-idpenelitian="<?php echo $pl['id_penelitian']?>" data-judul-penelitian="<?php echo $pl['judul_penelitian']?>" data-tahun-penelitian="<?php echo $pl['tahun']?>" data-sumber-dana-penelitian="<?php echo $pl['sumber_dana']?>" data-jumlah-dana-penelitian="<?php echo $pl['jumlah_dana']?>" data-status-penelitian="<?php echo $pl['status']?>" data-target="#myModalPenelitian">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                      </a>
                                      <a href="" data-url="<?php echo base_url('dosen/delPenelitian/'.$pl['id_penelitian']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="tab_4">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 100px;" data-toggle="modal" data-target="#pengabdian"><i class="fa fa-fw fa-plus"></i>Pengabdian</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($pengabdian)) echo 'id="example4"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr style="text-align: center;">
                              <th>Judul Pengabdian</th>
                              <th>Tahun</th>
                              <th>Sumber Dana</th>
                              <th>Jumlah Dana</th>
                              <th>Status</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($pengabdian)){
                            ?>
                                <tr>
                                    <td colspan="8" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($pengabdian as $pg) {
                            ?>
                                <tr>
                                    <td><?php echo $pg['judul_pengabdian']?></td>
                                    <td><?php echo $pg['tahun']?></td>
                                    <td><?php echo $pg['sumber_dana']?></td>
                                    <td><?php echo $pg['jumlah_dana']?></td>
                                    <td><?php echo $pg['status']?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('pengabdian',$pg['id_pengabdian']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $pg['id_pengabdian']?>" data-jenisAch='5' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                      <a class="btn btn-sm bg-maroon pengabdian" data-toggle="modal" data-idpengabdian="<?php echo $pg['id_pengabdian']?>" data-judul-pengabdian="<?php echo $pg['judul_pengabdian']?>" data-tahun-pengabdian="<?php echo $pg['tahun']?>" data-sumber-dana-pengabdian="<?php echo $pg['sumber_dana']?>" data-jumlah-dana-pengabdian="<?php echo $pg['jumlah_dana']?>" data-status-pengabdian="<?php echo $pg['status']?>" data-target="#myModalPengabdian">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                      </a> 
                                      <a href="" data-url="<?php echo base_url('dosen/delPengabdian/'.$pg['id_pengabdian']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="tab_5">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 100px;" data-toggle="modal" data-target="#penghargaan"><i class="fa fa-fw fa-plus"></i>Penghargaan</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($penghargaan)) echo 'id="example5"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr style="text-align: center;">
                              <th>Nama Penghargaan</th>
                              <th>Tahun</th>
                              <th>Pemberi</th>
                              <th>Tingkat</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($penghargaan)){
                            ?>
                                <tr>
                                    <td colspan="6" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($penghargaan as $ph) {
                            ?>
                                <tr>
                                    <td><?php echo $ph['nama_penghargaan']?></td>
                                    <td><?php echo $ph['tahun']?></td>
                                    <td><?php echo $ph['pemberi']?></td>
                                    <td><?php echo $ph['tingkat']?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('penghargaan',$ph['id_penghargaan']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $ph['id_penghargaan']?>" data-jenisAch='6' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                      <a class="btn btn-sm bg-maroon penghargaan" data-toggle="modal" data-idpenghargaan="<?php echo $ph['id_penghargaan']?>" data-nama-penghargaan="<?php echo $ph['nama_penghargaan']?>" data-tahun-penghargaan="<?php echo $ph['tahun']?>" data-pemberi="<?php echo $ph['pemberi']?>" data-tingkat-penghargaan="<?php echo $ph['tingkat']?>" data-target="#myModalPenghargaan">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                      </a> 
                                      <a href="" data-url="<?php echo base_url('dosen/delPenghargaan/'.$ph['id_penghargaan']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                    <div class="tab-pane" id="tab_6">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-info btn-sm" style="width: 90px;" data-toggle="modal" data-target="#prosiding"><i class="fa fa-fw fa-plus"></i>Prosiding</button>
                        </div>
                        <br><br>
                        <div class="box-body">
                          <table <?php if(!empty($prosiding)) echo 'id="example6"'?> class="table table-bordered table-striped">
                            <thead>
                            <tr>
                              <th>Nama Prosiding</th>
                              <th>Judul Artikel</th>
                              <th>Tanggal</th>
                              <th style="text-align: center;">Dokumen</th>
                              <th style="text-align: center;">Kontrol</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                if(empty($prosiding)){
                            ?>
                                <tr>
                                    <td colspan="5" style="text-align: center">(Data belum ada)</td>
                                </tr>
                            <?php
                                } else {
                                    foreach ($prosiding as $pr) {
                            ?>
                                <tr>
                                    <td><?php echo $pr['nama_prosiding']?></td>
                                    <td><?php echo $pr['judul_artikel']?></td>
                                    <?php
                                        setlocale (LC_ALL, 'IND');
                                        $date=date_create($pr['tanggal1']);
                                        $bulan1 = strftime("%b", $date->getTimestamp());
                                        $tanggal1 = strftime("%d", $date->getTimestamp());
                                        $tahun1 = strftime("%Y", $date->getTimestamp());
                                        setlocale (LC_ALL, 'IND');
                                        $date=date_create($pr['tanggal2']);
                                        $bulan2 = strftime("%b", $date->getTimestamp());
                                        $tanggal2 = strftime("%d", $date->getTimestamp());
                                        $tahun2 = strftime("%Y", $date->getTimestamp());
                                    ?>
                                    <td><?php echo $tanggal1.' '.$bulan1.' '.$tahun1.' s/d 
                                        '.$tanggal2.' '.$bulan2.' '.$tahun2;?></td>
                                    <td style="text-align: center;">
                                        <?php 
                                        $file = $this->mdosen->getfileAch('prosiding',$pr['id_prosiding']);
                                        if(empty($file)){
                                        ?>
                                        <a class="btn btn-sm btn-primary datafileAch" data-toggle="modal" data-idAch="<?php echo $pr['id_prosiding']?>" data-jenisAch='3' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                        <?php
                                        } else {
                                        ?>
                                        <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                        <a class="btn btn-sm btn-danger confDelDokAch" href="" data-url="<?php echo base_url('dosen/delDokumenAch/'.$file[0]['idDokumen']); ?>" ><i class="glyphicon glyphicon-remove"></i>&nbsp&nbsp Hapus</a> </td>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td style="text-align: center;">
                                      <a class="btn btn-sm btn-primary viewprosiding" data-toggle="modal" 
                                      data-idprosiding-view="<?php echo $pr['id_prosiding']?>" 
                                      data-nama-prosiding-view="<?php echo $pr['nama_prosiding']?>" 
                                      data-judul-artikel-view="<?php echo $pr['judul_artikel']?>" 
                                      data-tempat-prosiding-view="<?php echo $pr['tempat']?>" 
                                      data-tanggal1-view="<?php echo $pr['tanggal1']?>" 
                                      data-tanggal2-view="<?php echo $pr['tanggal2']?>" 
                                      data-penuliske-view="<?php echo $pr['penulis_ke']?>" 
                                      data-url-prosiding-view="<?php echo $pr['url_prosiding']?>" 
                                      data-target="#myModalViewProsiding">
                                        <i class="fa fa-fw fa-bars"></i>
                                        &nbspDetail
                                      </a>
                                      <a class="btn btn-sm bg-maroon editprosiding" data-toggle="modal" 
                                      data-idprosiding-edit="<?php echo $pr['id_prosiding']?>" 
                                      data-nama-prosiding-edit="<?php echo $pr['nama_prosiding']?>" 
                                      data-judul-artikel-edit="<?php echo $pr['judul_artikel']?>" 
                                      data-tempat-prosiding-edit="<?php echo $pr['tempat']?>" 
                                      data-tanggal1-edit="<?php echo $pr['tanggal1']?>" 
                                      data-tanggal2-edit="<?php echo $pr['tanggal2']?>" 
                                      data-penuliske-edit="<?php echo $pr['penulis_ke']?>" 
                                      data-url-prosiding-edit="<?php echo $pr['url_prosiding']?>" 
                                      data-target="#myModalProsiding">
                                        <i class="fa fa-fw fa-edit"></i>
                                        &nbspUbah
                                      </a>
                                      <a href="" data-url="<?php echo base_url('dosen/delProsiding/'.$pr['id_prosiding']); ?>" class="btn btn-danger btn-sm confDelAch"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                    </td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                  </div><!-- /.box -->
                  <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
            <!-- /.col -->
          </div>          
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal Button Edit-->
<div class="modal fade" id="myModalHKI" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data HKI</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_hki'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalJurnal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Jurnal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_jurnal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
</div>

<div class="modal fade" id="myModalViewJurnal" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Detail Data Pencapaian Jurnal</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/view_jurnal'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalPenelitian" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Penelitian</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_penelitian'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalPengabdian" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Pengabdian</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_pengabdian'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalPenghargaan" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Penghargaan</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_penghargaan'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalProsiding" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Data Prosiding</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_prosiding'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalViewProsiding" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Detail Data Prosiding</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/view_prosiding'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="modaluploadfile" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Tambah Dokumen</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/add_dokumen_ach'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

</script>
<!-- page script -->
</html>