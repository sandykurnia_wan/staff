<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Kelola
        <small>Akun</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kelola Akun</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Ubah Foto Profil</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php echo form_open_multipart('dosen/profilePhoto');?>
            <fieldset>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="filename" class="control-label">Pilih Gambar Untuk Diunggah</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <?php 
                                 $id = $this->session->userdata('username');
                                $ppdata = $this->mdosen->getProfpic($id);
                                $profpic = $ppdata[0]['filename'];
                            ?>
                            <!-- <div class="image" align="left">
                                <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" class="img-circle" alt="User Image" width="250" />
                            </div> -->
                            <!-- <input type="file" name="filename" size="50" /> -->
                            <input id="uploadImage" type="file" name="filename" accept=".png,.jpg,.jpeg,.gif" onchange="PreviewImage();" /><br>
                            <img id="uploadPreview" width="250" />
                            <span class="text-danger"><?php if (isset($error)) { echo $error; } ?></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="submit" value="Unggah" class="btn btn-primary"/>
                        </div>
                    </div>
                </div>
            </fieldset>
            <?php echo form_close(); ?>
            <?php if (isset($success_msg)) { echo $success_msg; } ?>
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Ubah Password</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <?php 
            $js = 'onSubmit="return validatePass()';
            echo form_open_multipart('dosen/updatePassword','',$js);?>
            <fieldset>
            <!-- // <form onsubmit="return validatePass()" action ="<?php echo base_url('dosen/updatePassword')?>" class="form-horizontal" method="post"> -->
                <div class="form-group" style="padding-bottom: 25px;">
                    <label class="col-sm-3 control-label">Password Baru</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control"  placeholder="Masukkan password baru Anda" name="password" pattern='[a-zA-Z0-9/S]{8,}$' id='pass1' required>
                    </div>
                </div>
                <div class="form-group" style="padding-bottom: 25px;">
                    <label class="col-sm-3 control-label">Konfirmasi Password Baru</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control"  placeholder="Masukkan password lagi" name="confirm_password" pattern='[a-zA-Z0-9/S]{8,}$' id='pass2' required>
                    </div>
                </div>
                <div class="form-group" style="padding-bottom: 25px;">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                    </div>
                </div>
            </fieldset>
            <?php echo form_close(); ?>           
        </div><!-- /.box-body -->
        <div class="box-footer">
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->
<script type="text/javascript">
    function validatePass() {
        var pass1 = document.getElementById("pass1").value;
        var pass2 = document.getElementById("pass2").value;
        var lgth = document.getElementById("pass1").value.length;
        if (lgth <8){
            alert("Panjang password kurang dari 8 karakter!");
            document.getElementById("pass1").style.borderColor = "#E34234";
            //document.getElementById("pass2").style.borderColor = "#E34234";
            return false;
        } else if (pass1 != pass2) {
            //alert("Passwords Do not match");
            alert("Passwords Tidak Sama!");
            //document.getElementById("pass1").style.borderColor = "#E34234";
            document.getElementById("pass2").style.borderColor = "#E34234";
            return false;
        }
        
    }
</script>
<script type="text/javascript">
    function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
    oFReader.onload = function (oFREvent)
        {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>
