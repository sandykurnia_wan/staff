<?php echo form_open_multipart('dosen/editnarasumber');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Judul Acara  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" maxlength="200" placeholder="Masukkan Judul Acara Anda" name="judul" id="judulNara" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Penyelenggara  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Nama Penyelenggara Acara" name="penyelenggara" id="penyelenggaraNara" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Tanggal  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control datepicker" placeholder="Masukkan Tanggal Acara Anda" name="tanggal" id="tanggalNara" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Tingkat  </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" maxlength="50" placeholder="Masukkan Tingkat Kegiatan Anda" name="tingkat" id="tingkatNara" required>
            <input type="hidden" class="form-control"  name="idnara" id="idnara" >
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div  style="padding-right: 25px">
            <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>             