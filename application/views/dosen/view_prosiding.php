<form action ="<?php echo base_url('dosen/viewProsiding')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Nama Prosiding</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Nama Prosiding Anda" name="nama_prosiding" id="nama_prosiding_view" disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Judul Artikel</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Artikel Anda" name="judul_artikel" id="judul_artikel_view" disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tempat</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Nama Tempat Prosiding Anda" name="tempat" id="tempat_prosiding_view" disabled>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal</label>
        <div class="col-sm-9">
            <div class="input-daterange input-group col-sm-12">
                <input type="text" style="text-align: left;" class="form-control fromDate" name="tanggal1" id="tanggal1_view" disabled/>
                <span class="input-group-addon">s/d </span>
                <input type="text" style="text-align: left;" class="form-control toDate" name="tanggal2" id="tanggal2_view" disabled/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Penulis Ke</label>
        <div class="col-sm-9">
            <select class="form-control" name="penulis_ke" id="penuliske_view" disabled>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4 dst">4 dan seterusnya</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">URL</label>
        <div class="col-sm-9">
            <input type="url" class="form-control"  placeholder="Masukkan URL Prosiding Anda" name="url_prosiding" id="url_prosiding_view" disabled>
            <p class="help-block">Contoh: http://www.prosiding.com/prosidingku</p>
            <input type="hidden" class="form-control" name="id_prosiding" id="id_prosiding_view" >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>