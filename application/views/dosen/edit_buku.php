<?php echo form_open_multipart('dosen/editbuku');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Judul Buku  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="200" placeholder="Masukkan Judul Buku Anda" name="judul" id="judulbuku" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Tahun  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control yearpicker"  placeholder="Masukkan Tahun Terbit Buku Anda" name="tahun" id="tahunterbit" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Jumlah halaman  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Jumlah Halaman Buku Anda" name="jml_hlmn" id="halaman" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Penerbit  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control" maxlength="100" placeholder="Masukkan Penerbit Buku Anda" name="penerbit" id="penerbit" required>
            <input type="hidden" class="form-control"  name="idbuku" id="idbuku" >
        </div>
    </div>
    <div class="form-group">
        <div  style="padding-right: 25px">
            <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>               