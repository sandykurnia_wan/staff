<form action ="<?php echo base_url('dosen/addJurnal')?>" class="form-horizontal" method="post">
    <div class="form-group">
        <label class="col-sm-2 control-label">Nama Jurnal</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Nama Jurnal Anda" name="judul_jurnal" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Judul Artikel</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Judul Artikel Anda" name="judul_artikel" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tahun</label>
        <div class="col-sm-9">
            <input type="text" maxlength="4" pattern="[0-9]{4,4}" class="form-control yearpicker" placeholder="Masukkan Tahun Terbit Jurnal Anda" name="tahun"  required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Volume</label>
        <div class="col-sm-9">
            <input type="text" class="form-control"  placeholder="Masukkan Volume Jurnal Anda" name="volume" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nomor Jurnal</label>
        <div class="col-sm-9">
            <input type="text" maxlength="15" class="form-control"  placeholder="Masukkan Nomor Jurnal Anda" name="no_jurnal" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Terbit</label>
        <div class="col-sm-9">
            <input type="text" style="text-align: left;" class="form-control datepicker" name="tgl_terbit" placeholder="Masukkan Tanggal Terbit Jurnal Anda" required/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Terindeks Scopus</label>
        <div class="col-sm-2">    
            <div class="radio">
                <label>
                    <input type="radio" name="indeks" value="Ya">
                    Ya
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="indeks" value="Tidak">
                    Tidak
                </label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tingkat</label>
        <div class="col-sm-9">
            <select class="form-control" name="tingkat">
                <option value="Nasional">Nasional</option>
                <option value="Internasional">Internasional</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Penulis Ke</label>
        <div class="col-sm-9">
            <select class="form-control" name="penulis_ke">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4 dst">4 dan seterusnya</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">URL</label>
        <div class="col-sm-9">
            <input type="url" class="form-control"  placeholder="Masukkan URL Jurnal Anda" name="url_jurnal" required>
            <p class="help-block">Contoh: http://www.jurnal.com/jurnalku</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</form>