<section class="content-header">
    <h1>
        Kegiatan
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kegiatan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Trigger the modal with a button -->
    <?php echo $this->session->flashdata('pesan') ?>
    <br/>
    <!-- Modal Organisasi -->
    <div class="modal fade" id="org" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kegiatan Organisasi</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_org'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Buku -->
    <div class="modal fade" id="buku" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kegiatan Penulisan Buku</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_buku'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Review -->
    <div class="modal fade" id="review" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kegiatan Reviewer</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_review'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Narasumber -->
    <div class="modal fade" id="narasumber" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kegiatan Narasumber</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_narasumber'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Leadership -->
    <div class="modal fade" id="leadership" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kegiatan Kepemimpinan</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('dosen/add_leadership'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
  <!-- Default box -->
    <div class="box box-primary" >
        <div class="box-header with-border">
            <h3 class="box-title">Data Kegiatan Dosen</h3>
        </div>
        <div class="box-body">
            <div class="col-lg-12">
                <h5 style="text-align: justify;">
                    Halaman ini menampilkan segala kegiatan yang pernah dilakukan dosen. Kegiatan dosen dikategorikan menjadi 5 kegiatan. Untuk menambahkan data kegiatan dosen, silahkan memilih tombol sesuai dengan jenis kegiatan yang ingin ditambahkan.
                </h5>
            </div>
            <div class="col-lg-12" >
                <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#org"><i class="fa fa-fw fa-plus"></i> Organisasi </a>
                <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#buku"><i class="fa fa-fw fa-plus"></i> Penulisan Buku </a>
                <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#review"><i class="fa fa-fw fa-plus"></i> Reviewer </a>
                <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#narasumber"><i class="fa fa-fw fa-plus"></i> Narasumber </a>
                <a href="" style="margin-right: 15px" class="btn btn-primary pull-left" data-toggle="modal" data-target="#leadership"><i class="fa fa-fw fa-plus"></i> Kepemimpinan </a>
            </div>
            <div><br></div>
            <div class="row" >
                <div class="col-md-12" style="margin-top: 20px">
                  <!-- Custom Tabs -->
                  <br>
                    <div class="nav-tabs-custom">
                      <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Organisasi</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Penulisan Buku</a></li>
                        <li><a href="#tab_3" data-toggle="tab">Reviewer</a></li>
                        <li><a href="#tab_4" data-toggle="tab">Narasumber</a></li>
                        <li><a href="#tab_5" data-toggle="tab">Kepemimpinan</a></li>
                      </ul>                      
                      <div class="tab-content">                      
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body">
                              <table <?php if(!empty($org)) echo 'id="example1"' ?> class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Nama Organisasi</th>
                                  <th>Tahun</th>
                                  <th>Jabatan</th>
                                  <th>No Keanggotaan</th>
                                  <th>Dokumen</th>
                                  <th>Kontrol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(empty($org)){
                                ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center">Data belum ada</td>
                                    </tr>
                                <?php
                                    } else {
                                        foreach ($org as $o) {
                                    
                                ?>
                                    <tr>
                                        <td><?php echo $o['judul']?></td>
                                        <td align="center"><?php echo $o['tahun']?></td>
                                        <td align="center"><?php echo $o['jabatan']?></td>
                                        <td align="center"><?php echo $o['no_anggota']?></td>
                                        <td align="center">
                                            <?php 
                                            $file = $this->mdosen->getfile('org',$o['id_org']);
                                            if(empty($file)){
                                            ?>
                                            <a class="btn btn-sm btn-primary datafile" data-toggle="modal" data-idAct="<?php echo $o['id_org']?>" data-jenisAct='1' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 
                                            <a  href="" data-url="<?php echo base_url('dosen/delDokumen/'.$file[0]['idDokumen']); ?>" class="btn btn-danger btn-sm confDelDok"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                            
                                            <?php
                                            }
                                            ?>
                                            
                                        <td align="center">
                                          <a class="btn btn-sm btn-primary org" data-toggle="modal" data-idorg="<?php echo $o['id_org']?>" data-judul="<?php echo $o['judul']?>" data-tahun="<?php echo $o['tahun']?>" data-jab="<?php echo $o['jabatan']?>" data-no="<?php echo $o['no_anggota']?>" data-target="#myModalOrg"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>
                                          
                                          <a href="" data-url="<?php echo base_url('dosen/delOrg/'.$o['id_org']); ?>" class="btn btn-danger btn-sm confDelKeg"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                          
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body">
                              <table <?php if(!empty($buku)) echo 'id="example2"' ?> class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Judul Buku</th>
                                  <th>Tahun Terbit</th>
                                  <th>Jumlah Halaman</th>
                                  <th>Penerbit</th>
                                  <th>Dokumen</th>
                                  <th>Kontrol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(empty($buku)){
                                ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center">Data belum ada</td>
                                    </tr>
                                <?php
                                    } else {
                                        foreach ($buku as $b) {
                                ?>
                                    <tr>
                                        <td><?php echo $b['judul_buku']?></td>
                                        <td align="center"><?php echo $b['tahun_terbit']?></td>
                                        <td align="center"><?php echo $b['jml_hlmn']?></td>
                                        <td align="center"><?php echo $b['penerbit']?></td>
                                        <td align="center">
                                            <?php 
                                            $file = $this->mdosen->getfile('buku',$b['id_buku']);
                                            if(empty($file)){
                                            ?>
                                            <a class="btn btn-sm btn-primary datafile" data-toggle="modal" data-idAct="<?php echo $b['id_buku']?>" data-jenisAct='2' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="btn btn-sm btn-info org" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 

                                            <a  href="" data-url="<?php echo base_url('dosen/delDokumen/'.$file[0]['idDokumen']); ?>" class="btn btn-danger btn-sm confDelDok"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                            
                                            <?php
                                            }
                                            ?>

                                        </td>
                                        <td align="center">
                                          <a class="btn btn-sm btn-primary buku" data-toggle="modal" data-idbuku="<?php echo $b['id_buku']?>" data-judulbuku="<?php echo $b['judul_buku']?>" data-tahunterbit="<?php echo $b['tahun_terbit']?>" data-halaman="<?php echo $b['jml_hlmn']?>" data-penerbit="<?php echo $b['penerbit']?>" data-target="#myModalBuku"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>
                                          
                                          <a  href="" data-url="<?php echo base_url('dosen/delBuku/'.$b['id_buku']);?>" class="btn btn-danger btn-sm confDelKeg"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="tab_3">
                            <div class="box-body">
                              <table <?php if(!empty($rev)) echo 'id="example3"' ?> class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Judul Kegiatan Reviewer</th>
                                  <th>Penyelenggara</th>
                                  <th>Tingkat</th>
                                  <th>Tanggal</th>
                                  <th>Dokumen</th>
                                  <th>Kontrol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(empty($rev)){
                                ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center">Data belum ada</td>
                                    </tr>
                                <?php
                                    } else { 
                                        foreach ($rev as $r) {
                                ?>
                                    <tr>
                                        <td><?php echo $r['judul']?></td>
                                        <td align="center"><?php echo $r['penyelenggara']?></td>
                                        <td align="center"><?php echo $r['tingkat']?></td>
                                        <?php 
                                          setlocale (LC_ALL, 'IND');
                                          $date=date_create($r['tanggal']);
                                          $bulan = strftime("%B", $date->getTimestamp());
                                          $tanggal = strftime("%d", $date->getTimestamp());
                                          $tahun = strftime("%Y", $date->getTimestamp());
                                        ?>
                                        <td align="center">
                                            <?php 
                                                if ($r['tanggal'] == '0000-00-00') {
                                                    echo '';
                                                }else{
                                                    echo $tanggal.' '.$bulan.' '.$tahun;
                                                }
                                            ?>
                                        </td>
                                        <td align="center">
                                            <?php 
                                            $file = $this->mdosen->getfile('review',$r['id_review']);
                                            if(empty($file)){
                                            ?>
                                            <a class="btn btn-sm btn-primary datafile" data-toggle="modal" data-idAct="<?php echo $r['id_review']?>" data-jenisAct='3' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 

                                            <a  href="" data-url="<?php echo base_url('dosen/delDokumen/'.$file[0]['idDokumen']); ?>" class="btn btn-danger btn-sm confDelDok"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                            
                                            <?php
                                            }
                                            ?>

                                        </td>
                                        <td align="center">
                                          <a class="btn btn-sm btn-primary review" data-toggle="modal" data-idrev="<?php echo $r['id_review']?>" data-judul="<?php echo $r['judul']?>" data-penyelenggara="<?php echo $r['penyelenggara']?>" data-tingkat="<?php echo $r['tingkat']?>" data-tanggal="<?php echo $r['tanggal']?>" data-target="#myModalReview"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>

                                          <a  href="" data-url="<?php echo base_url('dosen/delRev/'.$r['id_review']); ?>" class="btn btn-danger btn-sm confDelKeg"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div class="box-body">
                              <table <?php if(!empty($nara)) echo 'id="example4"' ?> class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Judul Acara</th>
                                  <th>Penyelenggara</th>
                                  <th>Tanggal</th>
                                  <th>Tingkat</th>
                                  <th>Dokumen</th>
                                  <th>Kontrol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(empty($nara)){
                                ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center">Data belum ada</td>
                                    </tr>
                                <?php
                                    } else { 
                                        foreach ($nara as $n) {
                                ?>
                                    <tr>
                                        <td><?php echo $n['judul']?></td>
                                        <td><?php echo $n['penyelenggara']?></td>
                                        <?php 
                                          setlocale (LC_ALL, 'IND');
                                          $date=date_create($n['tanggal']);
                                          $bulan = strftime("%B", $date->getTimestamp());
                                          $tanggal = strftime("%d", $date->getTimestamp());
                                          $tahun = strftime("%Y", $date->getTimestamp());
                                        ?>
                                        <td align="center">
                                            <?php 
                                                if ($n['tanggal'] == '0000-00-00') {
                                                    echo '';
                                                }else{
                                                    echo $tanggal.' '.$bulan.' '.$tahun;
                                                }
                                            ?>
                                        </td>
                                        <td align="center"><?php echo $n['tingkat']?></td>
                                        <td align="center">
                                            <?php 
                                            $file = $this->mdosen->getfile('narasumber',$n['id_narasumber']);
                                            if(empty($file)){
                                            ?>
                                            <a class="btn btn-sm btn-primary datafile" data-toggle="modal" data-idAct="<?php echo $n['id_narasumber']?>" data-jenisAct='4' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 

                                            <a  href="" data-url="<?php echo base_url('dosen/delDokumen/'.$file[0]['idDokumen']); ?>" class="btn btn-danger btn-sm confDelDok"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                            
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td align="center">
                                          <a class="btn btn-sm btn-primary narasumber" data-toggle="modal" data-idnara="<?php echo $n['id_narasumber']?>" data-judul="<?php echo $n['judul']?>" data-penyelenggara="<?php echo $n['penyelenggara']?>" data-tanggal="<?php echo $n['tanggal']?>" data-tingkat="<?php echo $n['tingkat']?>" data-target="#myModalNarasumber"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>

                                          <a  href="" data-url="<?php echo base_url('dosen/delNara/'.$n['id_narasumber']); ?>" class="btn btn-danger btn-sm confDelKeg"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div class="tab-pane" id="tab_5">
                            <div class="box-body">
                              <table <?php if(!empty($leader)) echo 'id="example5"' ?> class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                  <th>Jabatan</th>
                                  <th>Tingkat</th>
                                  <th>Periode</th>
                                  <th>Dokumen</th>
                                  <th>Kontrol</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    if(empty($leader)){
                                ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center">Data belum ada</td>
                                    </tr>
                                <?php
                                    } else { 
                                        foreach ($leader as $l) {
                                ?>
                                    <tr>
                                        <td><?php echo $l['jabatan']?></td>
                                        <td align="center"><?php echo $l['tingkat']?></td>
                                        <td align="center"><?php echo $l['periode']?></td>
                                        <td align="center">
                                            <?php 
                                            $file = $this->mdosen->getfile('leader',$l['id_leader']);
                                            if(empty($file)){
                                            ?>
                                            <a class="btn btn-sm btn-primary datafile" data-toggle="modal" data-idAct="<?php echo $l['id_leader']?>" data-jenisAct='5' data-target="#modaluploadfile"><i class="fa fa-upload"></i>&nbsp&nbsp Unggah</a>
                                            <?php
                                            } else {
                                            ?>
                                            <a class="btn btn-sm btn-info" href="<?php echo base_url('uploads/dokumen/'.$file[0]['filename']); ?>" ><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Unduh</a> 

                                            <a  href="" data-url="<?php echo base_url('dosen/delDokumen/'.$file[0]['idDokumen']); ?>" class="btn btn-danger btn-sm confDelDok"><i class="fa fa-fw fa-trash"></i>  Hapus</a>
                                            
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        <td align="center">
                                          <a class="btn btn-sm btn-primary leadership" data-toggle="modal" data-idleader="<?php echo $l['id_leader']?>" data-jabatan="<?php echo $l['jabatan']?>" data-tingkat="<?php echo $l['tingkat']?>" data-periode="<?php echo $l['periode']?>" data-target="#myModalLeadership"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Ubah</a>

                                          <a  href="" data-url="<?php echo base_url('dosen/delLeader/'.$l['id_leader']); ?>" class="btn btn-danger btn-sm confDelKeg"><i class="fa fa-fw fa-trash"></i>  Hapus</a> 
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    }
                                ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                      </div><!-- /.box -->
                      <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div> 
        </div>
      
    </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal Button Edit-->
<div class="modal fade" id="myModalOrg" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Data Organisasi</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_org'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalBuku" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Data Penulisan Buku</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_buku'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalReview" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Data Reviewer</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_review'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalNarasumber" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Data Narasumber</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_narasumber'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<div class="modal fade" id="myModalLeadership" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Ubah Data Kepemimpinan</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/edit_leadership'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<!--Modal button upload file -->
<div class="modal fade" id="modaluploadfile" role="dialog">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Tambah Dokumen</h4>
              </div>
              <div class="modal-body">
                <?php $this->load->view('dosen/add_dokumen'); ?> 
              </div>
              <div class="modal-footer">
              </div>
          </div>
      </div>
  </div>

<!-- Page script -->

<script type="text/javascript">
  function confirmDelete(){
      conf = confirm("Anda Yakin Akan Menghapus Data ?");
      if (conf == true)
          return true;
      else
          return false;
  };


</script>

<!-- page script -->
</html>