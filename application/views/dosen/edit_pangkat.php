<?php echo form_open_multipart('dosen/editpangkat');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Golongan/ Ruang  </label>
        <div class="col-sm-9">
            <select class="form-control" id="editgolruang" name="golruang" onChange='ChoiceEdit();'>
                <option value= >Pilih Golongan/Ruang Anda</option>
                <option value="1">I/a</option>
                <option value="2">I/b</option>
                <option value="3">I/c</option>
                <option value="4">I/d</option>
                <option value="5">II/a</option>
                <option value="6">II/b</option>
                <option value="7">II/c</option>
                <option value="8">II/d</option>
                <option value="9">III/a</option>
                <option value="10">III/b</option>
                <option value="11">III/c</option>
                <option value="12">III/d</option>
                <option value="13">IV/a</option>
                <option value="14">IV/b</option>
                <option value="15">IV/c</option>
                <option value="16">IV/d</option>
                <option value="17">IV/e</option>
            </select>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Pangkat  </label>
        <div class="col-sm-9">
            <input class="form-control" type="text" id="pangkatedit" name="pangkatedit" readonly="readonly">
            <input class="form-control" type="hidden" id="goledit" name="goledit" >
            <input class="form-control" type="hidden" id="pangkatlama" name="pangkatlama" >
            <input class="form-control" type="hidden" id="idpgkt" name="idpgkt" >

        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-3 control-label">Terhitung Mulai Tanggal  </label>
        <div class="col-sm-9">
            <input type="text" class="form-control datepicker" id="tmtpgkt" name="tmtedit" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div class="col-sm-11">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>        
<script type="text/javascript">
    var pangkat = new Array();
    var goladd = new Array();

    pangkat[0] = "";                        goladd[0] = "";
    pangkat[1] = 'Juru Muda';               goladd[1] = "I/a";
    pangkat[2] = 'Juru Muda Tingkat I';     goladd[2] = "I/b";
    pangkat[3] = 'Juru ';                   goladd[3] = "I/c";
    pangkat[4] = 'Juru Tingkat I';          goladd[4] = "I/d";
    pangkat[5] = 'Pengatur Muda';           goladd[5] = "II/a";
    pangkat[6] = 'Pengatur Muda Tingkat I'; goladd[6] = "II/b";
    pangkat[7] = 'Pengatur ';               goladd[7] = "II/c";
    pangkat[8] = 'Pengatur Tingkat I';      goladd[8] = "II/d";
    pangkat[9] = 'Penata Muda';             goladd[9] = "III/a";
    pangkat[10] = 'Penata Muda Tingkat I';  goladd[10] = "III/b";
    pangkat[11] = 'Penata ';                goladd[11] = "III/c";
    pangkat[12] = 'Penata Tingkat I';       goladd[12] = "III/d";
    pangkat[13] = 'Pembina';                goladd[13] = "IV/a";
    pangkat[14] = 'Pembina Tingkat I';      goladd[14] = "IV/b";
    pangkat[15] = 'Pembina Utama Muda';     goladd[15] = "IV/c";
    pangkat[16] = 'Pembina Utama Madya';    goladd[16] = "IV/d";
    pangkat[17] = 'Pembina Utama';          goladd[17] = "IV/e";


    function ChoiceEdit(){
        y = document.getElementById("editgolruang");
        document.getElementById("pangkatedit").value = pangkat[y.selectedIndex];
        document.getElementById("goledit").value = goladd[y.selectedIndex]; 
    };
</script>
