<?php echo form_open_multipart('dosen/addbuku');?>
    <fieldset >
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Judul Buku </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" maxlength="200" placeholder="Masukkan Judul Buku Anda" name="judul" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Tahun </label>
            <div class="col-sm-10">
                <input type="text" class="form-control yearpicker"  placeholder="Masukkan Tahun Terbit Buku Anda" name="tahun" maxlength="4" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Jumlah halaman </label>
            <div class="col-sm-10">
                <input type="number" class="form-control" min="1"  placeholder="Masukkan Jumlah Halaman Buku Anda" name="jml_hlmn" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <label class="col-sm-2 control-label">Penerbit </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" maxlength="100" placeholder="Masukkan Penerbit Buku Anda" name="penerbit" required>
            </div>
        </div>
        <div class="form-group" style="padding-bottom: 25px;">
            <div  style="padding-right: 25px">
                <button type="submit" class="btn btn-primary pull-right" style="margin-left: 10px">Simpan</button>
                <button type="reset" class="btn btn-danger pull-right">Reset</button>
            </div>
        </div>
    </fieldset>
<?php echo form_close(); ?>         