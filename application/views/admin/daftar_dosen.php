<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Daftar 
        <small>Akun Dosen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo Site_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar Dosen</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php echo $this->session->flashdata('pesan') ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"> Daftar akun dosen</h3>
        </div>
        <div class="box-body">
            <!-- Trigger the modal with a button -->
            <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-fw fa-plus"></i>Tambah Dosen baru</a>
            <br/>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Tambah Dosen</h4>
                        </div>
                        <div class="modal-body">
                            <?php $this->load->view('admin/tambah_dosen'); ?>  
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <table id="user1" class="table table-bordered table-hover col-xs-pull-right">
                <thead align="center">
                    <tr style="text-align: center">
                        <th>No</th>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; 
                    foreach($data as $d){ ?>
                    <tr>
                        <td align="center"><?php echo $i ?></td>
                        <td><?php echo $d['name'] ?></td>
                        <td><?php echo $d['username'] ?></td>
                        <td align="center"> 
                            <a  class="btn btn-primary btn-sm akundata" data-toggle="modal" data-nama="<?php echo $d['name']?>" data-username="<?php echo $d['username']?>" data-target="#editakun"><i class="fa fa-fw fa-edit"></i>  Ubah</a> 

                            <a  href="" data-url="<?php echo base_url()."admin/resetPass/".$d['username']; ?>" class="btn btn-warning btn-sm confirm_reset"><i class="fa fa-fw fa-undo"></i>  Reset Password</a>

                            <a  href="" data-url="<?php echo base_url()."admin/deleteDosen/".$d['username']; ?>" class="btn btn-danger btn-sm confirm_delete"><i class="fa fa-fw fa-trash"></i>  Hapus</a>  

                            <!-- <td align="center"><a class="btn btn-sm btn-primary akundata" data-toggle="modal" 
                                data-nama="<?php echo $d['name']?>" 
                                data-username="<?php echo $d['username']?>" 
                                data-target="#editakun"><i class="glyphicon glyphicon-pencil"></i>&nbsp&nbsp Edit</a></td> -->
                        </td>
                    </tr>
                    <?php 
                    $i = $i+1; } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
        <div class="box-footer">
            <!-- Footer -->
        </div><!-- /.box-footer-->
    </div><!-- /.box -->
    
</section><!-- /.content -->

<!-- Modal edit data akun -->
    <div class="modal fade" id="editakun" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Ubah Data Akun</h4>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('admin/edit_view'); ?>  
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
