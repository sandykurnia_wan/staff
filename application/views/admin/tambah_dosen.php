<?php echo form_open_multipart('admin/insertDosen');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Username : </label>
        <div class="col-sm-10">
            <input type="email" class="form-control"  placeholder="Masukkan Username Anda" name="username" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Nama : </label>
        <div class="col-sm-10">
            <input type="text" pattern="[^.\A-Za-z]" class="form-control"  placeholder="Masukkan Nama Anda" name="nama" required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <div class="col-sm-11">
            <button type="reset" class="btn btn-danger pull-right">Reset</button>
        </div>
        <div class="col-sm-1">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>&nbsp&nbsp
        </div>
    </div>
</fieldset>
<?php echo form_close(); ?>           