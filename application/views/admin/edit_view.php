<?php echo form_open_multipart('admin/editDosen');?>
<fieldset >
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Username </label>
        <div class="col-sm-10">
            <input type="email" class="form-control" name="username" id="username" readonly required>
        </div>
    </div>
    <div class="form-group" style="padding-bottom: 25px;">
        <label class="col-sm-2 control-label">Nama </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" id="nama" required>
        </div>
    </div>
    <div class="form-group" style="padding-right: 20px;">
        <button type="submit" class="btn btn-primary pull-right"> <i class="fa fa-fw fa-save"></i>  Simpan</button>
    </div>
</fieldset>
<?php echo form_close(); ?> 