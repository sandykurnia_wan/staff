<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->session->flashdata('pesan') ?>
    <h1>
        Dashboard
        <small>Admin</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Selamat datang Admin, pada Staff Site Informatika Universitas Diponegoro</h3>
        </div>
        <!-- <div class="box-body"> -->
            
        <!-- </div>/.box-body -->
        <!-- <div class="box-footer"> -->
            
        <!-- </div> /.box-footer -->
    </div><!-- /.box -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlDosen?></h3>
                <p>Jumlah Akun Dosen</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

<!--     <div class="col-lg-3 col-xs-6">
        small box
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlOrg?></h3>
                <p>Jumlah Kegiatan Organisasi</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php //echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        small box
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlPenulis?></h3>
                <p>Jumlah Kegiatan Penulisan Buku</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php //echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        small box
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlReviewer?></h3>
                <p>Jumlah Kegiatan Reviewer</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php //echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        small box
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlNarasumber?></h3>
                <p>Jumlah Kegiatan Narasumber</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php //echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        small box
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $jmlLeader?></h3>
                <p>Jumlah Kegiatan Kepemimpinan</p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a href="<?php //echo site_url('admin/lihatdosen') ?>" class="small-box-footer">Informasi detail <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div> -->


</section><!-- /.content -->
