<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

    <head>

        <title>Profil Lengkap <?php echo $pribadi[0]['nm_dosen']?> | Staff Site Informatika Undip</title>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/logo undip.png')?>"/>
        <!-- Basic Page Needs -->
        <meta charset="UTF-8" />
        <meta name="description" content="FlexyCodes - MZCard HTML5 Responsive vCard Template."/>
        <meta name="keywords" content="">
        <meta name="author" content="flexycodes">
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <!-- icon sosmed -->
        <link  rel="stylesheet" type="text/css" href="<?php echo base_url('assets/icomoon/style.css') ?>" />

        <!-- CSS | bootstrap -->
        <!-- Credits: http://getbootstrap.com/ -->
        <link  rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/css/bootstrap.min.css') ?>" />

        <!-- CSS | font-awesome -->
        <!-- Credits: http://fortawesome.github.io/Font-Awesome/icons/ -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/css/font-awesome.min.css') ?>" />

        <!-- CSS | animate -->
        <!-- http://daneden.github.io/animate.css/ -->
        <link rel="stylesheet" href="<?php echo base_url('assets/tema/css/animate.min.css') ?>">

        <!-- CSS | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/css/prettyPhoto.css') ?>"/>

        <!-- CSS | colors -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/css/colors/color_15.css') ?>" id="colors-style" />

        <!-- CSS | Style -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/tema/css/main.css') ?>">
			
        <!-- Js | modernizr.js -->
        <!-- Credits: http://modernizr.com -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/modernizr.custom.js') ?>"></script>


        <!-- CSS | Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <!-- Favicon -->
        <!-- <link rel="shortcut icon" type="image/x-icon" href="<?php //echo base_url('assets/tema/images/favicon/favicon.ico') ?>"> -->

        <!--[if IE 7]>
			<link rel="stylesheet" type="text/css" href="css/icons/font-awesome-ie7.min.css"/>
		<![endif]-->
	
	<style>
		.my_profile .image_profile {
			background-image: url(<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>);
			background-size: cover;
			background-position: 50% 50%;
			background-repeat: no-repeat;
			position: absolute;
			left: 0;
			top: 0;
			height: 100%;
		}
		.my_profile .image_profile:hover {
			background-image: url(<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>);
			background-size: cover;
			background-position: 50% 50%;
			background-repeat: no-repeat;
			-moz-transition: all 0.3s ease-in-out;
			-o-transition: all 0.3s ease-in-out;
			-webkit-transition: all 0.3s ease-in-out;
			transition: all 0.3s ease-in-out; 
			position: absolute;
			left: 0;
			top: 0;
			height: 100%;
		}
        .profpic{
            object-fit: cover;
            /*width: 150px;*/
            height: 200px;
        }

		#mapGoogle {
		    height: 100%;
		}
		html, body {
		    height: 100%;
		    margin: 0;
		    padding: 0;
		}	

        #detailtable {
            padding: 15px;
        }

	</style>
	
    </head>

    <body>

        <!--[if lt IE 7]>
                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Laoding page  -->
        <div class="preloader">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
        <!-- End Laoding page  -->

        <!-- #wrappers -->
        <div id="wrappers">

            <!-- #header -->
            <header id="header">

                <div class="menu_closed" id="menu_closed" title="Close Menu" data-toggle="tooltip">
                    <i class="fa fa-times"></i>
                </div>

                <div class="logo profpic">
                    <!--Profil foto-->
                    <a href="#"><img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>"  width="122" height="122" alt="Logo"></a>
                </div>
                <!-- nama dosen -->
                <h4 class="tagline"><?php echo $pribadi[0]['nm_dosen']?></h4>


                <!-- .navigation -->	
                <nav class="navigation">
                    <ul class="list-unstyled">

                        <li class="home active">
                            <a href="index.php#home" class="home link_menu"><i class="fa fa-home icon_menus"></i><span class="nav-label">home</span></a>
                        </li>
                        <li class="profile">
                            <a href="index.php#profile" class="profile link_menu"><i class="fa fa-user icon_menus"></i><span class="nav-label">profile</span></a>
                        </li>
                        <li class="service">
                            <a href="index.php#service" class="service link_menu"><i class="fa fa-cog icon_menus"></i> <span class="nav-label">education</span></a>
                        </li>
                        <li class="resume">
                            <a href="index.php#resume" class="resume link_menu"><i class="fa fa-tasks icon_menus"></i> <span class="nav-label">achievement</span></a>
                        </li>
                        <li class="portfolio">
                            <a href="index.php#portfolio" class="portfolio link_menu"><i class="fa fa-briefcase icon_menus"></i> <span class="nav-label">activity</span></a>
                        </li>
                        <li class="contact">
                            <a href="index.php#contact" class="contact link_menu"><i class="fa fa-map-marker icon_menus"></i> <span class="nav-label">Location</span></a>
                        </li>

                    </ul>
                </nav>
                <!-- End .navigation -->	

                <!-- #header_social_ul -->	
                <div class="social-ul" id="header_social_ul">
                    <ul>
                        <!-- scopus -->
                        <li class="social-twitter"><a href="https://www.scopus.com/authid/detail.uri?authorId=<?php echo $pribadi[0]['scopus_id']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Scopus"><i class="medsos-scopus"></i></a></li>
                        <!-- rgate -->
                        <li class="social-facebook"><a href="https://www.researchgate.net/profile/<?php echo $pribadi[0]['rgate']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Research Gate"><i class="medsos-rgate"></i></a></li>
                        <!-- scholar -->
                        <li class="social-google"><a href="<?php echo $pribadi[0]['scholar']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Google Scholar"><i class="medsos-scholar"></i></a></li>
                        <!-- sinta -->
                        <li class="social-linkedin"><a href="<?php echo $pribadi[0]['sinta']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Sinta"><i class="medsos-sinta"></i></a></li>
                    </ul>
                </div>
                <!-- End #header_social_ul -->	

                <!-- .copyright -->	
                <div class="copyright">
                    <p style="margin-bottom:0;">© 2017. <a href="#">Informatika Undip</a></p>		
                </div>
                <!-- End.copyright -->	

            </header>
            <!-- End #header -->

            <!-- .header-main -->
            <div class="header-main">
                <div class="content_wrappers">		
                    <div class="logo_wrapper">

                        <span class="site_title">
                            <a href="" title="Throne">
                                <!-- <img src="images/logo-top.png" alt="FlexyBlog"> -->
                            </a>
                        </span>

                    </div>

                    <a class="nav-btn menu_close" id="btn_open_menu" href="#"><i class="fa fa-bars"></i></a>

                </div>

            </div>
            <!-- End .header-main -->

            <!-- #main -->
            <div id="main">

                <!-- #home -->
                <section id="home" class="layers page-active page_current">	

                    <h2 style="display:none">home</h2>

                    <!-- .page_content -->
                    <div class="page_content">
                        <div class="header">
                        	<!-- HOME -->
                            <article class="home-page" id="home_style">
                                <div class="row">
                                    <div class="col-md-12 home-profile">
                                        <div class="home-profile-image"> 
                                            <!-- foto lagi -->
                                            <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" class="profpic" alt=""> 
                                        </div>
                                    </div><div class="col-md-12 home-details">
                                        <h2 class="name_users"><span class="span_color"><?php echo $pribadi[0]['nm_dosen']?></span></h2>
                                        <h3>Hello. I am<span class="rw-words rw-words-1">
                                                <span>Lecture</span>
                                                <span>Researcher</span>
                                                <span>Lecture</span>
                                                <span>Researcher</span>
                                                <span>Lecture</span>
                                                <span>Researcher</span>
                                            </span></h3>
                                        <hr>
                                        <p class="medium"><i>Gedung E Lantai 3 Fakultas Sains dan Matematika <br>
                                                Jalan Prof. Soedarto,SH Tembalang<br>Semarang, 50275</i> </p>
                                        <ul class="social">
                                            <li><a href="https://www.scopus.com/authid/detail.uri?authorId=<?php echo $pribadi[0]['scopus_id']?>"><i class="medsos-scopus"></i></a></li>
                                            <li><a href="https://www.researchgate.net/profile/<?php echo $pribadi[0]['rgate']?>"><i class="medsos-rgate"></i></a></li>
                                            <li><a href="<?php echo $pribadi[0]['scholar']?>"><i class="medsos-scholar"></i></a></li>
                                            <li><a href="<?php echo $pribadi[0]['sinta']?>"><i class="medsos-sinta"></i></a></li>
                                        </ul>
                                    </div>


                                </div>
                            </article>
                        </div>
                    </div>
                    <!-- .page_content -->
                </section>
                <!-- End #home -->

                <!-- #profile -->
                <section id="profile" class="layers">
                    <h2 style="display:none">profile</h2>
                    <!-- .page_content -->
                    <div class="page_content">
                        <!-- .block-header -->
                        <div class="block-header">
                            <!-- .my_profile -->
                            <div class="row no-marg my_profile">
                                <div class="col-xs-12 col-md-6 no-padd image_profile">
                                    <!--<div class="overfly"></div>-->	
                                </div>
                                <div class="col-xs-12 col-md-6 no-padd image_profile_resp">
                                    <div class="home_profile_resp"> 
                                        <img src="<?php echo base_url('uploads/profilePhoto/'.$profpic) ?>" alt=""> 
                                    </div>
                                </div>
                                <!-- .info_profile -->
                                <div class="col-xs-12 col-md-6 no-padd info_profile">
                                    <!-- .bg_desc -->
                                    <div class="bg_desc clearfix" style="height: auto; min-height: 400px">
                                        <div class="inner-text">
                                            <h2>I am <?php echo $pribadi[0]['nm_dosen']?></h2>
                                            <small class="myjob">Lecturer</small>
                                            <div class="feature-desc">
                                                <?php 
                                                    if(!empty($pribadi[0]['deskripsi'])){
                                                ?>
                                                <p><?php echo $pribadi[0]['deskripsi']?></p>
                                                <?php
                                                    } else {
                                                ?>
                                                <ul>
                                                    <li style="color: #000" class="user_name"><i class="fa fa-user"></i><strong>Nama</strong>: <?php echo $pribadi[0]['glr_dpn'].' '.$pribadi[0]['nm_dosen']; if(!empty($pribadi[0]['glr_blkg'])) echo ', '.$pribadi[0]['glr_blkg']?></li>
                                                    <li style="color: #000"><i class="fa fa-credit-card"></i><strong>NIP</strong>: <?php 
                                                    if (empty($pribadi[0]['nip'])){
                                                            echo 'Data belum ada';
                                                        } else {
                                                            echo $pribadi[0]['nip'];
                                                        }?></li>
                                                    <?php
                                                        setlocale (LC_ALL, 'IND');
                                                        $date=date_create($pribadi[0]['tgl_lahir']);
                                                        $bulan = strftime("%B", $date->getTimestamp());
                                                        $tanggal = strftime("%d", $date->getTimestamp());
                                                        $tahun = strftime("%Y", $date->getTimestamp());
                                                        
                                                    ?>
                                                    <li style="color: #000"><i class="fa fa-calendar"></i><strong>TTL</strong>: <?php 
                                                            if ($pribadi[0]['tgl_lahir'] == '0000-00-00') {
                                                                echo 'Data belum ada';
                                                            }else{
                                                                echo $pribadi[0]['tmpt_lahir'].', '.$tanggal.' '.$bulan.' '.$tahun;
                                                            }
                                                        ?>
                                                        </li>
                                                    <li style="color: #000"><i class="fa fa-envelope"></i><strong>Email</strong>: <?php echo $pribadi[0]['username']?></li style="color: #000">
                                                    <li style="color: #000"><i class="fa fa-sitemap"></i><strong>Jab. Fungsional </strong>: <?php 
                                                    if (empty($fungakhir[0]['jab_fung'])){
                                                            echo 'Data belum ada';
                                                        } else {
                                                            echo $fungakhir[0]['jab_fung'];
                                                        }?></li>
                                                    <li style="color: #000"><i class="fa fa-comment"></i><strong>Research Interest</strong>: <?php 
                                                    if (empty($pribadi[0]['interest'])){
                                                            echo 'Data belum ada';
                                                        } else {
                                                            echo $pribadi[0]['interest'];
                                                        }?></li>
                                                </ul>
                                                <?php
                                                    }
                                                ?>
                                                
                                                
                                            </div> 
                                            <div class="signature text-right">
                                                <div class="inner">
                                                    <!-- <img src="images/signature.png" alt="Signature"> -->
                                                    <cite class="name"><?php echo $pribadi[0]['nm_dosen']?></cite>
                                                    <?php 
                                                        if(empty($pribadi[0]['deskripsi'])){
                                                    ?>
                                                     <br><br><br><br><br><br><br><br><br><br><br><br>
                                                    <?php
                                                        }
                                                    ?>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End .bg_desc -->
                                    <!-- .bg_info -->
                                    <div class="bg_info clearfix">
                                        <?php 
                                            if(empty($pribadi[0]['deskripsi'])){
                                        ?>

                                        <?php
                                            }else{
                                        ?>
                                        <div class="inner-text">
                                            <ul>
                                                <li class="user_name"><i class="fa fa-user"></i><strong>Nama</strong>: <?php echo $pribadi[0]['nm_dosen']?></li>
                                                <li><i class="fa fa-calendar"></i><strong>NIP</strong>: <?php echo $pribadi[0]['nip']?></li>
                                                <?php
                                                    setlocale (LC_ALL, 'IND');
                                                    $date=date_create($pribadi[0]['tgl_lahir']);
                                                    $bulan = strftime("%B", $date->getTimestamp());
                                                    $tanggal = strftime("%d", $date->getTimestamp());
                                                    $tahun = strftime("%Y", $date->getTimestamp());
                                                    
                                                ?>
                                                <li><i class="fa fa-calendar"></i><strong>TTL</strong>: <?php echo $pribadi[0]['tmpt_lahir']?>, <?php echo $tanggal.' '.$bulan.' '.$tahun;?></li>
                                                <li><i class="fa fa-envelope"></i><strong>Email</strong>: <?php echo $pribadi[0]['username']?></li>
                                                <li><i class="fa fa-skype"></i><strong>Jab. Fungsional </strong>: <?php echo $fungakhir[0]['jab_fung']?></li>
                                                <li><i class="fa fa-envelope"></i><strong>Research Interest</strong>: <?php echo $pribadi[0]['interest']?></li>
                                            </ul>
                                        </div>
                                        <?php
                                            }
                                        ?>
                                        
                                    </div>
                                    <!-- End .bg_info -->
                                    <!-- .header_social_ul -->
                                    <div class="social-ul bg_social" id="profile_social_ul">
                                        <ul>
                                            <li class="social-twitter"><a href="https://www.scopus.com/authid/detail.uri?authorId=<?php echo $pribadi[0]['scopus_id']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Scopus"><i class="medsos-scopus"></i></a></li>
                                            <li class="social-facebook"><a href="https://www.researchgate.net/profile/<?php echo $pribadi[0]['rgate']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Research Gate"><i class="medsos-rgate"></i></a></li>
                                            <li class="social-google"><a href="<?php echo $pribadi[0]['scholar']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Google Scholar"><i class="medsos-scholar"></i></a></li>
                                            <li class="social-linkedin"><a href="<?php echo $pribadi[0]['sinta']?>" target="_blank" title="" data-toggle="tooltip" data-original-title="Sinta"><i class="medsos-sinta"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- End .header_social_ul -->
                                </div>
                                <!-- End .info_profile -->
                            </div>
                            <!-- End .my_profile -->
                        </div>
                        <!-- End .block-header -->
                    </div>
                    <!-- End .page_content -->
                </section>
                <!-- End #about -->

               <!-- #service -->
               <!-- EDUCATION -->
				<section id="service" class="layers">
					<h2 style="display:none">service</h2>
					
					<!-- .page_content -->
					<div class="page_content">
						
						<!-- .container-fluid -->
						<div class="container-fluid no-marg">
						
							<!-- .row -->
							<div class="row row_responsive">
								
								<!-- .section_general -->
								<div class="col-lg-11 section_general">
						
									<header class="section-header">
										<h3 class="section-title">STAFF SITE - Riwayat Pendidikan</h3>
										<div class="border-divider"></div>
									</header>
									
									<!-- service-5 -->
									<div class="row section_separate"> 
									
										<div class="title-section">
											<h2 class="section_title">Pendidikan Formal</h2>
											<div class="sep2"></div>
										</div>

                                        <div class="col-md-4 col-sm-6">
                                            <?php if(!empty($formal[0])) {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Sarjana <span>STRATA - 1 </span></h3>
                                                    <h4 style="line-height: 35%"><i><?php echo $formal[0]['bidang']?></i></h4> 
                                                    <h4><span><?php echo $formal[0]['nama_univ']?></span></h4>
                                                </div>
                                                <ul class="pricing-content list-unstyled">
                                                    <li><i class="fa fa-calendar"></i> Tahun Masuk : <?php echo $formal[0]['thn_masuk']?></li>
                                                    <li><i class="fa fa-calendar"></i> Tahun Lulus : <?php echo $formal[0]['thn_lulus']?></li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 1 : <?php echo $formal[0]['dosbing1']?> </li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 2 : <?php echo $formal[0]['dosbing2']?></li>
                                                </ul>
                                                <div class="pricing-footer">
                                                    <p style="text-align: left">Judul Tugas Akhir : </p>
                                                    <p style="text-align: justify"> <?php echo $formal[0]['judul_ta']?></p>
                                                </div>                    
                                            </div>
                                            <?php } else {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Sarjana <span>STRATA - 1 </span></h3>
                                                    <h4 style="line-height: 35%"><i>(Data Belum Ada)</h4>
                                                </div>                    
                                            </div>
                                            <?php }?>
                                        </div>

                                        <div class="col-md-4 col-sm-6">
                                            <?php if(!empty($formal[1])) {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Magister <span>STRATA - 2 </span></h3>
                                                    <h4 style="line-height: 35%"><i><?php echo $formal[1]['bidang']?></i></h4> 
                                                    <h4><span><?php echo $formal[1]['nama_univ']?></span></h4>
                                                </div>
                                                <ul class="pricing-content list-unstyled">
                                                    <li><i class="fa fa-calendar"></i> Tahun Masuk : <?php echo $formal[1]['thn_masuk']?></li>
                                                    <li><i class="fa fa-calendar"></i> Tahun Lulus : <?php echo $formal[1]['thn_lulus']?></li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 1 : <?php echo $formal[1]['dosbing1']?> </li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 2 : <?php echo $formal[1]['dosbing2']?></li>
                                                </ul>
                                                <div class="pricing-footer">
                                                    <p style="text-align: left">Judul Tugas Akhir : </p>
                                                    <p style="text-align: justify"> <?php echo $formal[1]['judul_ta']?></p>
                                                </div>                    
                                            </div>
                                            <?php } else {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Magister <span>STRATA - 2 </span></h3>
                                                    <h4 style="line-height: 35%"><i>(Data Belum Ada)</h4>
                                                </div>                    
                                            </div>
                                            <?php }?>
                                        </div>

                                        <div class="col-md-4 col-sm-6">
                                            <?php if(!empty($formal[2])) {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Doktor <span>STRATA - 3 </span></h3>
                                                    <h4 style="line-height: 35%"><i><?php echo $formal[2]['bidang']?></i></h4> 
                                                    <h4><span><?php echo $formal[2]['nama_univ']?></span></h4>
                                                </div>
                                                <ul class="pricing-content list-unstyled">
                                                    <li><i class="fa fa-calendar"></i> Tahun Masuk : <?php echo $formal[2]['thn_masuk']?></li>
                                                    <li><i class="fa fa-calendar"></i> Tahun Lulus : <?php echo $formal[2]['thn_lulus']?></li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 1 : <?php echo $formal[2]['dosbing1']?> </li>
                                                    <li><i class="fa fa-users"></i> Dosen Pembimbing 2 : <?php echo $formal[2]['dosbing2']?></li>
                                                </ul>
                                                <div class="pricing-footer">
                                                    <p style="text-align: left">Judul Tugas Akhir : </p>
                                                    <p style="text-align: justify"> <?php echo $formal[2]['judul_ta']?></p>
                                                </div>                    
                                            </div>
                                            <?php } else {?>
                                            <div class="pricing">
                                                <div class="pricing-head">
                                                    <h3>Pendidikan Doktor <span>STRATA - 3 </span></h3>
                                                    <h4 style="line-height: 35%"><i>(Data Belum Ada)</h4>
                                                </div>                    
                                            </div>
                                            <?php }?>
                                        </div>
    							</div>
								<!-- End .section_general -->

                                <div class="row section_separate">
                                    
                                    <div class="title-section">
                                        <h2 class="section_title">Pendidikan Non Formal</h2>
                                        <div class="sep2"></div>
                                    </div>
                                    
                                    <div>
                                        <table class="table table-bordered table-striped" border="1">
                                            <tr>
                                                <th style="text-align: center">Nama Kegiatan</th>
                                                <th style="text-align: center">Tanggal Pelaksanaan</th>
                                                <th style="text-align: center">Tempat Pelaksanaan</th>
                                                <th style="text-align: center">Penyelenggara</th>
                                            </tr>
                                            <tr>
                                                <?php if(!empty($non_formal)) {?>
                                                    <?php foreach($non_formal as $non){ ?>
                                                        <tr>
                                                            <td><?php echo $non['nama_kegiatan'] ?></td>
                                                            <?php
                                                                setlocale (LC_ALL, 'IND');
                                                                $date=date_create($non['tgl_kegiatan']);
                                                                $bulan = strftime("%B", $date->getTimestamp());
                                                                $tanggal = strftime("%d", $date->getTimestamp());
                                                                $tahun = strftime("%Y", $date->getTimestamp());
                                                            ?>
                                                            <td><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                            <td><?php echo $non['tempat'] ?></td>
                                                            <td><?php echo $non['penyelenggara'] ?></td>
                                                        </tr>
                                                    <?php }?>
                                                <?php } else {?>
                                                    <tr>
                                                        <td colspan="4" style="text-align: center">
                                                            <?php echo "(Data Belum Ada)" ?>
                                                        </td>
                                                    </tr>
                                                <?php }?>
                                            </tr>
                                        </table>
                                    </div>

                                </div>
								
								
							</div>
							<!-- End .row -->
							
						</div>
						<!-- End .container-fluid -->
						
					</div>
					<!-- End .page_content -->
				</section>
				<!-- End #service -->


                <!-- ACHIEVEMENT -->
				<section id="resume" class="layers">
					<h2 style="display:none">resume</h2>

					<!-- page_content -->
					<div class="page_content">
						
						<!-- .container-fluid -->
						<div class="container-fluid no-marg">
						
							<!-- .row -->
							<div class="row row_responsive">
								
								<!-- .section_general -->
								<div class="col-lg-11 section_general">
								
									<header class="section-header">
										<h3 class="section-title">STAFF SITE – Achievement</h3>
										<div class="border-divider"></div>
									</header>
									
                                    <div class="col-md-12">

                                            <div role="tabpanel">

                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#hki" aria-controls="desktop" role="tab" data-toggle="tab">HKI</a></li>
                                                    <li role="presentation"><a href="#jurnal" aria-controls="mobile" role="tab" data-toggle="tab">Jurnal</a></li>
                                                    <li role="presentation"><a href="#prosiding" aria-controls="users" role="tab" data-toggle="tab">Prosiding</a></li>
                                                    <li role="presentation"><a href="#penelitian" aria-controls="heart" role="tab" data-toggle="tab">Penelitian</a></li>
                                                    <li role="presentation"><a href="#pengabdian" aria-controls="users" role="tab" data-toggle="tab">Pengabdian</a></li>
                                                    <li role="presentation"><a href="#penghargaan" aria-controls="users" role="tab" data-toggle="tab">Penghargaan</a></li>
                                                </ul>

                                                <!-- Tab panes -->
                                                <div class="tab-content">

                                                    <div role="tabpanel" class="tab-pane fade active in" id="hki">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_hki[2];?>%">
                                                            <div class="skillbar-title"><span>HKI</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_hki[0];?>/<?php echo $persen_hki[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th style="text-align: center">Judul/ Tema HKI</th>
                                                                <th style="text-align: center">Tahun</th>
                                                                <th style="text-align: center">Jenis</th>
                                                                <th style="text-align: center">Nomor P/ID</th>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($hki)) {?>
                                                                    <?php foreach($hki as $h){ ?>
                                                                        <tr>
                                                                            <td><?php echo $h['judul_hki'] ?></td>
                                                                            <td><?php echo $h['tahun'] ?></td>
                                                                            <td><?php echo $h['jenis_hki'] ?></td>
                                                                            <td><?php echo $h['no_hki'] ?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="jurnal">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_jurnal[2];?>%">
                                                            <div class="skillbar-title"><span>Jurnal</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_jurnal[0];?>/<?php echo $persen_jurnal[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th style="text-align: center">Judul Jurnal</th>
                                                                <th style="text-align: center">Tahun</th>
                                                                <th style="text-align: center">Volume</th>
                                                                <th style="text-align: center">No Jurnal</th>
                                                                <th style="text-align: center">Tingkat</th>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($jurnal)) {?>    
                                                                    <?php foreach($jurnal as $j){ ?>
                                                                        <tr>
                                                                            <td><?php echo $j['judul_jurnal']?></td>
                                                                            <td><?php echo $j['tahun']?></td>
                                                                            <td><?php echo $j['volume']?></td>
                                                                            <td><?php echo $j['no_jurnal']?></td>
                                                                            <td><?php echo $j['tingkat']?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="5" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="prosiding">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_prosiding[2];?>%">
                                                            <div class="skillbar-title"><span>Prosiding</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_prosiding[0];?>/<?php echo $persen_prosiding[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th style="text-align: center">Nama Prosiding</th>
                                                                <th style="text-align: center">Judul Artikel</th>
                                                                <th style="text-align: center">Tempat</th>
                                                                <th style="text-align: center">Tanggal</th>
                                                                <th style="text-align: center">Penulis Ke-</th>
                                                                <th style="text-align: center">URL</th>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($prosiding)) {?>
                                                                    <?php foreach($prosiding as $pr){ ?>
                                                                        <tr>
                                                                            <td><?php echo $pr['nama_prosiding']?></td>
                                                                            <td><?php echo $pr['judul_artikel']?></td>
                                                                            <td><?php echo $pr['tempat']?></td>
                                                                            <?php
                                                                                setlocale (LC_ALL, 'IND');
                                                                                $date=date_create($pr['tanggal1']);
                                                                                $bulan = strftime("%B", $date->getTimestamp());
                                                                                $tanggal = strftime("%d", $date->getTimestamp());
                                                                                $tahun = strftime("%Y", $date->getTimestamp());
                                                                            ?>
                                                                            <td><?php echo $tanggal.' '.$bulan.' '.$tahun;?></td>
                                                                            <td><?php echo $pr['penulis_ke']?></td>
                                                                            <td><?php echo $pr['url_prosiding']?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="6" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="penelitian">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_penelitian[2];?>%">
                                                            <div class="skillbar-title"><span>Penelitian</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_penelitian[0];?>/<?php echo $persen_penelitian[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th rowspan="2" style="text-align: center">Tahun</th>
                                                                <th rowspan="2" style="text-align: center">Judul Penelitian</th>
                                                                <th colspan="2" style="text-align: center">Pendanaan</th>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: center">Sumber Dana</td>
                                                                <td style="text-align: center">Jumlah Dana</td>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($penelitian)) {?>
                                                                    <?php foreach($penelitian as $pl){ ?>
                                                                        <tr>
                                                                            <td><?php echo $pl['tahun'] ?></td>
                                                                            <td><?php echo $pl['judul_penelitian'] ?></td>
                                                                            <td><?php echo $pl['sumber_dana'] ?></td>
                                                                            <td><?php echo $pl['jumlah_dana'] ?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="pengabdian">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_pengabdian[2];?>%">
                                                            <div class="skillbar-title"><span>Pengabdian</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_pengabdian[0];?>/<?php echo $persen_pengabdian[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th rowspan="2" style="text-align: center">Tahun</th>
                                                                <th rowspan="2" style="text-align: center">Judul Pengabdian</th>
                                                                <th colspan="2" style="text-align: center">Pendanaan</th>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: center">Sumber Dana</td>
                                                                <td style="text-align: center">Jumlah Dana</td>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($pengabdian)) {?>    
                                                                    <?php foreach($pengabdian as $pd){ ?>
                                                                        <tr>
                                                                            <td><?php echo $pd['tahun'] ?></td>
                                                                            <td><?php echo $pd['judul_pengabdian'] ?></td>
                                                                            <td><?php echo $pd['sumber_dana'] ?></td>
                                                                            <td><?php echo $pd['jumlah_dana'] ?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane fade" id="penghargaan">
                                                        <div class="skillbar clearfix" data-percent="<?php echo $persen_penghargaan[2];?>%">
                                                            <div class="skillbar-title"><span>Penghargaan</span></div>
                                                            <div class="skillbar-bar"></div>
                                                            <div class="skill-bar-percent">
                                                                <?php echo $persen_penghargaan[0];?>/<?php echo $persen_penghargaan[1];?>
                                                            </div>
                                                        </div>
                                                        <div>
                                                        <table class="table table-bordered table-striped" border="1">
                                                            <tr>
                                                                <th style="text-align: center">Nama Penghargaan</th>
                                                                <th style="text-align: center">Institusi Pemberi Penghargaan</th>
                                                                <th style="text-align: center">Tahun</th>
                                                                <th style="text-align: center">Tingkat</th>
                                                            </tr>
                                                            <tr>
                                                                <?php if(!empty($penghargaan)) {?>
                                                                    <?php foreach($penghargaan as $ph){ ?>
                                                                        <tr>
                                                                            <td><?php echo $ph['nama_penghargaan'] ?></td>
                                                                            <td><?php echo $ph['pemberi'] ?></td>
                                                                            <td><?php echo $ph['tahun'] ?></td>
                                                                            <td><?php echo $ph['tingkat'] ?></td>
                                                                        </tr>
                                                                    <?php }?>
                                                                <?php } else {?>
                                                                    <tr>
                                                                        <td colspan="4" style="text-align: center"><?php echo "(Data Belum Ada)" ?></td>
                                                                    </tr>
                                                                <?php }?>
                                                            </tr>
                                                        </table>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                </div>
                                
								
                            </div>
							<!-- End .row -->
						</div>
						<!-- End .container-fluid -->

						

					</div>
					<!-- End page_content -->
				</section>
				<!-- End #resume -->

                <!-- #portfolio -->
                <!-- ACTIVITY -->
                <section id="portfolio" class="layers">
                    <h2 style="display:none">portfolio</h2>
                    <!-- .page_content -->
                    <div class="page_content">
                        <!-- .portfolios -->
                        
                            <?php 
                                if(!empty($org || $buku || $review || $narasumber || $leader)){
                            ?>
                            <div id="portfolios" class="portfolios">
                                <!-- .grid-wrap -->
                                <div class="grid-wrap">

                                    <!-- #filters -->
                                    <ul id="genre-filter" class="clearfix">
                                        <li class="label_filter">Filter</li>
                                        <li><span class="filter active" data-filter="org buku review nara leader">All</span></li>
                                        <li><span class="filter" data-filter="org">Organisasi</span></li>
                                        <li><span class="filter" data-filter="buku">Buku</span></li>
                                        <li><span class="filter" data-filter="review">Reviewer</span></li>
                                        <li><span class="filter" data-filter="nara">Narasumber</span></li>
                                        <li><span class="filter" data-filter="leader">Leadership</span></li>
                                    </ul>
                                    <!-- /#filters -->
                                    <!-- #portfolioslist -->    
                                    <div class="grid" id="portfoliolist">
                                        <!-- org putih -->
                                        <?php
                                            $i=1;
                                            foreach ($org as $o) {
                                        ?>
                                        <div class="view view-first portfolio org" data-cat="org">
                                            <img src="<?php echo base_url('assets/img/iconAct/'.($i%2==0 ? 'orggray.png' : 'orgblack.png'))?>" alt="img02"/>
                                            <div class="mask" style="background-color: rgba(157, 53, 33, 0.7);">
                                                <h4>Pengalaman Organisasi</h4>
                                                <p><span class="cat-portfolio" style="font-size: 20px"><?php echo $o['judul']?></span></p>
                                                <div class="portf_detail">
                                                    <a href="#" class="info external open_port_detail" data-id="<?php echo $i;?>">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>          
                                        </div>
                                        <?php
                                            $i=$i+1;
                                            }
                                        ?>
                                        <?php
                                        
                                         foreach ($buku as $b) {
                                        ?>

                                        <div class="view view-first portfolio buku" data-cat="buku">

                                            <img src="<?php echo base_url('assets/img/iconAct/'.($i%2==0 ? 'bukugray.png' : 'bukublack.png'))?>" alt="img02"/>

                                            <div class="mask" style="background-color: rgba(0, 195, 255, 0.7);">
                                                <h4>Penulisan Buku</h4>
                                                <p><span class="cat-portfolio" style="font-size: 20px"><?php echo $b['judul_buku']?></span></p>
                                                <div class="portf_detail">
                                                    <a href="#" class="info external open_port_detail" data-id="<?php echo $i;?>">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>          
                                        </div>
                                        <?php
                                            $i=$i+1;
                                        }
                                        ?>

                                        <?php
                                            foreach ($review as $r) {
                                        ?>
                                        <!-- review item -->
                                        <div class="view view-first portfolio review" data-cat="review">
                                            <img src="<?php echo base_url('assets/img/iconAct/'.($i%2==0 ? 'revgray.png' : 'revblack.png'))?>" alt="img02"/>
                                            <div class="mask" style="background-color: rgba(141, 207, 97, 0.7);">
                                                <h4>Pengalaman Reviewer</h4>
                                                <p><span class="cat-portfolio" style="font-size: 20px"><?php echo $r['judul']?> </span></p>
                                                <div class="portf_detail">
                                                   <a href="#" class="info external open_port_detail" data-id="<?php echo $i;?>">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                            $i=$i+1;
                                            }
                                        ?>
                                        
                                        <?php 
                                            foreach ($narasumber as $n) {
                                        ?>
                                        <div class="view view-first portfolio nara" data-cat="nara">
                                            <img src="<?php echo base_url('assets/img/iconAct/'.($i%2==0 ? 'naragray.png' : 'narablack.png'))?>" alt="img02"/>
                                            <div class="mask" style="background-color: rgba(241, 196, 15, 0.7);">
                                                <h4>Pengalaman Narasumber</h4>
                                                <p><span class="cat-portfolio" style="font-size: 20px"> <?php echo $n['judul']?></span></p>
                                                <div class="portf_detail">
                                                    <a href="#" class="info external open_port_detail" data-id="<?php echo $i;?>">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>          
                                        </div>
                                        <?php
                                            $i = $i+1;
                                            }
                                        ?>

                                        <?php 
                                            foreach ($leader as $l) {
                                        ?>
                                        <div class="view view-first portfolio leader" data-cat="leader">
                                            <img src="<?php echo base_url('assets/img/iconAct/'.($i%2==0 ? 'leadergray.png' : 'leaderblack.png'))?>" alt="img02"/>
                                            <div class="mask" style="background-color: rgba(55, 0, 255, 0.7);">
                                                <h4>Pengalaman Kepemimpinan</h4>
                                                <p><span class="cat-portfolio" style="font-size: 20px"> <?php echo $l['jabatan']?></span></p>
                                                <div class="portf_detail">
                                                    <a href="#" class="info external open_port_detail" data-id="<?php echo $i;?>">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>          
                                        </div>
                                        <?php 
                                            $i=$i+1;
                                        }
                                        $max = $i-1;
                                        ?>
                                    </div><!-- /portfoliolist -->   
                                </div><!-- /grid-wrap -->
                            </div><!-- /portfolios -->  
                            <?php
                                } else {
                            ?>
                            <!-- .container-fluid -->
                        <div class="container-fluid no-marg">
                        
                            <!-- .row -->
                            <div class="row row_responsive">
                                
                                <!-- .section_general -->
                                <div class="col-lg-11 section_general">
                                
                                    <header class="section-header">
                                        <h3 class="section-title">STAFF SITE – Activity</h3>
                                        <div class="border-divider"></div>
                                        <h4>Data Belum Ada</h4>
                                    </header>

                                    <div class="col-md-12">
                                        </div>
                                </div>
                            </div>
                            <!-- End .row -->
                        </div>
                        <!-- End .container-fluid -->
                            <?php
                                } 
                            ?>
                            

                        

                    </div>
                    <!-- .page_content -->
                </section>
                <!-- End #portfolio -->

                <!-- #portfolio_single -->
                <!-- DETAIL ACTIVITY -->
                <div class="overlay_single_portfolio active">

                    <?php 
                    $i = 1;
                    	foreach ($org as $o) {
                    ?>
                    <div class="single-portfolio" data-id="<?php echo $i;?>">
                        <div class="fc-page-title-breadcrumbs">
                            <div class="fc-page-title">
                                <div class="fc-page-title-wrapper">
                                    <div class="fc-page-title-captions">
                                        <h1 class="entry-title">Activity <?php echo $i;?></h1>
                                    </div>
                                    <a class="btn_close_port" href="#"><i class="fa fa-close"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="row no-marg">
                            <div class="col-md-12 post-image-wrap">
                                <!-- <img src="http://placehold.it/989x742" alt="" /> -->
                            </div>
                        </div>
                        <div class="content row no-marg">
                            <div class="col-md-12 content-split portfolio-split">
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <div class="section-title text-left">
                                    <h3>Pengalaman Organisasi</h3>
                                    <hr>
                                </div>
                                <div>
                                    <table cellpadding="5">
                                        <tbody>
                                            <tr>   
                                                <td><strong>Nama Organisasi</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $o['judul']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tahun</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $o['tahun']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Jabatan</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $o['jabatan']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Nomor Anggota</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $o['no_anggota']?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <img src="<?php echo base_url('assets/img/iconAct/organisasi.png')?>" alt="img02"/>
                            </div>
                            <br>
                            <div class="col-md-12 content-split portfolio-split">
                                <div class="portfolio-nav">
                                    <hr>
                                    <br>
                                    <ul>                                           
                                        <li class="prev-link <?php if($i == 1){echo 'hide_prev';}else{echo '';} ?>"><a href="#" data-post="<?php echo $i-1;?>" rel="Next" title="next"><i class="fa fa-angle-left icon-single"></i></a></li>
                                        <li class="all-items"><a href="#" title="All"><i class="fa fa-th-large icon-single"></i></a></li>
                                        <li class="next-link <?php if($i == $max){echo 'hide_next';}else{echo '';} ?>""><a href="#" data-post="<?php echo $i+1;?>" rel="Prev" title="prev"><i class="fa fa-angle-right icon-single"></i></a></li> 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                   		$i=$i+1; 
                    	}
                    ?>

                    <?php 
                    	foreach ($buku as $b) {
                    ?>
                    <div class="single-portfolio" data-id="<?php echo $i;?>">
                        <div class="fc-page-title-breadcrumbs">
                            <div class="fc-page-title">
                                <div class="fc-page-title-wrapper">
                                    <div class="fc-page-title-captions">
                                        <h1 class="entry-title">Activity <?php echo $i;?></h1>
                                    </div>
                                    <a class="btn_close_port" href="#"><i class="fa fa-close"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="row no-marg">
                            <div class="col-md-12 post-image-wrap">
                                <!-- <img src="http://placehold.it/989x742" alt="" /> -->
                            </div>
                        </div>
                        <div class="content row no-marg">
                            <div class="col-md-12 content-split portfolio-split">
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <div class="section-title text-left">
                                    <h3>Penulisan Buku</h3>
                                    <hr>
                                </div>
                                <div>
                                    <table cellpadding="5">
                                        <tbody>
                                            <tr>   
                                                <td><strong>Judul Buku</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $b['judul_buku']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tahun Terbit</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $b['tahun_terbit']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Jumlah Halaman</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $b['jml_hlmn']?> halaman</span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Penerbit</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $b['penerbit']?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <img src="<?php echo base_url('assets/img/iconAct/buku.png')?>" alt="img02"/>
                            </div>
                            <br>
                            <div class="col-md-12 content-split portfolio-split">
                                <div class="portfolio-nav">
                                    <hr>
                                    <br>
                                    <ul>                                           
                                        <li class="prev-link <?php if($i == 1){echo 'hide_prev';}else{echo '';} ?>"><a href="#" data-post="<?php echo $i-1;?>" rel="Next" title="next"><i class="fa fa-angle-left icon-single"></i></a></li>
                                        <li class="all-items"><a href="#" title="All"><i class="fa fa-th-large icon-single"></i></a></li>
                                        <li class="next-link <?php if($i == $max){echo 'hide_next';}else{echo '';} ?>""><a href="#" data-post="<?php echo $i+1;?>" rel="Prev" title="prev"><i class="fa fa-angle-right icon-single"></i></a></li> 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                   		$i=$i+1; 
                    	}
                    ?>

                    <?php 
                    	foreach ($review as $r) {
                    ?>
                    <div class="single-portfolio" data-id="<?php echo $i;?>">
                        <div class="fc-page-title-breadcrumbs">
                            <div class="fc-page-title">
                                <div class="fc-page-title-wrapper">
                                    <div class="fc-page-title-captions">
                                        <h1 class="entry-title">Activity <?php echo $i;?></h1>
                                    </div>
                                    <a class="btn_close_port" href="#"><i class="fa fa-close"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="row no-marg">
                            <div class="col-md-12 post-image-wrap">
                                <!-- <img src="http://placehold.it/989x742" alt="" /> -->
                            </div>
                        </div>
                        <div class="content row no-marg">
                            <div class="col-md-12 content-split portfolio-split">
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <div class="section-title text-left">
                                    <h3>Pengalaman Reviewer</h3>
                                    <hr>
                                </div>
                                <div>
                                    <table cellpadding="5">
                                        <tbody>
                                            <tr>   
                                                <td><strong>Nama Acara</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $r['judul']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Waktu</strong></td>
                                                <td><strong> :</strong></td>
                                                <?php
                                                    setlocale (LC_ALL, 'IND');
                                                    $date=date_create($r['tanggal']);
                                                    $bulan = strftime("%B", $date->getTimestamp());
                                                    $hari = strftime("%A", $date->getTimestamp());
                                                    $tanggal = strftime("%d", $date->getTimestamp());
                                                    $tahun = strftime("%Y", $date->getTimestamp());
                                                    
                                                ?>
                                                <td><span class="project-terms"><?php echo $hari.', '.$tanggal.' '.$bulan.' '.$tahun;?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Penyelenggara</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $r['penyelenggara']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tingkat</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $r['tingkat']?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <img src="<?php echo base_url('assets/img/iconAct/reviewer.png')?>" alt="img02"/>
                            </div>
                            <br>
                            <div class="col-md-12 content-split portfolio-split">
                                <div class="portfolio-nav">
                                    <hr>
                                    <br>
                                    <ul>                                           
                                        <li class="prev-link <?php if($i == 1){echo 'hide_prev';}else{echo '';} ?>"><a href="#" data-post="<?php echo $i-1;?>" rel="Next" title="next"><i class="fa fa-angle-left icon-single"></i></a></li>
                                        <li class="all-items"><a href="#" title="All"><i class="fa fa-th-large icon-single"></i></a></li>
                                        <li class="next-link <?php if($i == $max){echo 'hide_next';}else{echo '';} ?>""><a href="#" data-post="<?php echo $i+1;?>" rel="Prev" title="prev"><i class="fa fa-angle-right icon-single"></i></a></li> 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                   		$i=$i+1; 
                    	}
                    ?>

                    <?php 
                    	foreach ($narasumber as $n) {
                    ?>
                    <div class="single-portfolio" data-id="<?php echo $i;?>">
                        <div class="fc-page-title-breadcrumbs">
                            <div class="fc-page-title">
                                <div class="fc-page-title-wrapper">
                                    <div class="fc-page-title-captions">
                                        <h1 class="entry-title">Activity <?php echo $i;?></h1>
                                    </div>
                                    <a class="btn_close_port" href="#"><i class="fa fa-close"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="row no-marg">
                            <div class="col-md-12 post-image-wrap">
                                <!-- <img src="http://placehold.it/989x742" alt="" /> -->
                            </div>
                        </div>
                        <div class="content row no-marg">
                            <div class="col-md-12 content-split portfolio-split">
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <div class="section-title text-left">
                                    <h3>Pengalaman Narasumber</h3>
                                    <hr>
                                </div>
                                <div>
                                    <table cellpadding="5">
                                        <tbody>
                                            <tr>   
                                                <td><strong>Nama Kegiatan</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $n['judul']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Penyelenggara</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $n['penyelenggara']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tanggal</strong></td>
                                                <td><strong> :</strong></td>
                                                <?php
                                                    setlocale (LC_ALL, 'IND');
                                                    $date=date_create($n['tanggal']);
                                                    $bulan = strftime("%B", $date->getTimestamp());
                                                    $hari = strftime("%A", $date->getTimestamp());
                                                    $tanggal = strftime("%d", $date->getTimestamp());
                                                    $tahun = strftime("%Y", $date->getTimestamp());
                                                    
                                                ?>
                                                <td><span class="project-terms"><?php echo $hari.', '.$tanggal.' '.$bulan.' '.$tahun;?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tingkat</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $n['tingkat']?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br>
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <img src="<?php echo base_url('assets/img/iconAct/narasumber.png')?>" alt="img02"/>
                            </div>
                            <br>
                            <div class="col-md-12 content-split portfolio-split">
                                <div class="portfolio-nav">
                                    <hr>
                                    <br>
                                    <ul>                                           
                                        <li class="prev-link <?php if($i == 1){echo 'hide_prev';}else{echo '';} ?>"><a href="#" data-post="<?php echo $i-1;?>" rel="Next" title="next"><i class="fa fa-angle-left icon-single"></i></a></li>
                                        <li class="all-items"><a href="#" title="All"><i class="fa fa-th-large icon-single"></i></a></li>
                                        <li class="next-link <?php if($i == $max){echo 'hide_next';}else{echo '';} ?>""><a href="#" data-post="<?php echo $i+1;?>" rel="Prev" title="prev"><i class="fa fa-angle-right icon-single"></i></a></li> 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                   		$i=$i+1; 
                    	}
                    ?>

                    <?php 
                        // print_r(count($leader));
                    	foreach ($leader as $l) {
                    ?>
                    <div class="single-portfolio" data-id="<?php echo $i;?>">
                        <div class="fc-page-title-breadcrumbs">
                            <div class="fc-page-title">
                                <div class="fc-page-title-wrapper">
                                    <div class="fc-page-title-captions">
                                        <h1 class="entry-title">Activity <?php echo $i;?></h1>
                                    </div>
                                    <a class="btn_close_port" href="#"><i class="fa fa-close"></i></a>
                                </div>
                            </div>

                        </div>

                        <div class="row no-marg">
                            <div class="col-md-12 post-image-wrap">
                                <!-- <img src="http://placehold.it/989x742" alt="" /> -->
                            </div>
                        </div>
                        <div class="content row no-marg">
                            
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <div class="section-title text-left">
                                    <h3>Pengalaman Kepemimpinan</h3>
                                    <hr>
                                </div>
                                <div>
                                    <table cellpadding="5">
                                        <tbody>
                                            <tr>   
                                                <td><strong>Jabatan</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $l['jabatan']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tingkat</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $l['tingkat']?></span></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Periode</strong></td>
                                                <td><strong> :</strong></td>
                                                <td><span class="project-terms"><?php echo $l['periode']?></span></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br><br><br>
                                
                            </div>
                            <div class="col-md-6 sidebar portfolio-sidebar">
                                <img src="<?php echo base_url('assets/img/iconAct/leader.png')?>" alt="img02"/>
                            </div>
                            <br>
                            <div class="col-md-12 content-split portfolio-split">
                                <div class="portfolio-nav">
                                    <hr>
                                    <br>
                                    <ul>                                           
                                        <li class="prev-link <?php if($i == 1){echo 'hide_prev';}else{echo '';} ?>"><a href="#" data-post="<?php echo $i-1;?>" rel="Next" title="next"><i class="fa fa-angle-left icon-single"></i></a></li>
                                        <li class="all-items"><a href="#" title="All"><i class="fa fa-th-large icon-single"></i></a></li>
                                        <li class="next-link <?php if($i == $max){echo 'hide_next';}else{echo '';} ?>""><a href="#" data-post="<?php echo $i+1;?>" rel="Prev" title="prev"><i class="fa fa-angle-right icon-single"></i></a></li> 
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <?php
                   		$i=$i+1; 
                    	}
                    ?>
                </div>	
                <!-- End #portfolio_single -->

                <!-- #contact -->
                <!-- MAP GEDUNG E INFORMATIKA UNDIP -->
                <section id="contact" class="layers">
                    <h2 style="display:none">contact</h2>
                    <!-- .page_content -->
                    <div class="page_content">

                        <!-- #contact -->
                        <div id="contacts">

                            <div id="mapContainer">

                                <!-- <div id="tabs" class="tab_close" title="" data-toggle="tooltip" data-original-title="Send Message">
                                    <i class="fa fa-envelope"></i>
                                </div> -->

                                <!-- <div id="tabs_resp" class="tab_close" title="" data-toggle="tooltip" data-original-title="Send Message">
                                    <i class="fa fa-envelope"></i>
                                </div> -->
                                 <!-- <div id="map"></div> -->
								    
                                <!-- <div id="map_canvas"></div> -->
                                <div id="mapGoogle"></div>
								    <script>
								      var map;
								      function initMap() {
                                        var loc = {lat: -7.048127, lng: 110.441071};
								        map = new google.maps.Map(document.getElementById('mapGoogle'), {
								          center: loc,
								          zoom: 18
								        });
                                        var marker = new google.maps.Marker({
                                            position: loc,
                                            map: map
                                        });
								      }
								    </script>
								    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgXOrJBD0A9e4Wy19YjrLk6sxAsXMr-9U&callback=initMap"
								    async defer></script>
                            </div>


                            
                            <!-- end #contentContact -->

                        </div>
                        <!-- #contact -->

                    </div>
                    <!-- .page_content -->

                </section>
                <!-- End #contact -->

            </div>
            <!-- End #main -->
        </div>
        <!-- End #wrappers -->

        <!-- jquery | jQuery 1.11.0 -->
        <!-- Credits: http://jquery.com -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/jquery.min.js') ?>"></script>

        <!-- Js | jquery.ui.effect.js -->
        <!-- Credits: https://jqueryui.com/effect -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/jquery.ui.effect.min.js') ?>"></script>

        <!-- Js | bootstrap -->
        <!-- Credits: http://getbootstrap.com -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/bootstrap.min.js') ?>"></script> 

        <!-- Js | nicescroll.js -->
        <!-- Credits: http://areaaperta.com/nicescroll -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/jquery.nicescroll.min.js') ?>"></script>

        <!-- jquery | portfolio -->
        <!-- Credits: http://www.mixitup.io -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/jquery.mixitup.min.js') ?>"></script> 

        <!-- jquery | prettyPhoto -->
        <!-- Credits: http://www.no-margin-for-errors.com/ -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/jquery.prettyPhoto.js') ?>"></script>

        <!-- Js | gmaps -->
        <!-- Credits: http://maps.google.com/maps/api/js?sensor=true-->
        <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> -->
        <!-- <script type="text/javascript" src="<?php echo base_url('assets/tema/js/gmaps.min.js') ?>"></script> -->

        <!-- Js | Js -->
        <!-- Credits: http://themeforest.net/user/FlexyCodes -->
        <script type="text/javascript" src="<?php echo base_url('assets/tema/js/main.js') ?>"></script>


    </body>
</html>