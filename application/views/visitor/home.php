<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Staff List
    </h1>
    <ol class="breadcrumb">
        <li><a href="http://if.undip.ac.id"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Staff List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <?php echo $this->session->flashdata('pesan') ?>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
        	<table id="user1" class="table table-bordered table-hover dataTable">
        		<thead>
        			<th class="col-md-1 text-center">No</th>
        			<th class="col-md-2 text-center">NIP</th>
        			<th class="col-md-7 text-center">Nama</th>
        			<th class="col-md-2 text-center">Profile</th>
        		</thead>
        		<tbody>
        			<?php 
                        $i = 1;
                        foreach ($dosen as $d) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $i?></td>
                        <td align="center"><?php echo $d['nip']?></td>
                        <td><?php echo $d['glr_dpn'];echo ' ';echo $d['nm_dosen']; if(!(empty($d['glr_blkg']))) echo ', '.$d['glr_blkg'];?></td>
                        <td align="center"><a class="btn btn-sm btn-info" href="<?php echo base_url('visitor/profile/'.$d['id_dosen']) ?>" target=_blank><i class="glyphicon glyphicon-eye-open"></i>&nbsp&nbsp Lihat Profil</a></td>
                    </tr>
                    <?php
                       $i=$i+1; }
                    ?>
        		</tbody>
        		<tfoot>
        			
        		</tfoot>
        	</table>
		</div><!-- /.box-body -->
        <div class="box-footer">
            
        </div><!-- /.box-footer-->
    </div><!-- /.box -->

</section><!-- /.content -->
