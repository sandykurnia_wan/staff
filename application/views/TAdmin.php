<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Staff Site | <?php echo $title; ?> </title>
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/logo undip.png')?>"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/ionicons-2.0.1/css/ionicons.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Datatables -->
        <link href="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />

        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <a href="#" class="logo"><b>ADMINISTRATOR</b></a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url('uploads/admin.png') ?>" class="user-image" alt="User Image"/>
                                    <span class="hidden-xs"><?php echo $name?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <!--<img src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" /> -->
                                          <img src="<?php echo base_url('uploads/admin.png') ?>" class="img-circle" alt="User Image"/>
                                        <p>
                                            <?php echo $name?> - Staffsite Undip
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo site_url('admin/changePassword') ?>"  class="btn btn-default btn-flat">Change Password</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->
            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="image" align="center">
                            <img src="<?php echo base_url('uploads/admin.png') ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="info" align="center">
                            <?php echo $name?>
                        </div>
                    </div>
                    
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">NAVIGASI</li>
                        <li class="treeview">
                            <a href="<?php echo site_url('admin') ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="<?php echo site_url('admin/lihatdosen') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>Daftar Dosen</span>
                            </a>
                        </li>
<!--                         <li class="treeview">
                            <a href="<?php echo site_url('admin/idcard') ?>">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>QR code</span>
                            </a>
                        </li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div id="contents"><?= $contents ?></div>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2017 <a href="<?php echo base_url(); ?>">Staff Site Informatika</a>.</strong> All rights reserved.
            </footer>
            </div><!-- ./wrapper -->

            <!-- jQuery 2.1.3 -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
            <!-- SlimScroll -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
            <!-- FastClick -->
            <script src='<?php echo base_url('assets/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
            <!-- AdminLTE App -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
            <!-- DATA TABES SCRIPT -->
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.js') ?>" type="text/javascript"></script>
            <script src="<?php echo base_url('assets/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.js') ?>" type="text/javascript"></script>
            <!-- Script needed -->
            <script type="text/javascript">
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 2000);
            </script>
            <script type="text/javascript">
              $(function () {
                $("#user1").dataTable();
                $('#user2').dataTable({
                  "bPaginate": true,
                  "bLengthChange": true,
                  "bFilter": false,
                  "bSort": true,
                  "bInfo": true,
                  "bAutoWidth": true
                });
              });
            </script>

            <!-- sweet alert -->
            <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert.min.js"></script>
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/sweetalert.css">

            <!-- confirm delete akun -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confirm_delete').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Hapus user ?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Hapus !",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>

            <!-- confirm reset password akun -->
            <script type="text/javascript">
                $(document).ready(function(){
                  $('.confirm_reset').on('click', function(){
                    
                    var delete_url = $(this).attr('data-url');

                    swal({
                      title: "Reset password akun? ?",
                      type: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#DD6B55",
                      confirmButtonText: "Reset !",
                      cancelButtonText: "Batalkan",
                      closeOnConfirm: false     
                    }, function(){
                      window.location.href = delete_url;
                    });

                    return false;
                  });
                });
            </script>
            <script type="text/javascript">
                $(".akundata").click(function(){
                    var username = $(this).attr("data-username");
                    var nama = $(this).attr("data-nama");
                    $("#username").val(username);
                    $("#nama").val(nama);
                // alert(id);
                });
            </script>
    </body>
</html>